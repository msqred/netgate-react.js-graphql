var ImageParticles = {

    // Settings
    density: 0,

    produceDistance: 0,
    baseRadius: 0,
    maxLineThickness: 0,
    reactionSensitivity: 0,
    lineThickness: 0,

    particles: [],
    mouse: {
        x: -1000,
        y: -1000,
        down: false
    },

    animation: null,

    canvas: null,
    context: null,
    bgImage: null,
    bgCanvas: null,
    bgContext: null,
    bgContextPixelData: null,

    initialize: function (canvas_id, imageData, densityPara = 9, produceDistancePara = 5, baseRadiusPara = 1.6, maxLineThicknessPara = 1, reactionSensitivityPara = 2, lineThicknessPara = 1) {
        // Set up the visual canvas
        this.canvas = document.getElementById(canvas_id);
        this.context = this.canvas.getContext('2d');
        this.context.globalCompositeOperation = "lighter";
        this.canvas.width = window.innerWidth;
        this.canvas.height = window.innerHeight;
        this.canvas.style.display = 'block';
        this.canvas.addEventListener('mousemove', this.pointerMove, false);
        this.canvas.addEventListener('mousedown', this.pointerDown, false);
        this.canvas.addEventListener('mouseup', this.pointerUp, false);
        this.canvas.addEventListener('mouseout', this.pointerOut, false);
        //Assign the settings
        this.density = densityPara;
        this.produceDistance = produceDistancePara;
        this.baseRadius = baseRadiusPara;
        this.maxLineThickness = maxLineThicknessPara;
        this.reactionSensitivity = reactionSensitivityPara;
        this.lineThickness = lineThicknessPara;

        window.onresize = function (event) {
            ImageParticles.canvas.width = window.innerWidth;
            ImageParticles.canvas.height = window.innerHeight;
            ImageParticles.onWindowResize();
        };

        // Load initial input image
        this.getImageData(imageData);
    },

    makeParticles: function () {

        // remove the current particles
        this.particles = [];

        var width, height, i, j;

        var colors = this.bgContextPixelData.data;

        for (i = 0; i < this.canvas.height; i += this.density) {

            for (j = 0; j < this.canvas.width; j += this.density) {

                var pixelPosition = (j + i * this.bgContextPixelData.width) * 4;

                // Dont use whiteish pixels
                if (colors[pixelPosition] > 200 && (colors[pixelPosition + 1]) > 200 && (colors[pixelPosition + 2]) > 200 || colors[pixelPosition + 3] === 0) {
                    continue;
                }

                var color = 'rgba(' + colors[pixelPosition] + ',' + colors[pixelPosition + 1] + ',' + colors[pixelPosition + 2] + ',' + '1)';
                this.particles.push({
                    x: j,
                    y: i,
                    originalX: j,
                    originalY: i,
                    color: color
                });

            }
        }
    },

    updateparticles: function () {

        var i, currentPoint, theta, distance;

        for (i = 0; i < this.particles.length; i++) {

            currentPoint = this.particles[i];

            theta = Math.atan2(currentPoint.y - this.mouse.y, currentPoint.x - this.mouse.x);

            if (this.mouse.down) {
                distance = this.reactionSensitivity * 200 / Math.sqrt((this.mouse.x - currentPoint.x) * (this.mouse.x - currentPoint.x) +
                    (this.mouse.y - currentPoint.y) * (this.mouse.y - currentPoint.y));
            } else {
                distance = this.reactionSensitivity * 100 / Math.sqrt((this.mouse.x - currentPoint.x) * (this.mouse.x - currentPoint.x) +
                    (this.mouse.y - currentPoint.y) * (this.mouse.y - currentPoint.y));
            }


            currentPoint.x += Math.cos(theta) * distance + (currentPoint.originalX - currentPoint.x) * 0.05;
            currentPoint.y += Math.sin(theta) * distance + (currentPoint.originalY - currentPoint.y) * 0.05;

        }
    },

    produceLines: function () {

        var i, j, currentPoint, otherPoint, distance, lineThickness;

        for (i = 0; i < this.particles.length; i++) {

            currentPoint = this.particles[i];

            // produce the dot.
            this.context.fillStyle = currentPoint.color;
            this.context.strokeStyle = currentPoint.color;

            for (j = 0; j < this.particles.length; j++) {

                // Distaqnce between two particles.
                otherPoint = this.particles[j];

                if (otherPoint == currentPoint) {
                    continue;
                }

                distance = Math.sqrt((otherPoint.x - currentPoint.x) * (otherPoint.x - currentPoint.x) +
                    (otherPoint.y - currentPoint.y) * (otherPoint.y - currentPoint.y));

                if (distance <= this.produceDistance) {

                    this.context.lineWidth = (1 - (distance / this.produceDistance)) * this.maxLineThickness * this.lineThickness;
                    this.context.beginPath();
                    this.context.moveTo(currentPoint.x, currentPoint.y);
                    this.context.lineTo(otherPoint.x, otherPoint.y);
                    this.context.stroke();
                }
            }
        }
    },

    produceparticles: function () {

        var i, currentPoint;

        for (i = 0; i < this.particles.length; i++) {

            currentPoint = this.particles[i];

            // produce the dot.
            this.context.fillStyle = currentPoint.color;
            this.context.strokeStyle = currentPoint.color;

            this.context.beginPath();
            this.context.arc(currentPoint.x, currentPoint.y, this.baseRadius, 0, Math.PI * 2, true);
            this.context.closePath();
            this.context.fill();

        }
    },

    produce: function () {
        this.animation = requestAnimationFrame(function () {
            ImageParticles.produce()
        });

        this.remove();
        this.updateparticles();
        this.produceLines();
        this.produceparticles();

    },

    remove: function () {
        this.canvas.width = this.canvas.width;
    },

    // The filereader has loaded the image... add it to image object to be producen
    getImageData: function (data) {

        this.bgImage = new Image;
        this.bgImage.src = data;

        this.bgImage.onload = function () {

            //this
            ImageParticles.produceImageParticles();
        }
    },

    // Image is loaded... produce to bg canvas
    produceImageParticles: function () {

        this.bgCanvas = document.createElement('canvas');
        this.bgCanvas.width = this.canvas.width;
        this.bgCanvas.height = this.canvas.height;

        var newWidth, newHeight;

        // If the image is too big for the screen... scale it down.
        if (this.bgImage.width > this.bgCanvas.width - 100 || this.bgImage.height > this.bgCanvas.height - 100) {

            var maxRatio = Math.max(this.bgImage.width / (this.bgCanvas.width - 100), this.bgImage.height / (this.bgCanvas.height - 100));
            newWidth = this.bgImage.width / maxRatio;
            newHeight = this.bgImage.height / maxRatio;

        } else {
            newWidth = this.bgImage.width;
            newHeight = this.bgImage.height;
        }

        // produce to background canvas
        this.bgContext = this.bgCanvas.getContext('2d');
        this.bgContext.drawImage(this.bgImage, (this.canvas.width - newWidth) / 2, (this.canvas.height - newHeight) / 2, newWidth, newHeight);
        this.bgContextPixelData = this.bgContext.getImageData(0, 0, this.bgCanvas.width, this.bgCanvas.height);

        this.makeParticles();
        this.produce();
    },

    pointerDown: function (event) {
        ImageParticles.mouse.down = true;
    },

    pointerUp: function (event) {
        ImageParticles.mouse.down = false;
    },

    pointerMove: function (event) {
        ImageParticles.mouse.x = event.offsetX || (event.layerX - ImageParticles.canvas.offsetLeft);
        ImageParticles.mouse.y = event.offsetY || (event.layerY - ImageParticles.canvas.offsetTop);
    },

    pointerOut: function (event) {
        ImageParticles.mouse.x = -1000;
        ImageParticles.mouse.y = -1000;
        ImageParticles.mouse.down = false;
    },

    // Resize and reproduce the canvas.
    onWindowResize: function () {
        cancelAnimationFrame(this.animation);
        this.produceImageParticles();
    }
}

ImageParticles.initialize('canvas', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZ8AAAJSCAYAAAAcfMhjAAABN2lDQ1BBZG9iZSBSR0IgKDE5OTgpAAAokZWPv0rDUBSHvxtFxaFWCOLgcCdRUGzVwYxJW4ogWKtDkq1JQ5ViEm6uf/oQjm4dXNx9AidHwUHxCXwDxamDQ4QMBYvf9J3fORzOAaNi152GUYbzWKt205Gu58vZF2aYAoBOmKV2q3UAECdxxBjf7wiA10277jTG+38yH6ZKAyNguxtlIYgK0L/SqQYxBMygn2oQD4CpTto1EE9AqZf7G1AKcv8ASsr1fBBfgNlzPR+MOcAMcl8BTB1da4Bakg7UWe9Uy6plWdLuJkEkjweZjs4zuR+HiUoT1dFRF8jvA2AxH2w3HblWtay99X/+PRHX82Vun0cIQCw9F1lBeKEuf1UYO5PrYsdwGQ7vYXpUZLs3cLcBC7dFtlqF8hY8Dn8AwMZP/fNTP8gAAAAJcEhZcwAAbroAAG66AdbesRcAAATzaVRYdFhNTDpjb20uYWRvYmUueG1wAAAAAAA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjYtYzE0NSA3OS4xNjM0OTksIDIwMTgvMDgvMTMtMTY6NDA6MjIgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLyIgeG1sbnM6ZGM9Imh0dHA6Ly9wdXJsLm9yZy9kYy9lbGVtZW50cy8xLjEvIiB4bWxuczpwaG90b3Nob3A9Imh0dHA6Ly9ucy5hZG9iZS5jb20vcGhvdG9zaG9wLzEuMC8iIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdEV2dD0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlRXZlbnQjIiB4bXA6Q3JlYXRvclRvb2w9IkFkb2JlIFBob3Rvc2hvcCBDQyAyMDE5IChXaW5kb3dzKSIgeG1wOkNyZWF0ZURhdGU9IjIwMjAtMDEtMTJUMjA6MTg6MzkrMDI6MDAiIHhtcDpNb2RpZnlEYXRlPSIyMDIwLTAxLTE0VDIxOjQ2OjA1KzAyOjAwIiB4bXA6TWV0YWRhdGFEYXRlPSIyMDIwLTAxLTE0VDIxOjQ2OjA1KzAyOjAwIiBkYzpmb3JtYXQ9ImltYWdlL3BuZyIgcGhvdG9zaG9wOkNvbG9yTW9kZT0iMyIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpjZDA0N2IzYy1lNmY4LThkNDMtOTFjYy1iMDUwYzdkZGRmY2UiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6Y2QwNDdiM2MtZTZmOC04ZDQzLTkxY2MtYjA1MGM3ZGRkZmNlIiB4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ9InhtcC5kaWQ6Y2QwNDdiM2MtZTZmOC04ZDQzLTkxY2MtYjA1MGM3ZGRkZmNlIj4gPHhtcE1NOkhpc3Rvcnk+IDxyZGY6U2VxPiA8cmRmOmxpIHN0RXZ0OmFjdGlvbj0iY3JlYXRlZCIgc3RFdnQ6aW5zdGFuY2VJRD0ieG1wLmlpZDpjZDA0N2IzYy1lNmY4LThkNDMtOTFjYy1iMDUwYzdkZGRmY2UiIHN0RXZ0OndoZW49IjIwMjAtMDEtMTJUMjA6MTg6MzkrMDI6MDAiIHN0RXZ0OnNvZnR3YXJlQWdlbnQ9IkFkb2JlIFBob3Rvc2hvcCBDQyAyMDE5IChXaW5kb3dzKSIvPiA8L3JkZjpTZXE+IDwveG1wTU06SGlzdG9yeT4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7ZCDBCAAA6bElEQVR4nO3da5BkZ2He8afnsrfZm5ZNYRNXtJRTjopQIItLwIB2tboLu9S+J+aiEQgJkFQsSRy7UuVCJP6SVKoQJaeSSjnRqmxsEBetSKKsQSirAoG0uiB5DcgXggRYt52d6/ZMz0z3OfnQs6ve3re7zzl9zns55/+r4oOs0fQ7m0o/+/bzdE8tjmMBQOiuu71xQNKBB+6ausPxUZBAjfABELrrbm9MS7p74x/f+cBdU486PA4SGHN9AAAYxXW3Nw7r1eCRpP/q6ChIgZsPgCBdd3tjt6QjkvYb/vXtD9w19UdWD4RUCB8Awbnu9sbFkg5LenOfL1mQ9A8fuGuqYetMSIeX3QAEZWNYcEz9g0eSdkn6nI3zIBtuPgCC0TMsSILxgae4+QAIgmFYkMTnCzgKcsDNB4DXhgwLkviDB+6a+sPcDoRcED4AvJVgWJDEgqRfeOCuqVfyOBPywctuALyUcFiQxC5J/23U8yBf3HwAeCfDsCAJxgce4eYDwCsZhwVJMD7wCDcfAF7IYViQBOMDTxA+AJy77vbGPnWCZ9R+ZxjGB57gZTcATm0s2p5W8cEjdcYH91p4HAxB+ABwZmNY8F11QsGW/dfd3rjK4uPBgJfdADhx3e2NOyV9wtHDPy/pogfummo6evzKm3B9AADVsjEsOCzpeofHuFDSv5bE+MARbj4ArLE4LEjqtYwP3KDzAWCF5WFBUowPHCF8ABTO0bAgCcYHjvCyG4BCOR4WJMH4wAEGBwAK4cmwIIkLJf17Sb/r+iBVws0HQO48HBYk8fMP3DX1/1wfoirofADkytNhQRL/w/UBqoTwAZAbj4cFSey/7vbGb7g+RFXwshuAXAQwLEhiQdLPMD4oHoMDACMJaFiQxC4xPrCCmw+AzAIdFiTB+KBgdD4AMgl4WJAE44OCET4AUgt8WJAE44OC8bIbgFSuu71xh6RPuT6HBYwPCsTgAEAiG8OCOyXd4PYk1uySdJekj7g+SBlx8wEw1EbwHFM5+51h3vTAXVMnXB+ibOh8AAy0MSx4TtUMHkn6E9cHKCPCB0BfG8OCYyrvsCCJN++/4cXPuj5E2fCyGwCjCg0LhorakV7+yU8u/O7/fMOPXZ+lLBgcADjHdbc3dkft9h+PjY//uuuz+GJsfEw7X7Pnm+r8+gXkgJfdAJx1ZlhA8JxvaseOf/TPfvOHH3V9jrLgZTcAks4OC46p2v3OQGvN1fZPf/jDrc8+9LZ112cJHTcfAAwLEtq0ZfP4P3jd6464PkcZED5AxV13W+MOxbpbsXYplsSLIQNN7dx53Vt/7W/e6PocoWNwAFTUtbd1PrGgZvrEgt4Aqtk4URjGxse0Y/fuByX9jOuzhIybD1BBG8FzTNINZy47Ay89ib6oOrZOTb2W8cFoGBwAFXPtbemGBYkuPRW8GbXW16Mf/83fbmF8kA03H6BCrr0t/bCAm5HZxOTkGOOD7AgfoCKuva1xh6S7NeKijTB6FeOD7BgcACV3Zliggn4VQqJtQkkHDGPjY9q+a9fDkl7j+iyh4eYDlFj3sMDWY1btZrRt+/Y97/itH/0b1+cIDYMDoKTSDgtsKeOAobW+Hs2+/MoFT9z3C4uuzxIKbj5ACV17W6MuD4NHKufNaGJycmzLtm1fcX2OkBA+QMlce1vjkKT75GHwmJQljHbuueByxgfJMTgASuTa2xqHZbHfKULIA4adF+z5pqQLXJ8jBHQ+QAl0DQtK/6uufe+MZl9+5fcevff1/9HdCcJA+ACBu/bWxsWq6Ygq+IvOfAyi1vq6Zl9+ZRfjg8HofICAXXvrxrAg1oW+dyJF8LEvmpic1OZtW/938Y8UNsIHCNS1tw4YFgRQ0BfBlzDatWfPu99y/bNvKea7lwPhAwTo2lsbhyV9JvF/QBhZD6Nde/c+eNHBx3mO7YO1GxCQa2/NaVjg6VqsaDaXdFu2bd39mte+9t9K+sNs36HcGBwAgbj21sbFkqVhQUXCqFfeA4aoHWnmxRcZHxhwJQQCcM2tjXosHYulC638dZGX6XJ5mW5sfExbt28/mt8Jy4PwATx3jWFYYD0bKhhEUj5htGP3rne+/Td++I5iThguwgfw2DUJhwVWw6iityIpexht37Xz64wPzsXgAPDQNSMOC6zuCSo6XpCSDxjGamPbJyan7pf0K8WfKgwkMeCZazrDgqeV40flcDOyw/SjL83P64Xnn1drvfHLb7z6797r7nR+Ye0GeOSazicWHJblT6S2elmp0M1o/uRJzZ08efafa7XJH01M7n7DiaP7mg6P5QVuPoAnTMMCW7gZ5SuKIp184YVzgkeS4nj99VHU/D1Hx/IKNx/AAxvDghtcn6MfbkbJRVGkF597TmvNfpebWnNy02vecOLo639k9WCeYXAAOHTNxxu7Nz6Rer/rswzCgCGZtWZTLz7/vKJ2e8BXxVva7cZdkn7Z1rl8xMtugCPXfLxxsTqfSL0/tJeheJnufMtLSwmCpyNqr7z3jVf9baXDh5fdAAeu+XjjgDoflTO83wnob/5nVO1lusXZWZ166aVU/02tNvGjickLKjs+4OYDWHbNxxvTkv6vkg4LAvmbf7cq3YxOvvBC6uCRpDhuvT5qr/x+AUcKAjcfwKJrPl7AsMCDv/mnVYab0fBhQRLVHR8wOAAsuObjjd1SQcOCAAv60AcMa82mXv7JT9RaXx/xO8Vb2q3TlRwf8LIbULCzwwJbizZepiv0wc4MC0YPno4oalZyfMDLbkCBUg0LbAngZtTLl5fpsgwLEj1kBccH3HyAgqQeFtjCzSjTg2UdFiR6yAqOD7j5AAW4emNYEOAlg5tRjyiK9OLzow4Lkqg1JyYveMNf/cXPV2J8wOAAyNHVPcOCALcAQR66+8h5Hnet2dTLP81jWJBEvCWq0Ccf8LIbkJOrEwwLAnzFK7hD53Xc5aUlvfjj/IYFSUTRamXGB7zsBuTg6pyGBQFcMs4X2KGTHHdxdlanXi6m3xmmVhv/0cTkntKPD7j5ACO6OsdhQWCXjI7ADj3suCdfeMFZ8EhSHLdf3243Sj8+4OYDjODqjzUOq2bvVyEEdsnoCOTQURTpJSvDgiTKPz5gcABkcPXHuoYFFgv6ALcAQRx6rdnUK9aGBUmUf3zAy25ASld/bMiwwOLLUAG92vUqz16mW15a0kuWhwVJRNHqe//pVX9d2vDhZTcghas/lsOwwNLf/D28YCRj8eCLs7OaddjvDNMZH1zwhhNHX+/Da4G54uYDJHT1x3IaFlj6m79nF4zkLB165oUXvA4e6cz4YLmU4wNuPkACV3+scaekT1h5MG5G/eVwaL+GBYk0Jyb3lG58wOAAGGBjWHBY0vXWHtRSQR/ADuB8Ix7av2FBIlvaraXSjQ+4+QB9XP2xxj51+p03uz1JD25G/Q049PLSkmZefEFRu23vPDkan9j5K9/72j/5X67PkRfCBzDoWrT59YnUJoRRfxuH9n1YkETZxgcMDoAeG8OC7yqE4JEYMAwShzEsSKJs4wNuPkAXq8MCWyp6M4qiSC/9OKhhQRIbn3zwj4MfHzA4AORoWGBLUb9vYMDDFPxQQ62tBjksSGJLu3W6FOMDbj6oPG+HBTZYTAhbD7V8OuxhQRLjEzt+5Xtfuyjo8QHhg0q7yjAs8O3lI6sCD6PFufCHBcnUfjS56TVBjw8YHKCyrvqoeVgQZLGeF0efS5fHQ828WI5hQTJx8L92gZsPKumqj/YZFiT46zg3I78eKooivfLTn6i53Cj0PB4KenzA4ACVctVHhwwLErTmPhXr1nn26yPWVpuaefGFsi3akgp6fMDNB5Vx1UdzGBZwMxrM4g+/urysV/7+J6UeFkg11WqD7gjx6drY5Ae+//U3HrF1orwQPqiEqz5a0CcWEEaDFfTDn16Y16kXXyjmm2+o1SaH/PtxqTa4Nh+rbRr8PcYmNMIf0sOS6ieO7pvP+g1cInxQehvDgrutPBhhNFgOP/zcK7NrpxcWBz6r12oTqg3aU9XGOuERrntOHN037foQoyB8UGp9hwW2EEaDpfjh41grsy/Nb11dKd0bR9O68cTRfYddH2JUhA9KaeiwwBXCaLA+P3y7Fc3MvrSwd32tZfc8fllQ52W2Y64PkgfCB6WzETzHFMInFhBGg9Wk9bXWzKkX5vdGUaWfq56RNH3i6L6nXR8kL4QPSqWwYYEthNE5mo3V2dlXFve4PodjQQ8L+iF8UBobw4I7FWrwmFQ4jOZnllaWl5pbXZ/DseCHBf3w8Toohas+2rhDnUVbeYJHSvQZNGX7OKA41sqpF+dF8OiTZQ0eiU84QOA2+p07Jd3g9iSWJPj1CCF/AkO7Fc3MvsywQJ1+54jrgxSJ8EGwghoWFCFhylj6dT4jW19rzZx6sfLDgufV6Xeedn2QotH5IEhX3bIxLKiV7GW2PAXUFzEskNRZtB0o27CgH8IHwbnqlgHDAl+eTX3kaRgxLJBU4mFBPwwOEJQrb2ncEUt3x/2GBWVr3/Pk2XiBYcFZpR4W9MPNB0G48pbhw4JEf2vnZtSfxZsRwwJJFRkW9EP4wHsbwXNMKYcFhNGICgojhgWSKjQs6IfwgdeuvCWfTywgiHKQQxgxLJBUsWFBP4QPvHXloGHBiAijHKQMI4YFkio4LOiHwQG8dOUtxX5iQaJinfHCYAkHDFGslRmGBVJFhwX9cPOBV5IMC2zgZpSDWmdYMMewoNLDgn4IH3gj67DABsIovfW11szsSwwLVPFhQT+ED7yQ17DAFsJosOby6uwcwwKGBQMQPnCuyGGBLYTRqxZOMSwQw4KhGBzAqaKHBbYwYNj4xIKXGBaIYUEifKo1nLnylsZhlfRXIST6wOmQf/dBjyiKF2Zfmt/FsIBhQVKED6y78uaNYUHNv2FBUcocRu1We3bmhbk9DAsYFqRB5wOrrry5cbGkI5IuPO9fBvJkW4RQOyOGBZIYFmRC+MCaK29u1CUdVtJ+x8MnW1tCCKPFuUazsbC8xe0pnGNYkBGDA1hx5c2NQ5LuU5phQckL+kF8HjDEsVbmXllcJnj0aYInO24+KNyVNxc0LOBmlMMXpcOwQFJnWHDoxNF9h10fJGSEDwpzxc2N3TWbn1hAGOXwRf0xLJDUCZ4DDAtGR/igEFf0GRZYzQfCKIcv6mBYIIlhQa7ofJC7KzrDgmMyLNqs1hR0Rrl0RotzjSbBo3tE8OSKmw9ydUVnWPCZrP89NyM7kvzosbQyf3Ixbi6vbiv8QH779Imj++5wfYiyIXyQmysKGBYQRsUz/dhRFC/MvsywQAwLCkP4YGRX3GzvVyEQRsWLWu3ZmRcZFohhQaH4eB2MpN+woChWP4Em0I+7GUVzeXV2/mTl+x2GBRYwOEBmg4YFtjBgyM/SXKNJ8DAssIWbDzIZdVhQFG5G6cWxVhZmGBaIYYFVhA9Su+IjG8OCAJ5sCaPBoihemGNYwLDAAQYHSOyKjwwZFgTwZNvN+nE9+/Npt9qzpxgWMCxwhPBBIld8JMOwwLMn22GqtKRjWCCJYYFTDA4w1BUfyTgsCKygr8p4gWGBJIYFznHzwUBXfKQxLenuQr45NyOrD8aw4CyGBR4gfNDX2WGBLYRRYQ/GsEASwwKvED44z8aw4Iik/U4PQhjl8mAMCyQxLPAO4YNzbAwLDsvW7+BJgzBK/WAMCyQxLPAS4YOzrvhI44A6N57kv+raJcJo4IMtzTWajcXK/6rre9R5qW3e9UFwLsIHkqTLe4YFgT2vB3jg4o4cx1pZOMWwQAwLvEb4QJcnGBYE99we3IHzOXIUxQtzr1R+WCBJNzIs8BvhU2GXjzAsCO65PbgDpz9yu9WePfUSwwIxLAgCbzKtqMs7w4JjyrhoC+z9owEeON2Rm8urBE9nWHAxwRMGbj4VdLmFYUFwF43gDvzqkRuLywtL840wRiLFuV/SNMOCcBA+FXP5TV3DAotPuME9t4dw4FjLi3NLtZXTza2uj+LYZ08c3XfI9SGQDuFTIZffNGRYQBj159mB4zhuzr48v6XFsIBhQaAInwq4/KYMwwLLT7aePbcP5/DAZ4YFcbX7HYYFgWNwUHKX35RxWGC5oA9sC+BswLC6MSyoePAwLCgBbj4ldvlNBQ4LeIlusAIO3VhcXjjNsIBhQUkQPiV1zrDABsJosFEOzbDgDIYFJUL4lNDQYYENhNFgCQ/NsOAshgUlQ/iUSKZhgS2E0WCGQzMskMSwoLQIn5K4/KbGPnWCx79fhWBCGA20urI6uzC7VPXgeUZS/cTRfc+5PgjyR/iUQNeiLdwymjA6q7G4vHB6gWGBGBaUGuETOOvDAluqGEZnhgUNhgUMC8qP8AnY5Tc17pT0CdfnsKLkYXR2WLDOsIBhQTVMuD4A0jvYGRYcrknXuz6LNb1/RyowISw+lKSNYcHLDAvEsKBSuPkE5uCAYYE3Lx+5EOjNiGGBJIYFlUT4BOTgh7uGBQmeAQkjvx+KYYEkhgWVRfgE4uCHhwwLCKPBfAojhgVnMCyoMMInAAc/nGFYQBgN5iiMGBacxbCg4ggfjx38cGdYoDyGBYTRYBZ++HarPTvLsGBBnX7nmOuDwC3Cx1MHP1zwJxYQRoPl/MOvrqzOLjIseEadfudp1weBe4SPh84ZFthCGA02wg+/zLBAkh5W58Yz7/og8APh45mhwwJbCKP+kv7gsZaXGBZI0j0nju6bdn0I+IXw8UimYYEthFF/hh88juPmHMMCiWEB+iB8PJDrsMAWwqivdpthgRgWYAg+XsexwocFRUnwGTS2P6bGB6srq7OLcwwLxLAAQ3DzccjJsMCWCt6MlpcYFohhARIifBzxZlhgS8nDaGluaYVhAcMCJEf4OOD1sMCGhCkTQhjFsVbmXpnbyrCAYQHSofOxaGNYcKekG9yexLGEZVA8/Eucarfbs/MnF/a0W23XR3GJYQEyIXwsOfihxm51+p3OsMDHZ1NXAhwvrK2uzyycWtjLsIBhAbLhZTcLDn4owbDA9bOpzzzri1YazZmluaW9Fh/SRwwLMBLCp2CXfagxrc5LbWeDJ9ETJWHUn8MwYlggiWEBckD4FOiyDzXukPSpYV9HGI3IQhgxLDiLYQFyQfgU4LIPjTYsIIxGlHMYMSyQxLAAOSN8cnZZ77BgRARRDkYII4YFkhgWoACET44uSzIsGBFhlIOEYcSwQBLDAhSE8MmJaVhgA2GUA8Ofz9Lc0kqTYQHDAhSG8MlB0mGBDYTRiGKtzJ1kWCCGBSgYbzIdwajDgiIkejOmb+/Y9ES73Z5dOLmwp91mWMCwAEUjfDLKe1hQFMIomfXV9ZmF2coPC55XJ3iedn0QlB/hk4GNYUFRCKPzNZcZFqizaDvAsAC20Pmk5GpYYEvVOqOleYYFYlgAB8ZcHyAkG8OCu1XS4JE6l57u/2X/Is91PrFABI8+SfDABV52S+CyG3uGBSX6m/8wZXyZjmGBpM6wYPrE0X1HXB8E1cTLbkNsBM8xDRoWeP5kW6TQXqZjWCCJYQE8QPgMcNmNGYcFHj3Z2uZzGDEskMSwAJ4gfPq47MZGXdJh5dHvEEY5fNFoGBZIYlgAjzA4MLjsxsYhSfcpr2FBGQr6jJwPGBgWnMGwAF7h5tPjwI2Nw5JusHpZ4WaUwxedj2GBJIYF8BThs+HAkGEBYWRHXmHEsEASwwJ4jPCRdKAzLDgi6cKk/421fKhwEEnZwohhgSSGBfBc5cPnQE7DAsLIjmE//tL80kpzufL9DsMCeK/Sg4MDOQ4LrG0KKjxekAb86J1fhSCCh2EBwlDZm8+ZYYGtx+NmVJyoHc0szi7srfjv4GFYgKBU7uN1hg0LimLtE2gC+6ibUbXWWzMLp+YZFjAsQGAqdfPJMiywhZtReqvN1dml2cU9rs/hGMMCBKky4ZPXsMAWwmiw0wwLJIYFCFglBgcHphuHFOs+xWEEj8SAoa9YKwun5hkWMCxA4Ep/8zkwPWRYEODf/Kv6hleGBZIYFqAkShs+B6YzDgs8erJNqgphxLBAUid4DjAsQBmUMnwOTOc4LCCMnD8YwwJJDAtQMqXrfA5MN+rq3HjyWbSF1onI8pELfrDT80srBI/uEcGDkinVzefAdOOQpM9YfVBuRsU8WKyVhdn5reur67keJ0CfPnF03x2uDwHkrTThM3RYYAthNPKDMSyQ1Ol3Dp04uu+w64MARQg+fDIPC2whjFI9GMMCSQwLUAFBdz4bw4Jj8jV4JDqjFA+2urI6O39yrurB84ykfQQPyi7Ym8/+6cYBdRZtZ984GuAlI8hDF3Hk0wt8YoE6w4JDDAtQBUGGz/7pxrSku4d9XYDP60EeeqQjnxkWrDEsYFiAKgkufPaPMCwI8Hk9yEMnPXLUjmYW5xgWiGEBKiiY8NnfGRYckbQ/r+8Z4PN6kIc2Hbm13ppZmGVYIIYFqKggwmd/Z1hwWAUPCwJ8Xg/y0GvN1dmlucq/cZRPLECleR8++2/oGRZYfLIN8Hnd+0OfXlhaWWVYwLAAled1+Oy/IcGwgDAazJdDx1pZZFggMSwAJHkcPvtvyDgsIIwGc3DoqB3NLDEsYFgAdPEufPbfkPOwgDAarOBDt9ZbM4sMCxgWAD28Cp/9N1gYFhBG/eV8YIYFkhgWAEbehM95wwJbCKP+RjgwwwJJDAuAvrwIn0TDAlsIo/6SHJhhwRkMC4ABnIdP5mGBLYRRfz0HZlggiWEBkIiz8Ml9WGALYWTUajEsEMMCIDEnv1JhY1hwTKEFj2T19w2E8tsY1pqrswsz/CoESRcTPEAy1m8+zoYFNli+qvhwM1peajRXGstbXJ/DsfslTTMsAJKbsPlgXg0LitCb4wWnQ/fDWQ+iWCtLC4vxWnN1m+2H9sxnTxzdd8j1IYDQWAufS29o3CnpE5Iff2O3wmIY2cy9OIoXFufmd1V8WCBJNzIsALIp/GW3SzvDgsOSru97iEJP4LEAxwtRuz07f2puT8X7HYYFwIgKDZ9LP9jYp06/8+onFiR4FiSM/Hyotebq7NI8n1ggqX7i6L7nXB8ECFlh4XPpB88u2gYPCwij/jwKI4YFkhgWALkpJHwu/eAIwwLCqD8XYcSw4AyGBUCOcg+fSz/46rBgZAmfbAmjYpwZFrQZFjAsAHKWW/hc+sHhw4KRcSsaLMcfPmq3ZxcYFjAsAAqSS/gYhwU2EEaDZfzh15qrs6cZFjAsAAo0cvgkHhbYQBgNluCHX15qNJsMCxgWAAUbKXxGGhbYQBgN1v3Dx1o5zbBAYlgAWJE5fHIdFthCGBmdHRa0GBYwLADsSP3xOlaGBUVJ8Bk0lj+ezbmzwwIPfqmgQwwLAMtShY+zYUFRKh5Ga6sMC8SwAHAi8ctuXg0LbCnxy3TLS41mc5lhgRgWAE4kuvl4PywoShlvRmeGBasMCxgWAO4MDZ8ghwVFCTyMGBacxbAAcKxv+Fz6AcOwwKdnUh8EFEYMCyQxLAC8Yex83vOBxr5akmEBYTSYJ50RwwJJDAsAr5wXPu/5gHlYkOhJkjAazEEYMSyQJD2sTvDMuz4IgI5zwuc9H2hMS7pTCRZthFEOigwjhgVn3HPi6L5p14cAcK6z4fOeDzTukPSpzN8oty+qsJzCiGHBWQwLAE/V3v3+07vVue3ckOs3zu2LKixDGDEskNQZFtRPHN13zPVBAJhNqNPv5P6JBYlWXr5MwXyVck23tro622BY8Iw6bxx92vVBAPQ3Juk5Gw8U9/wv+xdV2IA/m5WlRpPg0cNiSg0EYULStKSnJV1o84G5GY3o1T+b5eWlpdrqSnOrw9P4gGEBEJBaHMdn5tXfdX2YbnRGCcRxc3FufgvDAoYFQGjGJOmbfzL1tKRPuj3KuXiZbrCo3Z6dnzlV9eBZkHQZwQOEp/d9PkcUyO/pqfLNaG11dXZ5canqizaGBUDAxnr+eVrS8w7OkVpVb0bN5eWFxsJi1YOHYQEQuH4fr+NV/5NFCW9GDAs6GBYAJdB78/Gy/8ki0YUnlFtRHDcXZ+e2ETy6keAByqHvbzINqf9JK6RbUdRuzy7O8okF4hMLgFI57+bTZVqB9D9phdIXra2uEjydYcEBggcol743H6k8/U9aPtyMmsvLCyunG0M/Xbzk+FUIQEkNDB9Jes/7G4ckfabz1cUfyEeWw6jiw4J4Lo6jRUmP/NVf/Pz7XJ8GQDGGho8kvef9ffofwmjEL+rh6BML4rg95OXVaCyOWtHA76H2zjhuXTD4gdqK43bSYz0v6eJnH3r7fNL/AEA4JhJ+3bRMn/9W0c9eK+Jz6eI4bs7PnDwZR1HU+ef1CcXxgBSKt0Tx2muHnjVaG/YlkuXP9UvoQkmHJdXdHgNAERLdfCTpPe9P2f9UJIhMst6MluZeWFlpzFX05ba+PvnsQ2+/0/UhAORr0NrtHN/805Tv//FgLeZK1jXdjt0/G09Mbin+gGH5zEUHj1/s+hAA8pX45nNG3/4n9SOP/B2CNehHb7fXZ2Zf+eHeOErcjVQB/Q9QMolvPl2mlcf7f7gZGX/08fHJvTt3v27W/qm8dqb/AVASqcPnm386Na8iSmDC6Oz/Nm/duWfr1AUrbk/lnesvOnj8kOtDAMhHlpuPvvmnU0/HRX/+W8XDaPsu+h8D+h+gJFJ3Pt3e3dP/WK1xKtAZRfQ/JvQ/QAlkuvl0mVZX/2P1slKBm9HY+OTeHfQ/veh/gBIYKXy+NaT/IYxGt3nrzj1b6H960f8AgRv15qNvpXj/D2GUzQ76HxP6HyBgI3U+3Xr7n0yHyeUkPj7Y6Oh/jOh/gECNfPPpMq0R3//Dzag/+h8j+h8gULmFz7D+JwvC6Fz0P0b0P0CA8rz5pOp/siCM6H/6oP8BApNb59Mtj/4ni6p0RvQ/RvQ/QEByvfl0mVYen/+WUlVuRvQ/RvQ/QEAKCZ8i+p8syhxG9D9G11908Pi060MAGK6om0/h/U8WVi8qFsKI/sfoTvofwH+FdD7dXPU/aYXaF9H/GD0j6QD9D+Cvwm4+Z8WaVmy//0kr1Jfo6H+M3izpTteHANBf4eHzrc9t9D+eTpf7CSmM6H+MbqD/AfxV/M1H0rc+Z+h/CKNcH4z+x4j+B/BU4Z1Pt3e/L0X/E9hnr/nQGdH/GNH/AB6ycvPpMq2k7//hZpT6weh/jOh/AA9ZDZ+z/U8WhFGiB9u8hf7HgP4H8Iztm4+5/8kioCCS7IbRjp30Pwb0P4BHrHY+3VL1P2kF1hdJ+R85aq/PzJ6k/+lB/wN4wvrNp8u0ivr8t8BeopPyPzL9jxH9D+AJZ+EzUv+TVkXDaPOWnXu2bKP/6UH/A3jA5c0nv/4nrQqFEe//MaL/ARxz1vl0K7T/yaJknRH9jxH9D+CQ05tPl2k5+P0/fZXsZkT/Y0T/AzjkRfhY7X+yKEEY0f8Y0f8AjngRPlKn/4k9+/0/fQUaRtvpf0zofwAHvOh8ur2rp/8JsH7x+tBRe31mjv6nF/0PYJk3N58u0+rqfwK8ZHh96LHxyb3b6X960f8AlnkXPo8M6X88fl7vz7ND0/8Y0f8AFnkXPpL0SIr3/3j2vJ6MB4em/zGi/wEs8a7z6fau9zWOSdo/yvfwuH7pz9Kh6X+M6H8AC7y8+XSpS1oY5Rt4cMlIz9Kh6X+M6H8AC7wOn2H9TxaE0bnof4zof4CCeR0+kvTI56aOSfp0Ud8/uCCScg8j+h8j+h+gQF53Pt3e9Ttd/Y+lTiTIvkjKdHD6HyP6H6Ag3t98utR1pv+x9NpZkC/RSZkOTf9jRP8DFCSY8Hnkzwb0P4RRfykOTf9jRP8DFCCY8JGkR/4sYf9DGPU35ND0P0b0P0DOgul8up3T/2RBZ9Rfjf6nD/ofIEdB3Xy61DXK+3+4GfUXS2Njk3u376L/6UH/A+QoyPAZ2P9kQRidh/7HiP4HyEmQ4SOl6H+yIIwk0f/0Qf8D5CDIzqfbyP1PWhaLHB86o6i9PjM3Q//Tg/4HGFGwN58udY34+W+pWLyu+HAzGhun/zGg/wFGFHz45N7/pFWBMKL/MaL/AUYQfPhIBfc/aZU0jOh/jOh/gIyC73y6We9/sgi4M6L/MaL/ATIoxc2nS102+58sAr4Z0f8Y0f8AGZQqfJz3P1kEFkb0P0b0P0BKpQofybP+J4sAwoj+x4j+B0ihVJ1PtyD6nyw86Yzof4zof4CESnfz6VKX7/1PFo5uRr3of4zof4CEShs+QfY/WTj8KCD6HyP6HyCB0oaP1Ol/4pD7nywsh9EU/Y8J/Q8wRGk7n26/1NP/+PCZac4U8MNH7fWZefqfXvQ/wAClvvl0qaur//HhM9OcKeCHp/8xov8BBqhE+Hz7z6bmFave78mWMNLIP/ymLTv3bKb/6UX/A/RRifCRpG//+cb7fxI80VY2iKSRwoj3/xjR/wAGleh8uv3Svxjw/p8EfUil+yJp6B8A/Y8R/Q/QozI3ny519Xv/T4K/9Vf6JTpp6B8A/Y8R/Q/Qo3Lh8+0/T/H+H8JoOMMPT/9jRP8DdKlc+Ehd/U9ahNFgXT/49p30Pwb0P8CGynU+3Qb2P1nQGZ0jaq/PzJ+i/+lB/wOoojefLnXl+flv3IzOMTY+uXf7TvqfHvQ/gCoePqn6nywIo07/s5X+pwf9Dyqv0uEjjdD/ZFHRMOL9P0b0P6i0Snc+3XLvf7IocWdE/2NE/4PKqvzNp0tdrn//T4lvRvQ/RvQ/qCzCZ0Ph/U8WJQsj+h8j+h9UEuHTxWr/k0UJwoj+x4j+B5VD52PgRf+TRSCdEf2PEf0PKoWbj1ldrvufLAK5GdH/GNH/oFIIHwMv+58sPA4j+h8j+h9UBuHTh/f9TxaehRH9jxH9DyqBzmeIYPufLBx0RvQ/RvQ/KD1uPsPVFWL/k0WCK0/etyL6HyP6H5Qe4TNEafqftCy+REf/Y0T/g1IjfBIoZf+TVsFhRP9jRP+D0qLzSaFS/U9aOfRF9D9G9D8oJW4+acSqK9aCtx8f4FIONyP6HyP6H5QS4ZPCtz/f0//48I5NX2UMI/ofI/oflA7hk9K3Pz+g/yGM+ksRRlO7fjYep//pRf+DUqHzyeid//zV/ifRe198+FA1n/X8+UTt9ZkF+p9e9D8oDW4+2dW18f6fRBcebkWD9fz5jI1P7t2247Uzjk/lG/oflAbhk9F3evufLoRRDmJp85YL9m7avHPZ9VE8Q/+DUiB8RvCdQf1PF8Iou+27fq42Nj7p+hi+of9B8Oh8ctDd/2RBZzRY1F6fnZ/5mz2uz+EZ+h8EjZtPPuoa4fPfuBkNNjY+uWdq5+vof85F/4OgET45GNT/ZEEYnW/zVvofA/ofBIvwyUnS/icLwqiD/seI/gdBovPJ2aj9TxZV6ozof4zofxAcbj75q8vy7/+p0s2I/seI/gfBIXxylnf/k0XZw4j+x4j+B0EhfApQZP+TRRnDiP7HiP4HwaDzKZCL/ieLUDsj+h8j+h8EgZtPseqy3P9kEerNiP7HiP4HQSB8CuRD/5NFSGFE/2NE/wPvET4F863/ycL3MKL/MaL/gdfofCwJpf/JwofOiP7HiP4H3uLmY09dAfQ/WSS68BR8M6L/MaL/gbcIH0tC7X/SSpwxBQQR/Y8R/Q+8RPhY9J3PTx1TrE/7shazwXZfRP9jRP8D79D5OPDO3zb0Px6+j8aGIvoi+h8j+h94hZuPG3X19j+eTJdtK+JmRP9jRP8DrxA+DnznCwn6H8JopDCi/zGi/4E3CB9HvvOFlO//IYxShxH9jxH9D7xA5+OYsf/Jgs7IKGqvz86fov/pQf8D57j5uFdXHu//qeCtSBp+Mxobn9wztYP+pwf9D5wjfBxL1P+kVdGX6CTzj07/Y0T/A6cIHw+k7n/SIow0tZP+x4D+B87Q+XjkHV39j9UKpyJ9UdRen12g/+lF/wMnuPn4pa6N/sfqZaUiN6Ox8ck92+h/etH/wAnCxyOPDuh/CKN80P8Y0f/AOsLHM48m7H8Io+zof4zof2AVnY+n3jHi+3/ojAaj/zGi/4E13Hz8VdcI7//hZjQY/Y8R/Q+sIXw8Naj/yYIwOh/9jxH9D6wgfDyWtP/JgjDqoP8xov9B4Qgfzz36hak7JD1c9ONUNoxqta07dr9+1vEpfLNL0mHXh0C5ET5hqCuPz39LoUphRP9j9OaLDh6/0/UhUF6ETwDy7n+yKHsY0f8YfeKig8frrg+BciJ8AlFk/5NFGcOI/sfo8EUHj+9zfQiUD+ETEFv9TxalCCP6H5Ndko64PgTKh/AJTWy//8ki1DCi/zGi/0HuCJ/APHrv1Lxi1b1ZiyVk9bgjhhH9jxH9D3JF+ATo0Xt7+h+fpssJWD9uhgej/zGi/0FuCJ9APXrvgP6HMBr9weh/TOh/kBvCJ2x1Jel/CKNMD0b/Y0T/g1wQPgF79N6M7/8hjBI/GP2PEf0PRkb4BO68/icLwmjgg9H/GNH/YCSETwkM7H+yIIx61Lbu2EX/04P+ByMhfMqjrqLe/xNQEEnFhBH9jxH9DzIjfEoic/+TVmC3Iim/I2/eQv9jQP+DTAifEsml/0mrYmFE/2NE/4PUCJ+Syb3/Sav0YUT/Y0D/g9QIn3Kqy5fPfythGNH/GNH/IBXCp4Ss9T9ZlCSM6H+M6H+QGOFTUo/eO3Us9uj3//QVcBhto/8xof9BIoRPiT1279QdsfRwUM/tQYVRbet2+p9e9D9IhPApv7q6+p9gntfP8DyM6H+M6H8wFOFTco8N6H88f1438/DQm7ZcsHeS/qcX/Q8GInwq4LGE7//x8Hl9OE8Ozft/jOh/0BfhUxGPZXj/jyfP6+k4OzT9jwH9D/oifKqlrhHe/0MYDUb/Y0T/AyPCp0IG9T9ZEEbno/8xov/BeQifikna/2RBGHXQ/xjR/+AchE8FPXbv1B2K9XDRCVHdMKL/MaD/wTkIn+qqS1qwmRBVCiP6HyP6H5xF+FTUY1/s0/8QRoOlODT9jxH9DyQRPpX22BcT9D+E0WBDDkz/Y0T/A8Kn6h77Ysr3/xBG/RkPTP9jQP8DwgeSRnn/D2HU38ZBx8Ym92zbTv/Tg/6n4ggf9O9/siCMjOh/jOh/KozwgaSE/U8WhNFZUzvofwzofyqK8MFZqfufLCodRvQ/BvQ/FUX4oFddI3z+W2oVCyP6HyP6nwoifHCOXPufLCymg6swov8xov+pGMIH5yms/0nLcjrYDCL6HyP6nwohfGBkpf9Jq1Qv0dH/GND/VAjhg0Hqstn/pBV4GNH/GNH/VAThg76c9z9pBRhG9D9G9D8VQPhgIG/6nywCCSP6HyP6n5IjfDCUl/1PFt6GEf2PAf1PyRE+SKoun/ufLDwKI/ofI/qfEiN8kEhw/U8WjsOI/seI/qekCB8kFnT/k4WDMNpG/2NC/1NChA9SeeyLU3fE0sP+fF6aRVbCqLZ1iv6nF/1PCRE+yGJaG/2PD5+X5kxBP/zY2OSerfQ/veh/SobwQWrHvzj1nGJNm/4dYaRcfnj6HyP6nxIhfJDJ8S9NHVGszw57siWMlPmHp/8xov8pCcIHmR3/0tQhSc+c/T8keLIljJTih6f/MaD/KQnCB6Oqq9/7fwijwRL88PQ/RvQ/JUD4YCTHvzT1nGTuf85DGA3W54en/zGi/wkc4YORHf/S1BFJn039HxJGg3X94Nu20/8Y0P8EjPBBLs7rf7IgjAaobZ3aSf/Tg/4nYIQP8lRXnp//Rhidg/7HiP4nUIQPcpOq/8mCMNKmzRfsndxE/9OD/idAhA9ylbn/yaKiYbRtx8/Vxsbof3rQ/wSG8EHucul/sqhMGPH+HwP6n8AQPihKXa5//0+Jw4j+x4j+JyCEDwpReP+TRcnCiP7HiP4nEIQPCmO1/8kiQcr4HkT0P0b0PwEgfFAoZ/1PWsHeiuh/DOh/AkD4wIa6XPc/aQUURvQ/RvQ/niN8UDgv+5+0PA8j+h8j+h+PET6wwvv+Jy0Pw4j+x4j+x1OED6wJpv/Jwoswov8xoP/xFOED2+oKrf/JwlEY0f8Y0f94iPCBVaXof7KwGEb0P0b0P54hfGBd6fqfLAoOI/ofI/ofjxA+cKLU/U8WuYcR/Y8B/Y9HCB+4E6uuWAsevWPTHzmEEf2PEf2PJwgfOHP8y139jy/v2PRVxjCi/zGi//EA4QOnjn+5T/9DGA2WIoy20v+Y0P84RvjAueNfnjoUS88MzBnCaLCBfz61rdvof3rR/zhG+MAXdW28/ydRzhBGg/X8+dD/GNH/OET4wAuPf7n/+38IoxzE0uQm+h8D+h9HCB944/F+/U8Pwii7rdt/bqxWG3d9DN/Q/zhA+MArj385/ft/CKM0alu27dzHy2/nov9xgPCBj+oa4fPfCKPBxse37N287bXl/3y9dOh/LCN84J1B/U8WhNH5Nm/Zu2ticsr1MXxD/2MR4QMvJe1/siCMOrbtuLBJ/3Me+h9LCB94K0v/k0V1w4j+x4D+xxLCB76ry/Lv/6lSGNH/GNH/WED4wGt59z9ZlD2M6H+M6H8KRvjAe0X2P1mUMYzof4zofwpE+CAItvqfLBJljPdBRP9jQP9TIMIHIanLcv+TVsi3IvofI/qfghA+CIYP/U9aoYUR/Y8R/U8BCB8Exbf+J60Qwoj+x4j+J2eED4Ljc/+Tlp9hRP9jQP+TM8IHoarL8/4nC1/CiP7HiP4nR4QPghRi/5OFyzCi/zGi/8kJ4YNgPf7lqSOKw+1/srAdRvQ/RvQ/OSB8ELTHvzJ1SLGe8WUtZlvxYUT/Y0D/kwPCB2VQV3f/49F02bYiwoj+x4j+Z0SED4L3+FeG9D+E0chhRP9jRP8zAsIHpfD4V1K8/4cwyhRG9D9G9D8ZET4ojce/kvH9P4RRwjCqbdm2g/6nB/1PRoQPyqauUd//Qxj1/dHHx7fs3byV/qcH/U8GhA9KZWj/kwVhdM6PvnnL3l0TE/Q/Peh/UiJ8UDqp+p8sKhpE0qs/9lb6HxP6nxRqcVzB/x+ESnjrrzWelvRmSarZelBrD+Re1G7ONBZ/uNf1OZKqjW0a+jVjtclh30W1sf5f026dfuQH3/jFd6c8WiVNuD4AUKC6pKcl7er9K1ZhGWHtgdwb2+h/Vlde3mX697XauDTkdlTTmIbeoGoTqtUGv0hTq03I9R92u7X4hz/4xi/+gdNDBISbD0rtrb/WqEu6b9jXcTPKbq05r6i97voYTrVbi//p+w++6XddnyMkdD4otScS9j/WNgUlHC9s2rxLtVoJUzWhdmvxswRPeoQPSu+JDO//IYxSqNU0udn4ylvptVuL/yWKmp90fY4QET6oirpGeP8PYTTY2PikJjZVa37dbi3+9yhq3vrsQ28P6P+l/EH4oBKeyPn9P4TR+SYmt2lsfNharByiqHlPFDVvIXiyI3xQGUn7nywIo44q9D9R1PxCu7X44Wcfenvb9VlCxtoNldP9/h8brD4Ve/C8H7XXtdacd32MQkRR88vt1uJvEzyj4+aDKqpr1M9/S8HqZcWDm1FZ+58oan613Vr8HYInH4QPKifv/ietKoRR2fqfKGr+n3Zr8Teffejta67PUhaEDyqpyP4nrbKGUVn6nyhqfr3dWvx1gidfdD6oNNv9TxYhd0ah9z9R1Hy43Vq87tmH3r7s+ixlw80HVVeXxf4ni5BvRiH3P3HceqTdWnovwVMMwgeV9sRXpp5TrGlfp8smoYVRiP1PHLeOt9bn3vvsQ29ruD5LWRE+qLwn7uvqfzxYi6UVQhiF1P/Eceup1vrcVc8+9Davb8Sho/MBNrz1VxP0P2E8f57Dl84ohP4njlt/2VqfO/jsQ2875fosZcfNB3hVXcP6H25GyR+sh+/9Txy3vtdan7uC4LGD8AE2PHFfhvf/EEapHsjX/ieOW3+9ETwnXZ+lKggfoMs5/U8WhNHQB9q0ya/+J45bf7cRPC+5PkuV0PkABon6nyz8ec5NrIgjR5Ev/U/8XGt97tIffOOSn7g+SdVw8wHM6iri/T/cjCRJY2OTmph03f/EP22tzx0keNwgfACDTP1PFhUOI7f9T/z3rfW5y37wjUt+5OgAlUf4AH2M3P9kUbEwctP/xC+31ueu+ME3Lvk7yw+MLnQ+wBCF9T9pBdgXScOPbbf/iU9uvNT2V5YeEH1w8wGGq8uHz38L8FYkDT+2vf4nnm2tz11J8PiB8AGGsNb/pFWCMDqj+P4nXmitz131g29c8kyBD4IUCB8gASf9T1oBhlH3cSeL638W2q3Fa37wjUueLOKbIxs6HyCFt3T1P8FVMAEcOIrWtZ5v/9Notxav+f6Db/pWnt8Uo+PmA6RT10b/E9xFI4AD59z/LLdbi79M8PiJ8AFSeHJA/xPAc/u5PD3weD79T7PdWvzV7z/4pmM5HAkFIHyAlJ5M2P94+tzen0cHnty0S8re/6y1W4u//v0H3/S1PM+EfNH5ABm9ZYT3/wRQv5zP8qEz9j9r7dbib3//wTcdyf9EyBM3HyC7ujK+/8ejS0Zylg+dof9pt1uL7yN4wkD4ABkN6n/SIozMUvQ/7ai9/IHvP/imLxVzEuSN8AFGkLT/SYswelWC/ieKouZN3/v6G/48v0dF0eh8gBy8pd54WjV7n/9Wtc5oQP8TR1Hz5u997aI/zv7d4QI3HyAfdcVasHVdqdrNqE//E0dR83aCJ0yED5CDJ48Y+h+LCVGFMOrtf6Ko+S+/97WL/nNRx0OxCB8gJ08eGdL/EEaDJTj0mf4nipq/972vXXSnvcMhb3Q+QM7eUs/4/h+LRU7InVG7tfL73/3q3v/g9jAYFTcfIH91ZXn/DzejwWIpjtqnx8e33uX6KBgd4QPkzNj/ZEEY9YjbcdR+1xP3TS27PglGR/gABRja/2RR6TCK23HUvuypr17wl65PgnzQ+QAFytz/ZFHazqgTPE/ev+ubVh8WheLmAxSrroyf/5ZaKW9GBE9ZET5AgXLrf7IIPowInjIjfICCFdL/ZBFUGBE8ZUfnA1hitf/JwpvOKI7jqL2f4Ck3bj6APXXZ6n+ysDhvG3Qzitrr/4rgKT/CB7DEaf+TluWt9ZmHiaL1Tz311Qs+U/wjwjXCB7DIm/4nLQthFEfrn3rq/t3/rpjvDt/Q+QAOeN//pDV6X3Tjk/dNHR79IAgFNx/AjboUr7s+RG5GuxkRPBVE+AAOdPqf2m+5PkdhkocRwVNRhA/gSLD9TxbmMCJ4KozOB3Dsko3+J8jfsZPdp588MnWH60PAHW4+gHt1SQv+fZJ0Ye4heED4AI491ef9PyUNo3uePDI17foQcI/wATzw1JGpI4r12UFJU4IwInhwFp0P4JFLru/z/p8EhZDnnRHBg3Nw8wH8Upfp898SXHs8vhkRPDgP4QN45Kn7E37+WzhhRPDAiJfdAA9dcn3jTkmfyPwN/HiZ7uEnj0wdKP5hECJuPoCHnrp/6pCkZzJ/A/c3o2fUeQkRMCJ8AH/Vldfv/7EbRs9IOvDkkan50b4NyozwATyVuP/JorgwIniQCOEDeOyp+y19/ls+YUTwIDEGB0AA+r7/x5bh64RnagQPUuDmA4Shrrz6nywGX3u48SA1wgcIQKH9TxavBtHzinXgKYIHKRE+QCCs9T/JLUiqP3U/wYP06HyAwDjvfzoWJB146v6ppx2fA4Hi5gOEpy6X/Q/BgxwQPkBgHPc/BA9yQfgAAXLU/xA8yA2dDxAwi/0PwYNccfMBwlaXnf6H4EGuCB8gYJb6nxsJHuSN8AECV3D/c+NT908dLuh7o8LofICSKKD/IXhQGG4+QHlMK7/+h+BBoQgfoCQ2eplDOXwrggeFI3yAEtkIjXtG+BYED6wgfIDyOaTOrzlI65MED2xhcACU0CXXNy6WdEzSroT/yT1P3T81XdR5gF7cfIASStn/EDywjvABSiph/0PwwAnCByi3Q+rf/xA8cIbwAUps47eMTuv89/8QPHCK8AFKztD/EDxwjrUbUBGXXN84LEkED3zw/wHOgQjPvtdsMQAAAABJRU5ErkJggg==');


