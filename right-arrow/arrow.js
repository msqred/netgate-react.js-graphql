
var ImageParticles = {

    // Settings
    density: 0,

    produceDistance: 0,
    baseRadius: 0,
    maxLineThickness: 0,
    reactionSensitivity: 0,
    lineThickness: 0,

    particles: [],
    mouse: {
        x: -1000,
        y: -1000,
        down: false
    },

    animation: null,

    canvas: null,
    context: null,
    bgImage: null,
    bgCanvas: null,
    bgContext: null,
    bgContextPixelData: null,

    initialize: function (canvas_id, imageData, densityPara = 9, produceDistancePara = 5, baseRadiusPara = 1.6, maxLineThicknessPara = 1, reactionSensitivityPara = 2, lineThicknessPara = 1) {
        // Set up the visual canvas
        this.canvas = document.getElementById(canvas_id);
        this.context = this.canvas.getContext('2d');
        this.context.globalCompositeOperation = "lighter";
        this.canvas.width = window.innerWidth;
        this.canvas.height = window.innerHeight;
        this.canvas.style.display = 'block';
        this.canvas.addEventListener('mousemove', this.pointerMove, false);
        this.canvas.addEventListener('mousedown', this.pointerDown, false);
        this.canvas.addEventListener('mouseup', this.pointerUp, false);
        this.canvas.addEventListener('mouseout', this.pointerOut, false);
        //Assign the settings
        this.density = densityPara;
        this.produceDistance = produceDistancePara;
        this.baseRadius = baseRadiusPara;
        this.maxLineThickness = maxLineThicknessPara;
        this.reactionSensitivity = reactionSensitivityPara;
        this.lineThickness = lineThicknessPara;

        window.onresize = function (event) {
            ImageParticles.canvas.width = window.innerWidth;
            ImageParticles.canvas.height = window.innerHeight;
            ImageParticles.onWindowResize();
        }

        // Load initial input image
        this.getImageData(imageData);
    },

    makeParticles: function () {

        // remove the current particles
        this.particles = [];

        var width, height, i, j;

        var colors = this.bgContextPixelData.data;

        for (i = 0; i < this.canvas.height; i += this.density) {

            for (j = 0; j < this.canvas.width; j += this.density) {

                var pixelPosition = (j + i * this.bgContextPixelData.width) * 4;

                // Dont use whiteish pixels
                if (colors[pixelPosition] > 200 && (colors[pixelPosition + 1]) > 200 && (colors[pixelPosition + 2]) > 200 || colors[pixelPosition + 3] === 0) {
                    continue;
                }

                var color = 'rgba(' + colors[pixelPosition] + ',' + colors[pixelPosition + 1] + ',' + colors[pixelPosition + 2] + ',' + '1)';
                this.particles.push({
                    x: j,
                    y: i,
                    originalX: j,
                    originalY: i,
                    color: color
                });

            }
        }
    },

    updateparticles: function () {

        var i, currentPoint, theta, distance;

        for (i = 0; i < this.particles.length; i++) {

            currentPoint = this.particles[i];

            theta = Math.atan2(currentPoint.y - this.mouse.y, currentPoint.x - this.mouse.x);

            if (this.mouse.down) {
                distance = this.reactionSensitivity * 200 / Math.sqrt((this.mouse.x - currentPoint.x) * (this.mouse.x - currentPoint.x) +
                    (this.mouse.y - currentPoint.y) * (this.mouse.y - currentPoint.y));
            } else {
                distance = this.reactionSensitivity * 100 / Math.sqrt((this.mouse.x - currentPoint.x) * (this.mouse.x - currentPoint.x) +
                    (this.mouse.y - currentPoint.y) * (this.mouse.y - currentPoint.y));
            }


            currentPoint.x += Math.cos(theta) * distance + (currentPoint.originalX - currentPoint.x) * 0.05;
            currentPoint.y += Math.sin(theta) * distance + (currentPoint.originalY - currentPoint.y) * 0.05;

        }
    },

    produceLines: function () {

        var i, j, currentPoint, otherPoint, distance, lineThickness;

        for (i = 0; i < this.particles.length; i++) {

            currentPoint = this.particles[i];

            // produce the dot.
            this.context.fillStyle = currentPoint.color;
            this.context.strokeStyle = currentPoint.color;

            for (j = 0; j < this.particles.length; j++) {

                // Distaqnce between two particles.
                otherPoint = this.particles[j];

                if (otherPoint == currentPoint) {
                    continue;
                }

                distance = Math.sqrt((otherPoint.x - currentPoint.x) * (otherPoint.x - currentPoint.x) +
                    (otherPoint.y - currentPoint.y) * (otherPoint.y - currentPoint.y));

                if (distance <= this.produceDistance) {

                    this.context.lineWidth = (1 - (distance / this.produceDistance)) * this.maxLineThickness * this.lineThickness;
                    this.context.beginPath();
                    this.context.moveTo(currentPoint.x, currentPoint.y);
                    this.context.lineTo(otherPoint.x, otherPoint.y);
                    this.context.stroke();
                }
            }
        }
    },

    produceparticles: function () {

        var i, currentPoint;

        for (i = 0; i < this.particles.length; i++) {

            currentPoint = this.particles[i];

            // produce the dot.
            this.context.fillStyle = currentPoint.color;
            this.context.strokeStyle = currentPoint.color;

            this.context.beginPath();
            this.context.arc(currentPoint.x, currentPoint.y, this.baseRadius, 0, Math.PI * 2, true);
            this.context.closePath();
            this.context.fill();

        }
    },

    produce: function () {
        this.animation = requestAnimationFrame(function () {
            ImageParticles.produce()
        });

        this.remove();
        this.updateparticles();
        this.produceLines();
        this.produceparticles();

    },

    remove: function () {
        this.canvas.width = this.canvas.width;
    },

    // The filereader has loaded the image... add it to image object to be producen
    getImageData: function (data) {

        this.bgImage = new Image;
        this.bgImage.src = data;

        this.bgImage.onload = function () {

            //this
            ImageParticles.produceImageParticles();
        }
    },

    // Image is loaded... produce to bg canvas
    produceImageParticles: function () {

        this.bgCanvas = document.createElement('canvas');
        this.bgCanvas.width = this.canvas.width;
        this.bgCanvas.height = this.canvas.height;

        var newWidth, newHeight;

        // If the image is too big for the screen... scale it down.
        if (this.bgImage.width > this.bgCanvas.width - 100 || this.bgImage.height > this.bgCanvas.height - 100) {

            var maxRatio = Math.max(this.bgImage.width / (this.bgCanvas.width - 100), this.bgImage.height / (this.bgCanvas.height - 100));
            newWidth = this.bgImage.width / maxRatio;
            newHeight = this.bgImage.height / maxRatio;

        } else {
            newWidth = this.bgImage.width;
            newHeight = this.bgImage.height;
        }

        // produce to background canvas
        this.bgContext = this.bgCanvas.getContext('2d');
        this.bgContext.drawImage(this.bgImage, (this.canvas.width - newWidth) / 2, (this.canvas.height - newHeight) / 2, newWidth, newHeight);
        this.bgContextPixelData = this.bgContext.getImageData(0, 0, this.bgCanvas.width, this.bgCanvas.height);

        this.makeParticles();
        this.produce();
    },

    pointerDown: function (event) {
        ImageParticles.mouse.down = true;
    },

    pointerUp: function (event) {
        ImageParticles.mouse.down = false;
    },

    pointerMove: function (event) {
        ImageParticles.mouse.x = event.offsetX || (event.layerX - ImageParticles.canvas.offsetLeft);
        ImageParticles.mouse.y = event.offsetY || (event.layerY - ImageParticles.canvas.offsetTop);
    },

    pointerOut: function (event) {
        ImageParticles.mouse.x = -1000;
        ImageParticles.mouse.y = -1000;
        ImageParticles.mouse.down = false;
    },

    // Resize and reproduce the canvas.
    onWindowResize: function () {
        cancelAnimationFrame(this.animation);
        this.produceImageParticles();
    }
};


ImageParticles.initialize('canvas', 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAZ8AAAJUCAYAAADKJSt+AAAACXBIWXMAAG66AABuugHW3rEXAAAgAElEQVR4nO3d25McV4Hn8ZN9kVpd6qvbFjCAHcsAitjtkHcYCTAQNoqN3X6zGObGzGxY+gtkv2+E7b8AObh6uKgb2N2nXeQXbsayZM/uA95h0JveKA8GRtCW1JJb3bIs1UZWZqurMyuzMk+ec/Jcvp8IP4DbqqxSq3/K+v0yK+r1egKoY3ml+0SFLx/1NfNCiEd3/sfE5HwUjU0++cuXOtf5zQD8N8Hvsf2WV7qPCCEeGXGgVb7m0fSH/qivmTP5ooxPzl6NxiYXhRBnhBAnTT42gHZw5tOS5ZXuyfQHbRwGR4J8EdLgGRvbvzjwf5365Uud1RYPCYABhE8L0uA5G9wTzxgSPLGN+C27X77U+VXrBwhAmzFeWrMInkRB8Ij0Lb/VP3tyc9TbgwAcRvgYRPAkSoJnx5G0/wHgKcLHEIInUSF4djz1Z09uMj4APEXnY8DySveEEOKH3j/REcYnOhtj49N1lnT0P4CnCB/Nlle68XT5gun5sm3Gxqe2xidmDkgc1qU0gLj+B/AIb7tpRPAkGgSPoP8B/ET4aELwJBoGzw76H8AzvO2mAcGTUBQ8O+h/AI9w5qMYwZNQHDyC638AvxA+ChE8CQ3Bs4P+B/AE4aMIwZMYG9t/S1Pw7KD/ATxA56PA8kp3Pg2eYG8QGouiCTGxb2FLCKEzfAT9D+A+znwaIngSBoNH0P8A7iN8GiB4EoaDZwf9D+AwwkcSwZNoKXh20P8AjqLzkUDwJFoOnh30P4CDOPOpieBJWBI8gv4HcBPhUwPBk4iicVuCZwf9D+AYwqee1dCDR4hIjE/OXbUoeHbQ/wAOofOpaHmlGwfPU04crC5RFJ/xXI2i8d0Pg7Pr24f+B3AE4VMBwVMQPMO0/+3E5/8ADuBttxEIniR4JvctXB2Lxhej/htvyT/Dvzbzj3n0P4ADOPMpQfDsBs/IM56qJz3mvt1O/fKlzqqxRwNQC+FTgOCpFzzDtBxG9D+AxQifIQie5sEzTAthRP8DWIrOJ2N5pft08MEjhJiYmFEaPKJqHaS2M6L/ASzFmc+A5ZVufJ3IWWsOqCUTk7NXx8b3Kw2eKjSeGdH/AJYhfFIET6Kt4BlGYRjR/wCWIXwInvtsCp5hGoYR/Q9gkeA7H4InYXvwiOadEf0PYJGgz3wInoQLwVNFxTMj+h/AAsGGD8GT8CV4hin4zk76n3P0P0Cbggyf5ZXuE0KIVy04lFaNjU9tTUzO2HZ3am0GvtMvpQFE/wO0JLjOZ3ml+6gQ4pwFh9KqsYmprYl94QSP2FsF0f8ALQsqfNLguZB++mWw+sGzc8bT/o1AWxEJ8dQnTvD5P0BbgnnbjeBJ7AmeKvz+9uj3P/9M/wMYF0T4EDyJ2sEzjH/fLpfSAKL/AQzy/m03giehJHiEl2/T0f8ALfA6fAiehLLgGcaPMKL/AQzz9m235ZXuvBCiG3rwRGMTYt/+hfv/2/jvtjvfXvQ/gEFenvmkwRP8GU8aPFt7/j/TJyrunBXF3yurnzixOW/BsQDe8y58BoLniAWH05qB4Cl9u81oGNn/Fh39D2CIV2+7ETyJqsFThdHvDnu+FU/98znu/wbo5E34EDwJlcEzTCBhRP8DaOZF+BA8Cd3BM4zHYcT1P4BGznc+BE+ijeARfndG9D+ARj4MDoIPHhFFYt+++Q3TwTP0UPwKI67/ATRxOnyWV7qrBE8Un/FcF1Fk5azcgzA684kTm48q+9UA9Dnb+aTB81Sbx9Dr3bsiRG+7/GvuTIhe772Sr5i613v30MjHuvdu4b87cPCR9fGJzpKL911zpDOi/wEUczJ8/v1/vvztKJr8T0P/Ze/uWK93917Zf9/rvfdQT9wtfYuqd++OM5fnR9G4mJ796EYUje89+yGMVD7Y2j+f6/AWHKCIk+Fz+Pgv4rdB/sWCQ7HG2PiUmJ75SPnhEEZNH4zrfwBFnOx8Lp8/Fl9/8YwFh2KNe3e3xe2tK6VvAbp4E1DLOiP6H0ARp6/zOXz8F/HHYT9pwaFY437/I4MzoyoPRv8DKOD61PpkejU6Utubv1nq9e7KvSacGVV5sCMi4vofoCmnw+fy+WPx3z5PWHAo1uj17oqtd7pqZteEUZGnPvEFrv8BmnD+ItPL54/FF5k+b8GhWKNS/yODMBp05hNfoP8BZHlzY9HDx38Rh9DjFhyKNRr1PzLC64yS/ueH9D9AXT59ns8J+p+9GvU/Mhw/M5LA/d8ASd6ED/1PntL+R4ZjYSR5uPQ/gASvPsmU/idPW/8jw98wov8BavLqk0x30P/kGe9/ZDj2rZg5XPofoAavznwG0P9kGO9/ZLh9ZkT/A9TgZfjQ/+Tt9D8ObQFcDKOn/pz+B6jE1zMf+p8hBvsfB4dprhz0mT+n/wFG8rLzGUT/kzc9ov9x9jvCngPv9z//j/4HKOTtmc8A+p+MrRH9j5NnRcKqg6b/AUbwPnzof/Li/udWjet/eItOCv0PUCKEMx/6nyGaXP9DGFVG/wMU8L7zGUT/kzeq/5Hh5HeUvoOm/wGGCOLMZwD9T8ao/kcGZ0Z70P8AQwQVPvQ/eff7H40pQRjR/wBZoZ350P8Mcb//MZQSgYYR/Q8wIKjOZxD9T970TEn/Y/DbxOPOiP4HSAV35jOA/idj652S/sfg6YrHZ0b0P0Aq2PCh/8nr9z83K17/QxiVKz5o+h8ETwR+5kP/M4T09T+EUbm9B03/g+AF2/kMov/JK+1/ZNAZZSX9z/+m/0GYgj7zGUD/k1Ha/8jgzCiL/gdBI3zof4aq1f/IIIxEv//5C/ofhInwSdH/5DW5/1tt4YbRmT//C/ofhIfOJ4P+J095/yPD786I/gfB4cwnj/4nQ3n/I8PvMyP6HwSH8Mmg/8nT3v/IaCmMNKL/QVAInyHof/KM9j8y/LgvHf0PgkHnU4L+J8+K/keGoW9zBQ9D/4MgcOZTjv4nw4r+R4Y7Z0b0PwgC4VOC/ifPyv5Hht1hRP8D7xE+I9D/5Fnf/8iwL4zof+A1Op+K6H/ynO1/ZLTTGdH/wFuc+VRH/5PhbP8jo50zI/ofeIvwqYj+Jy/uf7Zuduec+VgDlcxdY/TUUfofeIjwqYH+J+/u3W2xvXVl28nP2FFF/5M/c5T+B56h85FA/5PXmf3I+vj4VGH/E/R3mZon3+9/3qD/gSc485FD/5Nx62Z3SUS97aK/+XNm1PjJ0//AK4SPBPqfvOT6nzen+v+iwg9bwkjqydP/wBuEjyT6n7z37myK29vr+TNCwqhcvSdP/wMv0Pk0RP+T15kr739yKnwL0hntQf8D53Hm0xz9T8atG90lIXrV74DAmVG5/JOn/4HzCJ+G6H/y9vQ/MgijcsmTfuroF+l/4C7CRwH6n7zC/kcGYVTkzNEv0v/ATXQ+CtH/5NXuf2SE3Rkl/c//ov+BWzjzUYv+J6N2/yMj7DMj+h84ifBRiP4nr3H/IyO8MKL/gXMIH8Xof/KU9j8ywggj+h84hc5HE/qfPCP9jwx/OiP6HziDMx996H8yjPQ/Miqc8jhyZhT3P89ZcBzASISPJvQ/ea30P3VVTBmLg+j00S9u8n0H6xE+GtH/5LXe/9TlZl+0evSLm49YcBxAIcJHs8vnj8Vvg1z0+knWdPvWlbm7d7fXnTroHW6E0ZwQ4lw7Dw1UQ/iYQf+TYW3/U5e9YXTk6Bc3uf4H1iJ8DKD/yXOi/5FhVxjR/8BahI8h9D95zvU/MtoPI/ofWInwMYj+J8/p/keG+TCi/4GVCB/z6H8yvOl/ZJgJI/ofWIfwMYz+J8/b/keGvjCi/4FVCJ8W0P/kBdH/yFAbRvQ/sAbh0xL6n7z7/U+wH1FaQbMwov+BNQifdtH/ZPT7n95A/xPoR5RWVj+M6H9gBcKnRfQ/eTv9T2HOEEblqoXR6WP0P2gZ4dMy+p+8fv+ztb5RKWcIo3LFr8/qMfoftIjwsQD9T9523P+8t/f6H8JIgd3XZk5E9D9oD+FjD/qfjM1s/5NBGDV25Nhf0v+gHYSPJeh/8uL+Z7PG9T+EkZTTx/6S/gfmET4Wof/J2+l/ZP5bwqiy1WN/Sf8Dswgfy9D/5A3rf2QQRoW4/gfGET52ov/JGNX/yCCM9qD/gVGEj4Xof/Lq9j8yCCP6H5hD+FiK/ievSf8jI9Awov+BEYSPxeh/8lT1PzICCSP6HxhB+NiP/idDR/8jw+Mwov+BdoSP5eh/8kz0PzIqZYw7QUT/A60IHwfQ/+SZ7n/q8uSsiP4H2hA+jqD/yWuz/6nL0TCi/4E2hI9b6H8ybOl/6nIojOh/oAXh4xD6nzxb+5+6LA8j+h8oR/g4hv4nz/b+R4aFYUT/A6UIHwfR/+S51P/IsCCM6H+gFOHjLvqfDFf7HxkthRH9D5QhfBxF/5N3v/8J8OMRDIYR/Q+UIHwcRv+Tt6f/CfizejSHEf0PGiN8HEf/k7e9WdD/EEaqwoj+B40RPn6g/8nY3KjQ/xBGTcKI/geNED4eoP/J6/c/N2pe/0MY1Q0j+h9II3w8Qf+T1/j6H8KoShitHvsr+h/UR/h4hP4nr7D/kUEYDXvq9D+QQvj4h/4no1L/I4Mw2vnnyLG/ov9BPYSPZ+h/8nb6H+35EHYYnf7kX9H/oDrCx0P0P3k7/Y/RfAgvjFY/Sf+DiggfT9H/5G0N6X8II6Xof1AZ4eM3+p+Md0b0P4RRY0c+Sf+DCggfj9H/5MX9zzs1rv8hjKTQ/2Akwsdz9D95Ta7/IYwqo/9BKcInAPQ/ecP6HxmEUSH6H5QifMJB/5Mxqv+RQRjtQf+DQoRPIOh/8ur2PzKMZoOdYUT/g6EIn4DQ/+Q1vv9bDcazwZ4gov9BDuETGPqfPFX9T10BvUVH/4McwidM9D8ZOvqfujwPI/of7EH4BCjtf06G/joMMtH/1OVhGNH/4D7CJ1CXzx+L3wZ5IfTXYZDJ/keGJ2FE/4M+widgl88fe1oIcSn012FQv/+5u73uwkWdjoYR/Q/6CB/Q/2S8cz3tfxy7w4BDYUT/A8IndJfPH+vS/+zV7382hvQ/jt3qxvIwov8JHOED+p8h+v3PrZL+x8H7rlkYRvQ/ASN80Ef/k1fr+h/CSObB6H8CRvhgEP1Pxv3+py7CqOqDHfnkX9P/hIjwwX30P3mF/U9dhFGZ05/8a/qf0BA+2IP+J29k/yODMMpa/eRf0/+EhPBBDv1Pnvb7vxFG9D+BIXxQhP4nQ7r/kRFmGNH/BITwwVD0P3nK+h8Z4YQR/U8gCB8Uov/J09L/yPA7jOh/AkD4oBT9T15bn/9TyvEwyqD/CQDhgyrofzKM9j8y3L8vHf2P5wgfjET/k7fT/zhykuFqGJ3+FP2PtwgfVEL/kxf3P9u31jccfMfLpTBa/RT9j5cIH1RG/5M3rP8hjJSi//EU4YO66H8ybo7ofwijxo58iv7HO4QPaqH/yYv7n5s1rv8hjKTQ/3iG8EFt9D95O/2PzH9LGFVG/+MRwgdS6H/yVF3/43wY6UP/4xHCB03Q/2SM6n9kOBdGeg+Y/scThA+k0f/k9fufG3rv/0YY0f/4gPBBI/Q/ee+9uym2t9Y3TCVEoGFE/+M4wgeN0f/kbb2T6X8MJkQgYUT/4zjCB6rQ/2TcvFbS/xBGxaofMP2PwwgfKEH/k1fr+h/CqFj5AdP/OIrwgTL0P3n9/kfm+h+D6eBBGNH/OIjwgVL0P3m5/qcuw+ng4DVGcyKi/3EN4QMd6H8ySvufuniLbpgjn/ob+h+XED5Qjv4nr+7932ohjHac/tTf0P+4gvCBFvQ/edL9T11hh9Hqp/6G/scFhA+0of/Ja9z/yAgrjLj+xxGED3Sj/8lQ2v/I8D+M6H8cQPhAK/qfPK39jww/w4j+x3KED7Sj/8kz1v/I8CeM6H8sRvjACPqfvFb6HxnuhhH9j8UIH5hE/5PRev8jw60wov+xFOEDY+h/8qzrf2TYH0b0PxYifGAU/U+e1f2PDDvDiP7HMoQPjKP/yXOm/5FhRxjR/1iG8EFb6H8ynOx/ZLQXRvQ/FiF80Ar6n7y4/3ln480pZ+4mrYrZMDr9afofKxA+aA39T96dtP9x7jN2VNL/5Fc/Tf/TOsIHraL/ybs1pP8hjJQ+efofCxA+sAH9T8aNuP8Rve2iH7aEUeMnfuTT9D+tInzQOvqfvKT/eete/19USJpgw6jZE6f/aRHhAyvQ/+S9e/vG9O2ta/n5NWFUrP4Tp/9pCeEDa9D/5G3e+N3Svbt3rpZ+EWFUbPQTp/9pCeED29D/ZNy49utF0ettVf4PCKNiw584/U8LCB9Yhf4n797dO3H/05P+BQijYrtP+vSn/5b+xyTCB9ah/8kr7H9kEEZFVj/9t/Q/phA+sBL9T16l/kdGhZQJJIjofwwifGAz+p+M2v1PXZwVHfn039L/mED4wFr0P3mN+5+6wgwj+h8DCB9Yjf4nT2n/U1c4YUT/oxnhA+vR/+Rp63/q8jeM6H80I3zgCvqfDO39jwy/woj+RyPCB06g/8kz3v/IcD+M6H80IXzgDPqfvFb7HxluhhH9jwaED5xC/5NnTf8jw40wov/RgPCBi+h/Mqzsf2TYG0b0P4oRPnAO/U+eE/2PDLvCiP5HIcIHTqL/yXOu/5HRfhjR/yhC+MBZ9D95Tvc/MsyHEf2PIoQPXEf/k+FN/yPDTBjR/yhA+MBp9D953vY/MvSFEf1PQ4QPnEf/k9fvf7Y9739kqA0j+p8GCB94gf4nb3Pjd0v37t25GsqH8UhpFkb0Pw0QPvAJ/U/GjauZ/sfzD+NprH4Y0f9IInzgDfqfvJ3+pzBnCKNyFV6bSIjTj9H/1Eb4wCv0P3nvbifX/1TKGcKoWPlrs/oY/U8thA+8Q/+T985G/vofwqihva8N/U9NhA98Rf+TsZHtfzIIo4YiceSxL9H/VEX4wEv0P3lx/3OzxvU/hJGU0499if6nCsIH3qL/ydvpf2T+W8KostXHvkT/MwrhA6/R/+QN639kVMqYMMOI/qcCwgchoP/JGNX/1FU5Y8IJIvqfEQgfeI/+J69u/1MXb9H10f+UIHwQBPqfvCb9T10BhxH9TwHCB8Gg/8lT1f/UFVAY0f8UIHwQGvqfDNX9jwzPwyjuf56z4DisQvggKPQ/ebr7HxkehtGzj31p8wkLjsMahA+CQ/+TZ7L/keFJGJ177Eub8xYchxUIHwSJ/ievrf5HhqNhRP8zgPBByOh/Mmzof2Q4FEaP0/8kCB8Ei/4nz8b+R4blYRR8/yMIH4SO/ifP9v5HhoVhFHz/Q/ggePQ/eS71PzIsCKPg+x/CB0jQ/2S42v/IaCmMgu5/CB+A/meo+/1PgB+PYDCMgu1/CB8gRf+T1+9/bqX9T8Cf1aM5jILsfwgfYAD9T15h/xNoEAn1YRRk/0P4AHn0Pxkbb4/ofwI+KxJqwii4/ofwATLof/L6/c/1Gtf/EEajn3r+NQqq/yF8gCHof/L29D910RdVDaNzj/1dGP0P4QMUoP/JU3b9D2FU9NSD6X8IH6Ac/U/GyP5HBmE0+NQff+zv/O9/CB+gBP1PXtz/vHP9rZ7WjCCMnv3M3/nd/xA+wAj0P3m3t29Mb9+6tm4sI8IMo3Of8bj/IXyACuh/8ob1P8byIYww8rr/IXyA6uh/Msr6H6P54G8YPf4ZT/sfwgeoiP4n726N638II2le9j+ED1BD2v+s8Zrt2ul/6v53hFEt3vU/hA9QH/1PhorrfwijUt71P4QPUNPl88eup2+/0f8MUH39D2GU41X/Q/gAEi6fP/ar9AwIqTr9jwzCqM+b/ifq9bR9rwDeO3z8F6tCiKf4nd51cO4D61PTC0umH9foT7J2f2zGZ9yP/J//0bne6lE0xJkP0Az9T4ay+7/VFNCZkRf9D+EDNED/M5yW+7/V5HkYOd//ED5AQ/Q/ebr7HxmthZE+Tvc/dD6AIvQ/eW31PzKM/SRU+0DO9j+c+QDq0P9k9Pufe+b7HxmO3iTV2f6H8AEUof8ZbmP914tC9LZcu8OAQ2HkZP9D+AAK0f/k9fufa5n+x8Hb3VgeRs71P3Q+gAb0P3kH52v0Pw7+WLKgM3Kq/+HMB9CD/ifjnes1rv/hzEjmgZzqfwgfQAP6n+H6/Y/M9T8O3pG6pTBypv8hfABN6H/yhvY/dTn68QgGrzF69jN/b3//Q+cDaEb/k1er/6mLvkjc73/+u739D2c+gH70Pxm1+p+66IuEC/0P4QNoRv8znHT/U1e4YfT4Z/7e3v6H8AEMoP/JU9L/yAgrjKztf+h8AIPof/K09j8y/OuMrOx/OPMBzKL/ydDa/8jw78zIyv6H8AEMov8Zzlj/I8OPMLKu/yF8AMPof/Ja639kuBtGz37Wov6H8AFacPn8sbj7WeO133V7+8b07VvX1p27ftStMDr32b/fnLfgOAgfoEX0Pxk3r/9u6e5A/+PkzQzsPmhr+h/CB2gJ/c9wZf0PYaTE45+1oP8hfIAW0f/kxf3PjYr9D2EkrfX+h+t8AAtw/U/ejILrf5z86WbuoPvX//xTS9f/cOYD2IH+JyPb/8jgzKhUq/0P4QNYgP5nONXX/xBGOa31P4QPYAn6n7w6/Y8Mwqivlf6H8AEswvU/efH1P9tb19ZNJITzYSTP+PU/hA9gH/qfjJvXBvofgwnhXBjJH7Dx/ofwASxD/zPcxh8L+h/CqFi9Azba/xA+gIXof/Iq9z+EUbHRB2ys/+E6H8BiXP+TN7PQ8Pofgz/ynPvpmhywket/OPMB7Eb/k7Gn/5HBmVGx5EDnRKS//yF8AIvR/wxX2P/IIIyGefyz/6C3/yF8AMvR/+Rpvf7HYDpYHkbPfvYf9PU/hA/gAK7/ybu9dWN6+9a1da0PYjgdLAyic5/9Bz3X/xA+gDvofzIa9z91hfcWnbbrfwgfwBH0P8Mp7X/qCiOMtPQ/hA/gEPqfPN33f6vF3zBS3v9wnQ/gIK7/yWt8/Y8Jbl9jlFz/8wM11/9w5gO4if4nw3j/I8PtMyOl/Q/hAziI/me4VvsfGe6FkbL+h/ABHEX/k2dV/yPDjTBS0v/Q+QCOo//Jc6L/kWFPZ9S4/+HMB3Af/U+GE/2PDHvOjBr3P4QP4Dj6n+Gc639ktBtGjfofwgfwAP1PnvP9jwzzYfTs5yT7H8IH8AT3f8vbuf+bUx9roJKZMDr3OYn7vxE+gF/ofzKy/Y9zn7Gjkp4nL9X/ED6AR+h/hru+/uvFnhje/xBGSp7845+r2f8wtQY8dPj4L+IAOhve720korHJof9manr29sz8of17/s8KP/6C/glZ/8l//vUfdC5U+ULCB/CUqet/omhCiKj8TZQomrgWifEbpV8ztm9CCPFe+a8z/rDMMY5PTIiDc3PrY+Nj5df+EEbFqj3x/vU/r1e4/mdC24ECaNvT4+MHPyFENDPsOKJofExE4/fKjjGKxmaFiBYUPI+F9B/jJvZNipm5+S0RidEXnWbfehryA7fCl/ip2hPf6X9GLuA48wE8tLzSjddH8ZnPkyH//u4/MLU1PTNzQNkvyJlRsb1P/PnXf9Ap7YAIH8AzyyvdR9K/fR4J+fe2Mzd7dd/+/YtaH4QwKtYr738IH8AjyyvdR4UQF9K3P4IURZGYWZhfH5+YMH9vN8JoUNL/fH94/8PUGvDE8ko3Xrj9S8jBEw8LZhcX2wkeUW26HNC0u/T6H8IH8MDySve5MKfVu+JhwezCwtbIRZtJhNHjn/uvw6//4W03wGHpsOBM6B+poHxYYELFH72e/IT+/Ovf39v/ED6Ao9LgucCwwMCwwAS/+6Jc/8PbboCD0mFBN+TgiYcFs4sL614Ej/D+Lbpc/0P4AI5JhwVBL9paHxaY4F8Y7el/eNsNcEg6LHg25N+zgTsWuNXxqObu23T9/ofwARzAsCDh5LDAFHfCqN//ED6A5RgWJDrzQ4YF/PgqZncYrRE+gMW4Y0F6x4LFCncs4EdZOXvC6KIQ4gThA1gqHRacCX1YMDOf/yiESj+1+NFWrp0wWnv9+534+5rBAWAjhgVCTMbDgvlqwwLCSAH9YXTq9e93Vnf+B+EDWIRhQSIeFnRm5YcFhJEC6sIoHhic4A4HgKUYFiQODhsWNEQYKSAXRpeEECdf/37nV9l/QfgAFmBYsHPHAjMfhUAYKTD69bnYS854hn6kAuEDtIxhgRBj4+NidmHu6tj4eCu3yiGMFNj7+qy9lg4LihA+QIsYFiSLtrnFBavuWEAYNXLqte/tDguKED5ACxgWJJoOC0whjCrpDwte+17xR2cPmrDmsIFAMCxIdGZn1vcfmHLixqDZm3YOzZlKX+St/rDgte/lhwVFCB/AIIYFybDg4Pzc+uS+SWfvSE0Y7XExPeMZOiwowttugCEMC9ofFpgS0Nt0a699r3xYUITwAQxYXuk+LYT4csivtY3DAlM8DaNnXvte54zsf0z4AJotr3RXGRZMbXXmMsOCgH/0OB5GG2m/c67C1xYifABNGBYkOnMVhwWEkYIv0u7NtN+pPCwoQvgAGqTDgvhvhg+H+vpGYw2HBYSRgi9SKl60PVF3WFCE8AEUW17pnhBCrAY/LFhUPCwgjBR8kTTpYUGRMa2HCwQmHRb8MOjP4JmcEPNLi1vKF21R5p+AVHrq+l6fZ1QHj+DMB1CHYUEyLDg4MCww+tMl0B9lGs+KlAwLihA+QEMMCxIHKwwLCCP9FIWRsmFBEcIHaIBhQTIsmJEcFhBG+kmEkdJhQRHCB5DEsCAZFswpHBYQRvqNeNprr62p73eGYXAASGBYIMTE5IRYUDwsMLopCHTAUPK0nzEVPIIzH6A+hgX5YYEpnBlp0R8WXFzTMywowl2tgYoYFiSqDAt0GfybuvZsCOOu1P1hwcU1fcOCIoQPUAHDgmbDAh2MZ4PR5DOiPyy4uKZ3WFCEt92AERgWqB8WmMBbdKXWLhrsd4ZhcACUYFiQDiIbGuMAABhySURBVAseXNwamxhfdKmgZ7xQ6Jm2g0dw5gMUY1ggxP7pGsMCx36UBHhm1MqwoAjhA2QwLEj0hwXTDYYFhJElD9bX2rCgCOEDDGBYkA4LFjQMCwijth6s1WFBETofIJUOCy6EHDzxsGB+aeGqlkWbYxd1etIZrdkYPIIzHyCxvNKNC9izIb8c8bBg7oGFLREJ4xePujhdduDM6PmLa53nlB+LIoQPgsewoOawwATCqMmDxcOCpy+udVaNHY8EwgfBSocFcb/zeMivw/RMZ+PAwWm7p+SEUdUH20jfZrNmWFCE8EGQ0mHBasiLtnhYcHBu5uq+qf3OXDh6H2E0TDIsWLWv3xmGwQGCs7zSfSL0KXUcPAtLC1f3T+1fdPLmzg4etOZDXnMpeARnPggNw4KdYcH8dhRFU1W+3smfEGGdGT1/cdXeYUERwgfBYFggxFQyLIj/0E/L/hqEkRkVDjkZFqzaPSwoQvjAewwLEh1NwwLCyIzMIW+kb7NZPywoQvjAawwLdocF+w0NCwgj7frDggsO9TvDMDiAtxgWpMOCBxeu7j+w39gdqRkwaLXmQ/AIznzgK4YF6bBgqeKwwOCPAc6MpD1/wcFhQRHCB95hWDAwLIgkhwWEUTmzB90fFlxwdFhQhPCBNxgWJDqzGoYFhFE5fQe9kb7N5uywoAjhAy8wLEiHBfOGhgWEUTk1B+3FsKAIgwM4j2HBwLDA1K1yDBb0gQ4YvBkWFOHMB05jWFBzWGAKZ0blyg/aq2FBEcIHzmJYoGBYYIqhHzPO/jTbPfBTvg0LihA+cA7DgoSWYYEphFFWMiw469+woAidD5ySDgsuhBw8cb8zszh71dngEeaKHEf6onhY8GhIwRObsOAYgErSYUF8xuPuD92GdoYFY+Pj7n0GT5lsMmg6ZTH0MHW8JIQ4eeGsv8OCIrztBicwLLB0WGCKn2/TvXDhbOdpsw9pD858YD2GBcmwYCYzLAjqr43+nRmdunA2jGFBEc58YK10WBD/AX0y5N+leFgwPaLfCf5PsTtnRsENC4oQPrDS8kr3kbTfCfrC0Zn4jgUHMheOVvgjG/yfajvDKB4WnLhwttPVdTwuIXxgnYFFW9jDgocWro5XGRYQRqO1H0bBDguKED6wCsOCZFgw/2CDYQFhVM783ReCHhYUIXxgjeWV7hkhxOmQf0emOvlhQWOEUTm9T/7Uq4EPC4oQPmgdw4JEf1gwY+DCUcKonJon3x8WvMqwoBDhg1YxLCgZFphCGJWr/+T7w4JXGRaUInzQGoYFNYcFphBG5cqffH9Y8CrDgpEIH7SCYUE6LFia34jGIrvDlzAqt/vkX3iVYUFlhA+MY1ggxP4D+2/NLsxGIhIHLDicegijYU69+l2GBXUQPjCGYUFi+uD0dmeu48/92cIOo2RY8F2GBXVxbzcYwbAgMbs4296wQJcKN0Sz8G7SKiTDgu8yLJBB+EA7hgWWDgt0CSOMkmHBdxkWyOJtN2jFsMChYYEp7r9Nt/bqdzsnLTgOpxE+0IZhgePDAlPcCiOGBYoQPlCOYUFieqZgWMAfuXJ2htFG2u9cMP/QfiJ8oBTDgsTcA8mwoNKfLv4Ilms/jC6l/Q6LNoUIHyjDsCAZFiweKh4WEEYKmA2ji+kZD8MCxQgfKMGwIBkWLDxYb1hAGDWkN4gYFmhE+KAxhgXpsGBxNooaDgsIo4bUhRHDAs0IH0hLhwVx8DwV8qsYDwsOarpjAWHUUP0wYlhgCOEDKWnwXGBYYPaOBYRRQ+WvTX9YcJ5hgRGED2pjWDB6WGAKYdTQ7mvTHxacZ1hgDOGDWtJhwZmQg0dmWGAKYSRl7fx3GBaYRvigsuWV7nNCiGdDfsVUDQtMIYxGOnX+OwwL2kD4YCSGBQmdwwITCKI9+sOC899hWNAW7mqNUgwLEqaHBTpUupO0p599kJEMC77DsKBNhA8KMSywZ1igQ6BhdDE942FY0DLedsNQDAvSYcFD8xtRFOZHIXj4Nh3DAosQPshhWJAOCx4oGBYE+kfG8TBiWGAZwgf3MSxI9IcF8zWGBYRRwy/SimGBpQgf9DEsSMwtKRgWEEYNv0iZN9PgYVhgIcIHDAsGhwUTiocFAf/xajmM4kXbEwwL7EX4BI5hwcCwwMQdCwgjBV80EsMCB4yF/gKELB0WnA05ePbtn4zPeLbGxqK5aMiyWLko809AKj315q/PMwSPGzjzCRDDgsSBztTW7OLMyNvkGP0TwpmR7BdtpBeOntN4iFCI8AkMw4LEfINhAWFkRo0wYljgIMInIAwL0mHBQ/PrE5MTS6p+TcLIjIKnngwLvs2wwDWETyAYFiTDgvkH59bHx8eUBc8whJEZvXhY8G36HVcxOAgAw4LdYYHu4BGmNwXhDhieIXjcxpmP55ZXuqsMC6oNC0zhzKiR/rDglW8zLHAdd7X2FMOCRJNhgS5GbxTt112p+8OCV77NsMAHhI+H0mFB/DfDh0N9DXQMC3QhjCrpDwteYVjgDcLHM8sr3RNCiNWghwX7SoYFDvywJYxy1l6h3/EOgwOPLK90nxZC/DDoYcHUiGGBgwV94AOGZwgePzE48ATDAiEOHFQwLHDwj4OnAwaGBZ4jfBzHsCDRHxZMaxgWEEZtPBjDggAQPg5jWDAwLNhnaFhAGOl+MIYFgSB8HMWwYMSwwBTCSOWDMSwICIMDBzEsqDAsMMXxAYPRByt/QIYFgeHMxzEMCxQNC0xx7I9XC2dF8bDg6Ve+3Vk1+dBoH+HjCIYFiXhYMJUOC5z8ziWMBm30+51vMSwIEeHjAIYFQoxVGBYQRvopPNxLafAwLAgUnY/l0mHBhZCDZ3LfhHjg/YsjF21O3uDZsYNWdLhrBA8487FYOiz4csivQTwsWHhwfiuKROOOhzMj/Soc7vOvfKvznFvPCjoQPpZiWJAMC+Y0DgsII/0GDjcZFnyLYQEShI9lGBYk5h9MhwUGvz0JI236w4KfMyzAAMLHIgwL0mHBoZJhAWFUzM4DvpQGD/0O9mBwYAmGBRWHBQYLeucGDPYd8BrBgyKc+ViAYUE6LHhIwbCAM6NiZg/4+Z8zLEAJwqdlDAvSYcEDmoYFhFExPQfcHxb8nGEBRiB8WpIOC+J+5/EgX4DU/WGBKYRRseYHzLAAldH5tCAdFlwIOXh2hgVGg0fQGZVqdsDxsOARggdVET6GLa90nwh9Sj0+MS4eeP/C1X1Tk+3ekVoQRqWqHzDDAtTG224GLa9041vGnw3mCQ8RL9oW37eg5I4F2hn+o+Ho23QMCyCF8DGEYYHmYYEJ9EWDkmHBPzIsgJwJXje9GBYk5h6YWT9wcKr9t9mayL71pDEhDD6UjGRY8I/0O5BH+GiUDgtWQ+534mFB/FHXVvQ7qoUZRpfS4KHfQSO87aZJOiw4F/JHXfeHBYfmro5NjJtdtNnCv7fpXhJCnCR4oALhowHDgvRWOQXDgmC/49wOoxd+/o+dp9X/sggV4aMYwwIhpmfSYUHFby3CyPqHOsWwAKoRPoowLEjMLc2sTxcNCyp8qwX93WhfGDEsgDZcZKoAdyxIhgUPvG++OHhEtYsWnfwobFXsuuA1HhY8SvBAF858GmJYkA4L3jd3dbzpsIAzo3LmnvxLPYYF0IzwaYBhwc5n8Gi6YwFhVE7Pk3/hZYYFMIDwkcSwYGBYYAphVK75kz/1MsMCGEL41MSwIFE6LDCFMCpX/cn3hwUv0+/AIAYHNTAsqDgsMIUBQ7lqT74/LCB4YBpnPhUxLFA4LDCFM6NyveSOBS8zLEALCJ8KGBZoHhaYQhgNeuHlFxkWoD2EzwgMC1oYFpgSbhidevlFhgVoF3e1LsCwIGHFsECXCreKtvyjDepKhgUv0u+gfYTPEMsr3UfS4An6oxAWHvL0oxCK+B1G8bDgxMsvdroWHAvA225ZA4s2hgWhfhRCEXffpkuGBS8yLIA9CJ8BDAvSYcEH0mEB3xrl3AgjhgWwEuGTWl7pnhFCnLbiYFrSHxYslQwL+FYpZ18YMSyAtYIPn3RYEP8BfdKCw2nNfDwsmNkdFlT6riCMyrUXRgwLYL2gw4dhQTosODS3vn/EsIAwUsBMGF1K+x2CB1YLNnwYFiTDgqX3yw0LCKOG9HzK68V00cawANYLMnwYFiTDgqUPqLtjAWHUUPOzorWXX+yctOCZAJUEFz4MC+7fsaAXRWJa12MQRg3VCyOGBXBOMOHDsCAxu9jZODg3bfytRsKooeGvTTwsOPGzFzsXHHomQF8Q4cOwIBkWzC/NXJ3q7LfiwlHCqKFeMiz4GcMCOMr78GFYkATPg3+yYPUdCwijWvrDgp99k2EB3OV1+DAs2BkWzG9HUTRlweFURhgVWvvZNxkWwH3e3liUYYGZYYEugzfwLMyYSl/klVM/+ybDAvjBu/BhWJBoa1igQ6U7SXv22QcZG+nbbAwL4A2vwodhgX3DAh0CC6NkWPBNhgXwizfhw7AgHRZ8MB0WBNSHeBxGDAvgLS8GBwwL0mHBn5QMCwJeijk6XmBYAK85Hz4MC+5/FEK9YQFhpOCLtGFYAO85Gz4MCxL9YcG8gmEBYaTgixpjWIBgOBk+afBcCH5Y8KDGYQFhpOCLamFYgKA4Fz4MC5LgeeiDC1fHTN6xgDBS8EWFGBYgOE6FTzosOBNy8MTDggcLhgVGfycJIwVf1MewAEFyJnyWV7rPCSGeteBQWtOpOSwgjMxoEEbP/OybnTPOPWFAAevDJ+134j+gT1lwOK2JhwUzDYcFhJEZFZ76huj1+51zXjxhQILVF5kyLNgdFhxQMCwwep2l37e7KTXiqb+Z9jsMCxA0a898GBbsDgtMfRQCZ0baxYu2J376DYYFgJXhw7CgfFhgCmGk1NpPv8GwANgxZtsrkQ4LzoYcPPGw4ME/WbjV9mfwRJl//Hkw454heIC9rOl8GBYkVAwLdGmtM3L3rCi+Y8HJn36DYQGQZUX4MCxIhwUPDQwLHPiBayyM3Bwv9IcFP/0GwwJgmNY7H4YF6bDgQyOGBQ7+7d/YIdv32jAsAEZoNXwYFggxuV9yWEAYWfBAQzEsACpoLXy4Y4EQnVmJj0IoQhhZ8ED9YQF3LAAqMB4+DAsSsw9oHhYQRiYfiGEBUJPR8GFYkFh836ySOxZU5uhazJEwYlgASDAWPgwLKg4LTCCMVD0QwwJAkpHwYViQDAuWPjC/MTYW2fca8BadzIMxLAAa0B4+DAuEONDZf2vh0GwUReKABYczGmE06sEYFgANaQsfhgWJmfnp7bkHOntm1M79bCeMdiTDgq8zLACa0hI+DAsSD1QcFhBG+ik45GRY8HWGBYAKysMnHRbEfzN8ONTfoXhYcKjBsIAw0q/mISfDgq8zLABUUXpX6+WV7on0jCfY4ImHBe97+IGNJos2527w7OAdqWsc8hrBA6in7MxneaX7tBDiyyH/HsXDgkXNwwInV9Lunhk9/9Ovd55r+1gAHykJn+WV7irDguntuaV0WGDwhy1hpEU8LHj6J1/vrFp/pICjGoUPw4JEf1hwsGRYQBiVs+ug4+B54icMCwCtpMOHYUE6LPiwxLCAMCrX3kFfSoOHfgfQTGpwwLAgHRY8IjksMFjQO/np1O0c9BrBA5hT+8yHYYEQBw5qHhZwZlRO/UE//xOGBYBRtcKHYUFmWGAKYVRO/qAZFgAtqRQ+DAsSI4cFphBG5aodNMMCoEUjw4dhQYNhgSmEUbn8QTMsAFpWOjhgWNBwWGAKA4Zyew+aYQFggcIzH4YFBoYFpnBmtOP5n3yNYQFgg6Hhw7CgpWGBKeGFUTIs+BrDAsAWE4PHwbAgYc2wQJfs+2UaE8LgQxVJhgVfY1gA2OR++DAs2B0WTEyMLzpZrMvyN4wupcFDvwNYph8+6bAgfktizrYD7PXuCtG7V/o193q3rwghtkt+jTHRu1vyi/Smer17h2bm529PpMMCC/7G3h4/wmgtfauN4AEsFP2H//LrgmFBT/TuvVd6xL3enWs9ce9GyRdM9Hp3Sn+RXu+uVWdaH/7YR9cnJiaXRn1dUGGUZX9nxLAAsFz08c//4h0hRIffqMSBTke8/5FMHlb4CUgYWfNQpxgWAPaLr/P5b/w+7dra3BSbN29e2fN/Vri4xcnrX1Qx+MRLXud4WPAfCR7ADf2p9eHjb/wq9IXboLHxcfHwxz+2HUVRtak1Z0bFzDzxeFhw4sdf63SNPBqAxnbucPA0L+Wue3fvivXf/776j03OjIrpf+IvxYs2ggdwy/2LTA8ff+OMEOI0v3+7PvinH7myb//+Q41/Ic6MijV74i/8+Gsd/uIEOGjw3m7Ppe+bI/WHt37bPHgEZ0al5J/4KYIHcNf98Ll8/uh13n7b693tbXHz2vV15b8wYVRs9BPvDwt+zLAAcNqeu1pfPn80/gN9kd/SXW9fubLU6/UKL2BVgjAqtveJx8OCR3/MrXIA5w37SAXOfgbUHh+oUCFlAgwihgWAR4be1ZrxQZ6y8UFTYY4XXvjxV+l3AJ8UfZgc44MMZeODpsJ7i+4UwQP4Z2j4pOODk/x+74rHB9fX1+0LZH/DKBkWfJVhAeCjwo/Rvnz+6DnGB3tdX3977t7du3afEfoRRsmw4KsMCwBfFYZPirOfAfH44I+/+/2kNQdUhXthlAwLvsqwAPDZ0MHBoMPH34j7n2f5Ltj1wY/8u/V9U1MjP3bBCXYNGBgWAIEYdeYTi5dvb/INsevf/vU3fgSPsOrMiGEBEJCR4cOdD/Leu3PHzvGBCubDiGEBEKAqZz6MD4ZwYnyggt4wYlgABKpS+KROcu3PLifHByqoCyOGBUDARg4OBjE+yPNqfKBCtQHD2o+/2mFJCQSsVviIJIDiv6k+zDdNYmJyUnzoo396K4qiaRuOxzr5b69TP6LfAYJX5223HfyNdUA8Pth4++071hyQbXbff9sQkfg8wQNAyITP5fNHL6Tv1yN19cofwhgfyIuHBU/86CudC64+AQBqyZz5iHR6zQ/bAVd+89acNQdjl4tp8LBoA3CfVPhcPn+0m158itTW5mb8j/pPPXXb2o++0omD53roLwSAvWoPDgYxPtiL8cEep370FfodAMPJvu22g/HBAMYHffHbsZ8neACUaRQ+6fhgjVd4V+DjA4YFACppeuYjGB/kBTo+YFgAoLJGnc+Ow8ffiAPoy7zsuw59+ENXOjMzdnz0tn7xsIC3YAFUpiR8RBJA8d94j/DSJ8bGx8XDH//YVhRFB2w4Ho0YFgCoTcXbbjv42IUB8Y1Hr/7hD5Z+SrUSDAsASFMWPowP8jbW3556784dH6/9YVgAoBGVZz6C8UHeH3/7O9/ueM2wAEBjyjqfHYePvxEXz2f5rdnl0fiAYQEAJZSHj0gCKH475nF+ixKejA8YFgBQRvXbbjsYHwxwfHzAsACAclrC5/L5o3Ef8AK/XbscHR+8ybAAgA66znxizzE+2Ovf/vU3Lo0P4kXbowwLAOigpfPZwfgg78EPfGB9ZmHe9hBiWABAK51nPvHbb6vpNBept69cWer1etsWvx7PEDwAdNMaPinGBwPi8cH673+v73RTXvwW6Rd+9JUOHxIIQDvt4ZOOD57nt3LXzWvXD7x7+/YVW45nYFhwzoJjARAAE2c+Iv3I7Tf5htr1h7d+a8tFpwwLABindXAw6PDxN04IIX7Ib/EuC8YHDAsAtMLUmU/89ts5xgd7tTw+YFgAoDXGwifFD7sB8fjgD2/99p7hh2VYAKB1RsPn8vmjXcYHe23euDH97va2qTsfMCwAYAXTZz6C8UGeoTsfMCwAYA1jg4NBh4+/8YQQ4lW+DXYtHnpoY35paU7TL8+wAIBV2jjz2fnU05f4Vth1ff3tuXt37+q4Fx7DAgDWaSV8Unzq6YB4fPDH3/1+UuEvybAAgLVaC590fMAPxgEKxwcMCwBYrZXOZ9Dh42/EIfQw3yaJiclJ8eGPfbTJL3EpDZ7rCg8LAJSasODlPMn4YNd7d+6I6+vrsuMDhgUAnNBm59PH+CBPcnzAsACAM1oPnxTjgwHx+ODKb96qeuYTv26nGBYAcIkV4ZOOD56z4FCssbW5Gf8zanywkfY7qwG9NAA80PrgYNDh42/EV98fcf9lVSMeH3zoo396K4qi6SG/IMMCAM6y5W23HXzq6YB4fLDx9tt3hvyrNYIHgMusOvMRydlP/BbSUxYcijUeOfzxjbHx8Z0O6PkffaXDW5QAnGbbmY9gfJCXjg9upsMCggeA86w78xHJ2U8cQF+24FDqinuYUW+F/arC11zI/h+Lhx567//+z4f/ydgzAQCNrAwfUX18sJH+MC9zvcLXdNN/Sr8mXeUBAJoQQvx/o5thXW9WPTcAAAAASUVORK5CYII=');
