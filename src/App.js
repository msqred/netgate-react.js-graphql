import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import MainPage from "./Pages/Main";
import WebsitesPage from "./Pages/Websites"
import LandingPage from './Pages/Landing'
import ShopPage from "./Pages/Shop";
import CorporatePage from "./Pages/Corporate";
import NotFoundPage from "./Pages/NotFound";
import InDevelop from './Pages/InDevelop'
import PromoPage from "./Pages/Promo";
import BaseSingle from './Pages/BaseSingle';
import BasePage from './Pages/Base'
import BrandingPage from "./Pages/Branding";
import ContactsPage from './Pages/Contacts'
import MainPageEn from "./Pages/Main/En";
import MainPageUa from "./Pages/Main/Ua";
import WebsitesPageEn from "./Pages/Websites/En";
import WebsitesPageUa from "./Pages/Websites/Ua";
import LandingPageEn from "./Pages/Landing/En";
import LandingPageUa from "./Pages/Landing/Ua";
import ShopPageEn from "./Pages/Shop/En";
import ShopPageUa from "./Pages/Shop/Ua";
import CorporatePageEn from "./Pages/Corporate/En";
import CorporatePageUa from "./Pages/Corporate/Ua";
import PromoPageEn from "./Pages/Promo/En";
import PromoPageUa from "./Pages/Promo/Ua";
import BrandingPageEn from "./Pages/Branding/En";
import BrandingPageUa from "./Pages/Branding/Ua";
import BasePageEn from "./Pages/Base/En";
import BasePageUa from "./Pages/Base/Ua";
import BaseSingleEn from "./Pages/BaseSingle/En";
import BaseSingleUa from "./Pages/BaseSingle/Ua";
import InDevelopPageEn from "./Pages/InDevelop/En";
import InDevelopPageUa from "./Pages/InDevelop/Ua";
import ContactsPageEn from "./Pages/Contacts/En";
import ContactsPageUa from "./Pages/Contacts/Ua";


const App = () => (

    <Router>
        <React.Fragment>
            <Switch>
                <Route exact path={'/'} component={MainPage}/>
                <Route exact path={'/en'} component={MainPageEn}/>
                <Route exact path={'/ua'} component={MainPageUa}/>

                <Route path='/websites' component={WebsitesPage}/>
                <Route path='/en/websites' component={WebsitesPageEn}/>
                <Route path='/ua/websites' component={WebsitesPageUa}/>

                <Route path='/landing' component={LandingPage}/>
                <Route path='/en/landing' component={LandingPageEn}/>
                <Route path='/ua/landing' component={LandingPageUa}/>

                <Route path='/shop' component={ShopPage}/>
                <Route path='/en/shop' component={ShopPageEn}/>
                <Route path='/ua/shop' component={ShopPageUa}/>

                <Route path='/corporate' component={CorporatePage}/>
                <Route path='/en/corporate' component={CorporatePageEn}/>
                <Route path='/ua/corporate' component={CorporatePageUa}/>

                <Route path='/promo' component={PromoPage}/>
                <Route path='/en/promo' component={PromoPageEn}/>
                <Route path='/ua/promo' component={PromoPageUa}/>

                <Route path='/branding' component={BrandingPage}/>
                <Route path='/en/branding' component={BrandingPageEn}/>
                <Route path='/ua/branding' component={BrandingPageUa}/>

                <Route path='/indevelop' component={InDevelop}/>
                <Route path='/en/indevelop' component={InDevelopPageEn}/>
                <Route path='/ua/indevelop' component={InDevelopPageUa}/>

                <Route path='/base/:link' component={BaseSingle}/>
                <Route path='/en/base/:link' component={BaseSingleEn}/>
                <Route path='/ua/base/:link' component={BaseSingleUa}/>

                <Route path='/base' component={BasePage}/>
                <Route path='/en/base' component={BasePageEn}/>
                <Route path='/ua/base' component={BasePageUa}/>

                <Route path='/contacts' component={ContactsPage}/>
                <Route path='/en/contacts' component={ContactsPageEn}/>
                <Route path='/ua/contacts' component={ContactsPageUa}/>

                <Route exact path='*' component={NotFoundPage}/>

            </Switch>
        </React.Fragment>
    </Router>
);

export default App;
