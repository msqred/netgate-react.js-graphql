import React, {useState, useEffect} from 'react'
import {NavLink} from "react-router-dom";
import {Link} from "react-router-dom";
import {graphql} from 'react-apollo'
import gql from 'graphql-tag'

let sum = 0;

const Calculator = ({data: {loading, error, brandingCalculators}}) => {

    let button;

    const [isActive, setIsActive] = useState(null);
    const [isActiveOption, setIsActiveOption] = useState(null);
    const [style, setStyle] = useState(null);
    const [option, setOption] = useState(null);
    const [stateSum, setStateSum] = useState(0);
    const [day, setDay] = useState(0);
    const [styleDay, setStyleDay] = useState(0);
    const [optionDay, setOptionDay] = useState(0);


    useEffect(() => {
        setOnLoad();
    }, [brandingCalculators]);


    const setOnLoad = () => {
        if (button) {
            setIsActive(brandingCalculators[0].styleItems[1]);
            setStyle(brandingCalculators[0].styleItemsCost[1]);
            setStyleDay(parseInt(brandingCalculators[0].styleItemsDay[1]));

            setIsActiveOption(brandingCalculators[0].optionsItems[0]);
            setOption(brandingCalculators[0].optionsItems[0]);
            setOptionDay(parseInt(brandingCalculators[0].optionsItemsDay[0]));
        }
    };


    const setActive = (e) => {
        setIsActive(e.target.getAttribute('id'));
        setStyle(e.target.getAttribute('data-cost'));
        setStyleDay(parseInt(e.target.getAttribute('data-day')));
    };

    const setActiveOption = (e) => {
        setIsActiveOption(e.target.getAttribute('id'));
        setOption(e.target.getAttribute('data-value'));
        setOptionDay(parseInt(e.target.getAttribute('data-day')));
    };

    useEffect(() => {
            setAllSum();
            setDayFunc();
        }
    );

    const setAllSum = () => {
        sum = style * option;
        setStateSum(sum);
    };

    const setDayFunc = () => {
        if (isActiveOption !== null) {
            setDay(styleDay + optionDay);
        }
    };


    if (error) return <h1></h1>
    if (!loading) {
        return (


            <div className="branding_calc calc">
                <div className="b_c--wrap">
                    <h3 className="b_c--title up">{brandingCalculators[0].title}</h3>

                    <div className="b_c--calculator up">

                        <div className="b_c--left">

                            <div className="b_c--brand">
                                <div className="b_c--b--title">
                                    <span>{brandingCalculators[0].styleName}</span>
                                    <Link
                                        to={brandingCalculators[0].styleHelpLink}
                                        className="b_c--b__info"
                                    >
                                        <i className="icon-info"/>
                                        <span>{brandingCalculators[0].styleHelpText}</span>
                                    </Link>
                                </div>
                                <div className="b_c--b--buttons">

                                    {brandingCalculators[0].styleItems.map(function (name, index) {
                                        return (
                                            <div
                                                className={`b_c--b--btn ${isActive === name ? 'active' : ''}`}
                                                onClick={setActive}
                                                data-cost={brandingCalculators[0].styleItemsCost[index]}
                                                data-day={brandingCalculators[0].styleItemsDay[index] ? brandingCalculators[0].styleItemsDay[index] : 1}
                                                key={name}
                                                id={name}
                                                ref={node => {
                                                    button = node;
                                                }}
                                            >
                                                {name}
                                            </div>
                                        );
                                    })}
                                </div>
                            </div>

                            <div className="b_c--quantity">
                                <div className="b_c--q--title">
                                    <span>{brandingCalculators[0].optionsName}</span>
                                    <Link
                                        to={brandingCalculators[0].optionsHelpLink}
                                        className="b_c--b__info"
                                    >
                                        <i className="icon-info"/>
                                        <span>{brandingCalculators[0].optionsHelpText}</span>
                                    </Link>
                                </div>
                                <div className="b_c--q--buttons">

                                    {brandingCalculators[0].optionsItems.map(function (name, index) {
                                        return (
                                            <div
                                                className={`b_c--q--btn ${isActiveOption == name ? 'active' : ''}`}
                                                onClick={setActiveOption}
                                                data-value={name}
                                                data-day={brandingCalculators[0].optionsItemsDay[index] ? brandingCalculators[0].optionsItemsDay[index] : 1}
                                                key={name}
                                                id={name}
                                            >
                                                {name}
                                            </div>
                                        );
                                    })}
                                </div>
                            </div>

                        </div>

                        <div className="b_c--right">
                            <div className="b_c--right--title">
                                <span>{brandingCalculators[0].resultName}</span>
                                <div className="b_c--r--t--info">
                                    <span>{brandingCalculators[0].resultText}</span>
                                    <Link
                                        to={brandingCalculators[0].resultHelpUrl}
                                        className="b_c--b__info"
                                    >
                                        <i className="icon-info"/>
                                        <span>{brandingCalculators[0].resultHelpText}</span>
                                    </Link>
                                </div>
                            </div>
                            <div className="b_c--right--result">
                                <div className="b_c--right--result--sum">
                                    $<span>{stateSum}</span>
                                </div>
                                <div className="b_c--right--result--day">
                                    <span>{day}</span> дней
                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        )
    }
    return (
        <div></div>
    )
}

export const calculator = gql`
    {
        brandingCalculators {
            styleName
            styleItems
            styleHelpText
            styleHelpLink
            optionsName
            optionsItems
            optionsHelpText
            optionsHelpLink
            resultName
            resultText
            resultHelpText
            resultHelpUrl
            title
            styleItemsCost
            styleItemsDay
            optionsItemsDay
        }
    }
`

export default graphql(calculator)(Calculator)


