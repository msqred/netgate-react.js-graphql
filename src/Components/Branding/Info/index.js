import React, {useEffect} from 'react'
import {graphql} from 'react-apollo'
import gql from 'graphql-tag'


const BrandingInfo = ({data: {loading, error, brandingInfoItemses}}) => {

    useEffect(() => {

            const animationLoad = document.querySelectorAll('.animation');

            if (animationLoad.length > 0) {
                animation(animationLoad);

                window.addEventListener('scroll', () => {
                    animation(animationLoad);
                });
            }

            function animation(arr) {

                for (let i = 0; i < arr.length; i++) {
                    let min = arr[i].getBoundingClientRect().top - window.innerHeight;
                    if (min < 0) {
                        arr[i].classList.add('active');
                    }
                }


            }

        }
    );


    if (error) return <h1></h1>
    if (!loading) {
        return (

            <div className="branding_info animation">
                <div className="branding_info--wrap">

                    {brandingInfoItemses.map(item => {
                        return (
                            <div
                                className="branding_info--item"
                                key={item.id}
                            >
                                <div className="b_i--i--icon">
                                    <img
                                        src={`https://media.graphcms.com/${item.icon.handle}`}
                                        alt=""
                                    />
                                </div>
                                <div className="b_i--i--point">
                                    <i></i>
                                </div>
                                <div className="b_i--i--title">
                                    <span>{item.title}</span>
                                </div>
                                <div className="b_i--i--text">
                                    <p>{item.text}</p>
                                </div>
                            </div>
                        );
                    })}


                </div>
            </div>

        )
    }
    return (
        <div></div>
    )
}

export const brandingInfo = gql`
    {
        brandingInfoItemses {
            id
            icon {
                handle
            }
            title
            text
        }
    }
`

export default graphql(brandingInfo)(BrandingInfo)


