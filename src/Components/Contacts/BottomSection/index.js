import React, {useState, useRef} from 'react';
import gql from "graphql-tag";
import {graphql} from 'react-apollo'
import {ReactComponent as Instagram} from '../../../Assets/svg/instagramIcon.svg'


const BottomSection = ({data: {loading, error, contactses}, openPopup, hiddenBlock}) => {


    if (loading) return 'Loading...';
    if (error) return 'Something Bad Happened';

    return (

        <div className={`contacts_bottom up ${hiddenBlock === true ? 'hidden' : ''}`}>
            <div
                className="contacts_btn"
                onClick={openPopup}
            >
                <span>{contactses[0].buttonText}</span>
                <i className="icon-btn_arrow"/>
            </div>
            <div className="contacts_bottom--social">
                <p>{contactses[0].socialText}</p>
                <div className="contacts_bottom--social--icons">
                    <a
                        href={contactses[0].facebookLink}
                        className="c_b--s--i--items"
                    >
                        <i className="icon-facebook"/>
                    </a>
                    <a
                        href={contactses[0].instagramLink}
                        className="c_b--s--i--items"
                    >
                        <Instagram/>
                    </a>
                    <a
                        href={contactses[0].twitterLink}
                        className="c_b--s--i--items"
                    >
                        <i className="icon-twitter"/>
                    </a>
                </div>
            </div>
        </div>

    )
}


const contacts = gql`
    {
        contactses {
            buttonText
            socialText
            facebookLink
            instagramLink
            twitterLink
      }
    }
`

export default graphql(contacts)(BottomSection)
