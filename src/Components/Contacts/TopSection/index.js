import React, {useState, useRef} from 'react';
import gql from "graphql-tag";
import {graphql} from 'react-apollo'
import {ReactComponent as Icon} from '../../../Assets/svg/contactsIcon.svg'


const TopSection = ({data: {loading, error, contactses}, hiddenBlock}) => {


    if (loading) return 'Loading...';
    if (error) return 'Something Bad Happened';

    return (

        <div className={`contacts_top ${hiddenBlock === true ? 'hidden' : ''}`}>
            <div className="contacts_top--wrap--left">
                <div className="c_t--w--l--tel up">
                    <div className="c_t--w--l--tel--icon">
                        <i className="icon-icon_mobile"/>
                    </div>
                    <a
                        href={`tel:${contactses[0].phone}`}>
                        {contactses[0].phone}
                    </a>
                </div>

                <div className="c_t--w--l--telegram up">
                    <div className="c_t--w--l--telegram--icon">
                        <i className="icon-icon_telegram"/>
                    </div>
                    <a href={`https://t.me/${contactses[0].telegram}`}>
                        <span>{`@${contactses[0].telegram}`}</span>
                        <i className="c_t--w--l--telegram--arrow icon-arrow_telegram"/>
                    </a>
                </div>
            </div>
            <div className="contacts_top--wrap--right">
                <div className="c_t--w--r--icon">
                    <Icon/>
                </div>
            </div>
        </div>

    )
}


const contacts = gql`
    {
        contactses {
            phone
            telegram
      }
    }
`

export default graphql(contacts)(TopSection)
