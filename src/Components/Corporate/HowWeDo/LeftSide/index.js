import React, {useState} from 'react';
import gql from "graphql-tag";
import {graphql} from 'react-apollo'
import {ReactComponent as HowSquareIcon} from '../../../../Assets/svg/howSquare.svg';
import {ReactComponent as HowLineIcon} from '../../../../Assets/svg/howLine.svg';
import {ReactComponent as HowBottomIcon} from '../../../../Assets/svg/howBottom.svg';


const LeftSide = ({data: {loading, error, corporateHowWeDoes}}) => {


    if (loading) return '';
    if (error) return '';

    return (
        <div className="c_h--w--left">
            <div className="c_h--w--left--icon up">
                <HowSquareIcon/>
                <HowLineIcon/>
                <HowBottomIcon/>
                <img
                    src={`https://media.graphcms.com/${corporateHowWeDoes[0].icon.handle}`}
                    alt=""
                />
            </div>
            <div className="c_h--w--left--title up">
                <span>{corporateHowWeDoes[0].title}</span>
            </div>

            <a
                href={corporateHowWeDoes[0].buttonUrl}
                className="c_h--w--left--btn up"
            >
                <span>{corporateHowWeDoes[0].buttonText}</span>
                <i className="icon-calculator"/></a>
        </div>
    )
}


const leftSide = gql`
    {
        corporateHowWeDoes {
            title
            icon {
                handle
            }
            buttonText
            buttonUrl
        }
    }
`

export default graphql(leftSide)(LeftSide)
