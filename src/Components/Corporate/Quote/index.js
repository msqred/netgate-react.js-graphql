import React, {useState} from 'react';
import gql from "graphql-tag";
import {graphql} from 'react-apollo'
import {ReactComponent as QuoteIcon} from '../../../Assets/svg/quote.svg';


const Quote = ({data: {loading, error, corporateQuotes}}) => {


    if (loading) return '';
    if (error) return '';

    return (
        <div className="corporate_quote up">
            <QuoteIcon/>
            <QuoteIcon/>
            <div className="c_q--item">
                <div className="c_q--item--img">
                    <img
                        src={`https://media.graphcms.com/${corporateQuotes[0].photo.handle}`}
                        alt=""
                    />
                </div>
                <div className="c_q--item--text">
                    <p>{corporateQuotes[0].text}</p>
                </div>
                <div className="c_q--item--name">
                    <span>{corporateQuotes[0].name}</span>
                </div>
            </div>
        </div>
    )
}


const quote = gql`
    {
        corporateQuotes {
            photo {
                handle
            }
            text
            name
        }
    }
`

export default graphql(quote)(Quote)
