import React, {useState} from 'react'
import gql from 'graphql-tag'
import {graphql} from 'react-apollo'
import {NavLink, Link} from "react-router-dom";
import {ReactComponent as Logo} from '../../Assets/svg/Logo.svg';


const Header = ({data: {loading, error, headers}}) => {

    const [langActive, setLangActive] = useState(false);

    const setLang = (e) => {
        setLangActive(!langActive);
    }

    function BurgerClick(e) {
        const menuBurger = document.querySelector('.m_s--nav');
        const mainMenu = document.querySelector('.main_menu');

        e.preventDefault();
        menuBurger.childNodes[0].classList.toggle('active');
        mainMenu.classList.toggle('active');
    }

    if (error) return <h1></h1>
    if (!loading) {

        return (
            <header className="header_main">
                <a href="/" className="logo">
                    <Logo/>
                </a>

                <div className="main_screen--menu">
                    <div className="m_s--top">
                        <div className="m_s--nav" onClick={BurgerClick}>
                            <div className="m_s--nav--burger">
                                <i></i>
                                <i></i>
                            </div>
                        </div>
                        <div
                            className={`m_s--lang ${langActive === true ? 'active' : ''}`}
                            onClick={setLang}
                        >

                            {headers.map(item => (

                                (() => {

                                    return (
                                        <React.Fragment>
                                            {item.languages.map(function (name, index) {
                                                return (
                                                    <NavLink
                                                        key={item.name}
                                                        to={item.languagesLink[index]}
                                                        activeClassName="active"
                                                        exact={true}
                                                    >
                                                        {name}
                                                    </NavLink>
                                                );
                                            })}
                                        </React.Fragment>
                                    )
                                })()
                            ))}
                            <div className="cls_lang">
                                <i></i>
                                <i></i>
                            </div>
                        </div>
                    </div>

                    <div className="main_menu">
                        <div className="nav">
                            {headers.map(item => (

                                (() => {

                                    return (
                                        <React.Fragment>
                                            {item.mainMenu.map(function (name, index) {
                                                return (
                                                    <NavLink
                                                        key={item.id}
                                                        to={item.mainMenuLink[index]}
                                                        data-text={name}
                                                        activeClassName="active"
                                                        exact={true}
                                                    >
                                                        <span>{name}</span>
                                                    </NavLink>
                                                );
                                            })}
                                        </React.Fragment>
                                    )
                                })()
                            ))}

                        </div>
                    </div>

                    <div className="m_s--points">
                        {headers.map(item => (

                            (() => {

                                return (
                                    <React.Fragment>
                                        {item.rightMenu.map(function (name, index) {
                                            return (
                                                <Link
                                                    key={item.id}
                                                    to={item.rightMenuLink[index]}
                                                    activeClassName="active"
                                                >
                                                    {name}
                                                </Link>
                                            );
                                        })}
                                    </React.Fragment>
                                )
                            })()
                        ))}
                    </div>
                </div>
            </header>
        )
    }

    return <h2></h2>
};


export const header = gql`
  {
    headers {
        id
        mainMenu
        mainMenuLink
        languages
        languagesLink
        rightMenu
        rightMenuLink
    }
  }
`

export default graphql(header)(Header)
