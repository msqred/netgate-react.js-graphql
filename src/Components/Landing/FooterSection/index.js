import React from 'react'
import gql from "graphql-tag";
import {graphql} from "react-apollo";


const FooterSection = ({data: {loading, error, landingPageBottomInfoes, landingPageBottomCategories}}) => {

    if (loading) return 'Loading...';
    if (error) return 'Something Bad Happened';


    return (
        <div className="landing_page--footer">
            <div className="l_p--footer--wrap">
                <div className="l_p--f--left">
                    {landingPageBottomCategories.map(category => (
                        <div className="l_p--f--left--text up" key={category.id}>

                            <p className="f-title">{category.title}</p>
                            <React.Fragment>
                                {category.items.map(item => (
                                    <p>{item}</p>
                                ))}
                            </React.Fragment>
                        </div>
                    ))}
                </div>
                <div className="l_p--f--right up">
                    <a
                        href={landingPageBottomInfoes[0].buttonUrl}
                        className="l_p--btn-calc"
                    >
                        <span>{landingPageBottomInfoes[0].buttonText}</span>
                        <i className="icon-calculator"/>
                    </a>
                </div>
            </div>
            <img
                src={`https://media.graphcms.com/${landingPageBottomInfoes[0].backgroundImage.handle}`}
                alt={landingPageBottomInfoes[0].backgroundImage.fileName}
                className="bg-footer"/>
        </div>
    )
}


const footerSection = gql`
    {
        landingPageBottomInfoes {
            buttonUrl
            buttonText
            backgroundImage {
              handle
              fileName
            }
          }
          landingPageBottomCategories {
            id
            title
            items
          }
    }
`

export default graphql(footerSection)(FooterSection)


