import React, {useState} from 'react';
import {Helmet} from "react-helmet";
import gql from "graphql-tag";
import {graphql} from 'react-apollo'
import TopSection from "./TopSection";
import MainSection from "./MainSection";
import FooterSection from "./FooterSection";


const Landing = ({data: {loading, error, landingPages, landingPageItemses}}) => {


    if (loading) return 'Loading...';
    if (error) return 'Something Bad Happened';

    return (
        <React.Fragment>

            <div className="landing_page--main">
                <div className="l_p--main--wrap">

                    <Helmet>
                        <title>Netgate - Landing page</title>
                        <meta name="description" content="Landing page"/>
                    </Helmet>

                    <TopSection/>
                    <MainSection mainTitle={landingPages[0].title} data={landingPageItemses}/>


                </div>

                <img className="l_p--main--bg"
                     src={`https://media.graphcms.com/${landingPages[0].backgroundImage.handle}`}
                     alt={landingPages[0].backgroundImage.fileName}
                />
            </div>

            <FooterSection/>

        </React.Fragment>
    )
}


const landing = gql`
    {
        landingPages {
            title
            backgroundImage {
                handle
                fileName
            }
        }
        landingPageItemses {
            id
            title
            text
            lineSvg {
                handle
            }
            squareSvg {
                handle
            }
            mainIconSvg {
                handle
            }
            isReverse
        }
    }
`

export default graphql(landing)(Landing)
