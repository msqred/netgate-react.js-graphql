import React, {useState} from 'react';
import gql from 'graphql-tag'
import {graphql} from 'react-apollo'
import {Link} from 'react-router-dom'


const Main = ({data: {loading, error, mainPages}}) => {

    if (error) return <h1></h1>
    if (!loading) {

        return (
            <div className="main_pag">
                <div className="main_screen">
                    <div className="main_screen--left">
                        <div className="m_s--l--wrap">
                            <Link to={mainPages[0].leftSideLink} className="m_s--l--arrow"
                            >
                                <iframe src="http://netgate.com.ua/left-arrow/"
                                        scrolling="no"
                                />
                            </Link>

                            <div className="m_s--l--info up">
                                <Link to={mainPages[0].leftSideLink}
                                      className="m_s--l--category up">{mainPages[0].leftSideCategory}</Link>
                                <Link to={mainPages[0].leftSideLink}
                                      className="m_s--l--text">{mainPages[0].leftSideText}</Link>
                            </div>
                        </div>
                    </div>
                    <div className="main_screen--right">

                        <div className="m_s--l--wrap">
                            <Link
                                to={mainPages[0].rightSideLink}
                                className="m_s--l--arrow"

                            >
                                <iframe
                                    src="http://netgate.com.ua/right-arrow/"
                                    scrolling="no"
                                />
                            </Link>
                            <div className="m_s--l--info up">
                                <Link to={mainPages[0].rightSideLink}
                                      className="m_s--l--category up">{mainPages[0].rightSideCategory}</Link>
                                <Link to={mainPages[0].rightSideLink}
                                      className="m_s--l--text">{mainPages[0].rightSideText}</Link>
                            </div>
                        </div>
                    </div>
                    <div className="main_screen--bg">
                        <img className="parallax"
                             src={`https://media.graphcms.com/${mainPages[0].backgroundImage.handle}`} alt=""/>
                    </div>

                </div>

            </div>
        )
    }

    return <h2></h2>
};


export const main = gql`
    {
        mainPages {
            leftSideLink
            leftSideText
            leftSideCategory
            rightSideText
            rightSideLink
            rightSideCategory
            backgroundImage {
                handle
            }
        }
    }
`

export default graphql(main)(Main)
