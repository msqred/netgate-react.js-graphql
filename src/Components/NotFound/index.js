import React, {useState} from 'react';
import gql from "graphql-tag";
import {graphql} from 'react-apollo'
import {Helmet} from "react-helmet";
import {ReactComponent as NotFoundIcon} from '../../Assets/svg/notFound.svg';


const NotFound = ({data: {loading, error, notFounds}}) => {


    if (loading) return 'Loading...';
    if (error) return 'Something Bad Happened';

    return (
        <React.Fragment>
            <Helmet>
                <title>{notFounds[0].headerTitle}</title>
                <meta name="description" content={notFounds[0].headerDescription}/>
            </Helmet>

            <div className="nf_page">
                <div className="nf_page--wrap">

                    <div className="nf_img">
                        <NotFoundIcon/>
                    </div>
                    <div className="nf_info">
                        <span className="nf_text--title up">{notFounds[0].title}</span>
                        <span className="nf_text--description up">{notFounds[0].text}</span>
                        <a
                            href={notFounds[0].buttonUrl}
                            className="nf_btn up"
                        >
                            <span>{notFounds[0].buttonText}</span>
                        </a>
                    </div>
                </div>
            </div>

        </React.Fragment>
    )
}


const notFound = gql`
    {
        notFounds {
            title
            text
            buttonText
            buttonUrl
            headerTitle
            headerDescription
        }
    }
`

export default graphql(notFound)(NotFound)
