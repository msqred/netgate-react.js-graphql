import React from 'react'
import {NavLink} from "react-router-dom";
import {Link, animateScroll as scroll} from "react-scroll";
import {graphql} from 'react-apollo'
import gql from 'graphql-tag'
import {ReactComponent as PromoIcon} from '../../../Assets/svg/promoIcon.svg';
import {ReactComponent as PromoSquare} from '../../../Assets/svg/promoSquare.svg';
import Menu from "./Menu";


const TopSection = ({data: {loading, error, promoInfoes}}) => {
    if (error) return <h1></h1>
    if (!loading) {
        return (


            <div className="websites_top">
                <div className="w_t--wrap">
                    <Menu/>
                </div>

                <div className="promo_info--wrap">
                    <div className="p_i--wrap--icon up">
                        <PromoSquare/>
                        <PromoIcon/>
                    </div>
                    <div className="p_i--wrap--text">
                        <div className="p_i--wrap--title up">
                            <span>{promoInfoes[0].title}</span>
                            <Link
                                to="calc"
                                className="p_i--wrap--btn-calc"
                                spy={true}
                                smooth={true}
                                offset={-70}
                                duration={500}
                            >
                                <i className="icon-calculator"/>
                            </Link>
                        </div>
                        <div className="p_i--wrap--info up">
                            <p>{promoInfoes[0].text}</p>
                        </div>
                        <div className="p_i--wrap--description up">
                            <p>{promoInfoes[0].description}</p>
                        </div>
                    </div>
                </div>

                <img
                    className="w_t--circle"
                    src={`https://media.graphcms.com/${promoInfoes[0].circleImage.handle}`}
                    alt=""
                />
                <img
                    className="w_t--bg"
                    src={`https://media.graphcms.com/${promoInfoes[0].backgroundImage.handle}`}
                    alt=""
                />
            </div>
        )
    }
    return (
        <div></div>
    )
}

export const topSection = gql`
    {
        promoInfoes {
            title
            text
            description
            backgroundImage {
                handle
            }
            circleImage {
                handle
            }
        }
    }
`

export default graphql(topSection)(TopSection)


