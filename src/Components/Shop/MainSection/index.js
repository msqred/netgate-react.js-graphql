import React from 'react';
import gql from "graphql-tag";
import {graphql} from 'react-apollo'


const MainSection = ({data: {loading, error, shopPageInfoes, shopPageMiddleTopItemses, shopPageMiddleBottomItemses}}) => {


    if (loading) return 'Loading...';
    if (error) return 'Something Bad Happened';

    return (
        <React.Fragment>
            <h1 className="s_p--title">{shopPageInfoes[0].title}</h1>

            <div className="s_p--wrap">

                {shopPageMiddleTopItemses.map(category => (
                    <div className={`s_p--wrap--item ${category.blue ? 'blue' : ''} up`} key={category.id}>
                        <div className="s_p--w--i--title">{category.title}</div>

                        <div className="s_p--w--i--list">
                            {category.items.map(item => (
                                <p className="up">{item}</p>
                            ))}
                        </div>
                    </div>
                ))}
            </div>

            <div className="function_wrap">
                <div className="function_title up">{shopPageInfoes[0].functionalBlockTitle}</div>
                <div className="function_box">

                    {shopPageMiddleBottomItemses.map(item => (
                        <div className="function_item up">
                            <div className="function_item--icon">
                                <img
                                    src={`https://media.graphcms.com/${item.icon.handle}`}
                                    alt=""
                                />
                            </div>
                            <h3 className="function_icon--title">{item.title}</h3>
                            <p>{item.text}</p>
                        </div>
                    ))}

                </div>
            </div>
        </React.Fragment>
    )
}


const mainSection = gql`
    {
        shopPageInfoes {
            title
            functionalBlockTitle
        }
        shopPageMiddleTopItemses {
            id
            title
            items
            blue
        }
        shopPageMiddleBottomItemses {
            id
            icon {
                handle
            }
            title
            text
        }
    }
`

export default graphql(mainSection)(MainSection)
