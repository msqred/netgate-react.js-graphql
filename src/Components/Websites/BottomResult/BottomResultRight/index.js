import React from 'react'
import gql from "graphql-tag";
import {graphql} from "react-apollo";
import {connect} from 'react-redux';
import BottomResultLeft from "../BottomResultLeft";


const mapStateToProps = (state) => {
    return {

    };
}


const BottomResultRight = ({data: {loading, error, websiteBottoms}, websitesSum, websitesDay, openPopup}) => {

    if (loading) return 'Loading...';
    if (error) return 'Something Bad Happened';

    return (


        <div className="w_b--right up">
            <div className="w_b--top">
                <p>{websiteBottoms[0].paymentName}</p>
                <div className="w_b--top--include">
                    <p className="w_b--top--include--btn">{websiteBottoms[0].paymentDesciption}</p>
                    <a href={websiteBottoms[0].paymentHelpUrl} className="icon_why">
                        <i className="icon-info"/>
                        <span>{websiteBottoms[0].paymentHelpText}</span>
                    </a>
                </div>
            </div>

            <div className="w_b--top--result">
                <span className="w_b--top--result--cost">{`$ ${websitesSum ? websitesSum : 0}`}</span>
                <span className="w_b--top--result--day">{websitesDay ? websitesDay : 0} {websiteBottoms[0].nameInCost}</span>
            </div>
            <div
                className="btn_order btn_order--websites"
                onClick={openPopup}
            >
                <span>{websiteBottoms[0].buttonText}</span>
                <i className="icon-btn_arrow"/>
            </div>
        </div>
    )
}

const websitesInfo = gql`
    {
        websiteInfoes {
            title
            backgroundImage {
                handle
                fileName
            }
            headerTitle
            headerDescription
        }
        websiteBottoms {
            id
            resultTitle
            typeName
            desingName
            adaptiveName
            additionalName
            paymentName
            paymentDesciption
            paymentHelpUrl
            paymentHelpText
            nameInCost
            buttonText
            buttonUrl
        }
    }
`

export default connect(mapStateToProps)(graphql(websitesInfo)(BottomResultRight))