import React from 'react';
import BottomResultLeft from './BottomResultLeft';
import BottomResultRight from "./BottomResultRight";
import gql from "graphql-tag";
import {graphql} from "react-apollo";

const BottomResult = ({data: {loading, error, websiteInfoes}, additionalSum, websitesSum, websitesDay, activePopup, openPopup, closePopup}) => {

    if (loading) return 'Loading...';
    if (error) return 'Something Bad Happened';

    return (
        <div className="websites_b">
            <div className="w_m--wrap">

                <BottomResultLeft
                    additionalSum={additionalSum}
                />

                <BottomResultRight
                    websitesSum={websitesSum}
                    websitesDay={websitesDay}
                    openPopup={openPopup}
                />


            </div>

            <img className="websites_b--bg"
                 src={`https://media.graphcms.com/${websiteInfoes[0].backgroundImage.handle}`}
                 alt=""/>
        </div>
    )
}

const websitesInfo = gql`
    {
        websiteInfoes {
            backgroundImage {
                handle
            }
        }
    }
`

export default graphql(websitesInfo)(BottomResult)