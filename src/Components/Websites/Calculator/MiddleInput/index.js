import React, {useState} from 'react'
import {connect} from "react-redux";


const MiddleInput = (props) => {

    let label, input, inputCount;

    const [isActive, setIsActive] = useState(false);


    const addToStore = () => {
        setIsActive(!isActive);
        if (input.checked) {
            if (inputCount) {
                inputCount.value = 1;
                props.onAddAdditional(label.getAttribute('data-text'), parseInt(label.getAttribute('data-cost')), parseInt(label.getAttribute('data-day')), parseInt(inputCount.value));
            } else {
                props.onAddAdditional(label.getAttribute('data-text'), parseInt(label.getAttribute('data-cost')), parseInt(label.getAttribute('data-day')), 1);
            }
        } else {
            if (inputCount) {
                props.onDeleteAdditional(label.getAttribute('data-text'));
                inputCount.value = 0;
            } else {
                props.onDeleteAdditional(label.getAttribute('data-text'));
            }
        }
    };

    const changeInput = () => {
        if (inputCount.value > 0) {
            setIsActive(true);
            props.onDeleteAdditional(label.getAttribute('data-text'));
            props.onAddAdditional(label.getAttribute('data-text'), parseInt(label.getAttribute('data-cost')), parseInt(label.getAttribute('data-day')), parseInt(inputCount.value));
        } else {
            setIsActive(false);
            props.onDeleteAdditional(label.getAttribute('data-text'));
        }
    };


    return (
        <div className={`w_m--m-line ${isActive ? 'active' : ''}`}>
            <p className="w_m--m-l--name">{props.name}</p>
            <div className="w_m--cost-block">
                <span className="w_m--m-l--cost">{`${props.cost} USD`}</span>

                {props.useCount && props.countInputText ? (
                    <React.Fragment>
                        <input
                            className="w_m--m-l--number"
                            type="number"
                            name="number"
                            placeholder="0"
                            ref={node => {
                                inputCount = node;
                            }}
                            onChange={changeInput}
                        />
                        <span>{props.countInputText}</span>
                    </React.Fragment>
                ) : (
                    ''
                )}


            </div>
            <div className="w_m--m-line--check">
                <input type="checkbox"
                       id={props.id}
                       name={props.nameId}
                       onClick={addToStore}
                       ref={node => {
                           input = node;
                       }}
                />
                <label htmlFor={props.id}
                       data-text={props.name}
                       data-cost={props.cost}
                       data-day={props.day ? props.day : 0}
                       ref={node => {
                           label = node;
                       }}
                />
            </div>
        </div>
    )
}


export default connect(
    state => ({
        initialState: state
    }),
    dispatch => ({
        onAddAdditional: (name, cost, day, count) => {
            dispatch({
                type: 'ADD_WEBSITE_ADDITIONAL', payload: {
                    name: name,
                    cost: cost,
                    day: day,
                    count: count
                },
            })
        },
        onDeleteAdditional: (name) => {
            dispatch({
                type: 'DELETE_WEBSITE_ADDITIONAL', payload: {
                    name: name
                },
            })
        }
    })
)(MiddleInput);
