import React from 'react'


class TopCategoryName extends React.Component {



    constructor(props) {
        super(props);


        this.state = {
            categoryName: this.props.categoryName,
            helpUrl: this.props.helpUrl,
            helpText: this.props.helpText
        };

    }



    render() {
        return (
            <div className="w_m--t--title">
                <span>{this.state.categoryName}</span>

                <a href={this.state.helpUrl} className="p_c--c--type__info">
                    <i className="icon-info"/>
                    <span>{this.state.helpText}</span>
                </a>
            </div>
        )
    }
}

export default TopCategoryName;
