import React, {useEffect} from 'react'
import {connect} from "react-redux";


const TopInput = (props) => {


    const addToStore = () => {
        if (topInput.checked) {
            props.onDeleteAdaptive(topLabel.getAttribute('data-text'));
            console.log(1);
        } else {
            props.onAddAdaptive(topLabel.getAttribute('data-text'), topLabel.getAttribute('data-cost'), topLabel.getAttribute('data-day'), topLabel.getAttribute('data-categoty'));
        }
        props.onAddCategory(topLabel.getAttribute('data-text'), topLabel.getAttribute('data-cost'), topLabel.getAttribute('data-day'), topLabel.getAttribute('data-categoty'));
        props.onAddDedign(topLabel.getAttribute('data-text'), topLabel.getAttribute('data-cost'), topLabel.getAttribute('data-day'), topLabel.getAttribute('data-categoty'));

    };

    const addOnLoad = () => {
        props.onAddAdaptive(topPCLabel.getAttribute('data-text'), topPCLabel.getAttribute('data-cost'), topPCLabel.getAttribute('data-day'), topPCLabel.getAttribute('data-categoty'));
    };

    useEffect(() => {
        if (topPCLabel) {
            addOnLoad();
        }
    }, []);


    let topLabel, topInput, topPCLabel;

    return (
        <React.Fragment>

            <div className="w_m--t--check" key={props.id}>

                {props.category === 'Адаптивность' ?
                    <React.Fragment>

                        {props.name === 'ПК, Ноутбуки' ?
                            <React.Fragment>
                                <input
                                    type="checkbox"
                                    id={props.id}
                                    name={props.nameId}
                                    data-categoty={props.category}
                                    checked
                                    ref={node => {
                                        topInput = node;
                                    }}
                                />

                                <label htmlFor={props.id}
                                       data-text={props.name}
                                       data-cost={props.cost}
                                       data-day={props.day ? props.day : 0}
                                       data-categoty={props.category}
                                       className="adaptive_check"
                                       ref={node => {
                                           topPCLabel = node;
                                       }}
                                >
                                    {props.name}
                                </label>
                            </React.Fragment>

                            :

                            <React.Fragment>
                                <input
                                    type="checkbox"
                                    id={props.id}
                                    name={props.nameId}
                                    data-categoty={props.category}
                                    ref={node => {
                                        topInput = node;
                                    }}
                                />

                                <label htmlFor={props.id}
                                       data-text={props.name}
                                       data-cost={props.cost}
                                       data-day={props.day ? props.day : 0}
                                       data-categoty={props.category}
                                       onClick={addToStore}
                                       ref={node => {
                                           topLabel = node;
                                       }}
                                >
                                    {props.name}
                                </label>
                            </React.Fragment>


                        }

                    </React.Fragment>


                    :

                    <React.Fragment>
                        <input
                            type="radio"
                            id={props.id}
                            name={props.nameId}
                            data-categoty={props.category}
                            ref={node => {
                                topInput = node;
                            }}
                        />
                        <label htmlFor={props.id}
                               data-text={props.name}
                               data-cost={props.cost}
                               data-day={props.day ? props.day : 0}
                               data-categoty={props.category}
                               onClick={addToStore}
                               ref={node => {
                                   topLabel = node;
                               }}
                        >
                            {props.name}
                        </label>
                    </React.Fragment>

                }

            </div>
        </React.Fragment>

    )
}

export default connect(
    state => ({
        initialState: state
    }),
    dispatch => ({
        onAddCategory: (typeName, typeCost, typeDay, typeCategory) => {
            dispatch({
                type: 'ADD_WEBSITE_TYPE', payload: {
                    name: typeName,
                    cost: typeCost,
                    day: typeDay,
                    category: typeCategory,
                    count: 1
                },
            })
        },
        onAddDedign: (typeName, typeCost, typeDay, typeCategory) => {
            dispatch({
                type: 'ADD_WEBSITE_DESIGN', payload: {
                    name: typeName,
                    cost: typeCost,
                    day: typeDay,
                    category: typeCategory,
                    count: 1
                },
            })
        },
        onAddAdaptive: (typeName, typeCost, typeDay, typeCategory) => {
            dispatch({
                type: 'ADD_WEBSITE_ADAPTIVE', payload: {
                    name: typeName,
                    cost: typeCost,
                    day: typeDay,
                    category: typeCategory,
                    count: 1
                },
            })
        },
        onDeleteAdaptive: (typeName) => {
            dispatch({
                type: 'DELETE_WEBSITE_ADAPTIVE', payload: {
                    name: typeName
                },
            })
        }
    })
)(TopInput);
