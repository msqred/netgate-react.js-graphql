import React, { useState, useEffect } from 'react';
import {graphql} from 'react-apollo';
import gql from 'graphql-tag';
import {ReactComponent as WebsitesIcon} from '../../../Assets/img/websites-icon.svg';
import TopInput from './TopInput';
import TopCategoryName from "./TopCategoryName";
import MiddleCategoryName from "./MiddleCategoryName";
import MiddleInput from "./MiddleInput";
import {connect} from "react-redux";



const Calculator = ({data: {loading, error, websitesCalculatorTopCategories, websitesCalculatorMidleCategories, websiteInfoes}}) => {



    if (error) return <h1></h1>
    if (!loading) {
        return (

            <React.Fragment>


                <div className="websites_m">
                    <div className="w_m--icon up">
                        <WebsitesIcon/>
                    </div>


                    <div className="w_m--wrap">
                        <h1 className="title up">{websiteInfoes[0].title}</h1>
                        {/*Верхняя часть калькулятора*/}
                        <div className="w_m--top up">

                            {websitesCalculatorTopCategories.map(category => (
                                <div className="w_m--t--item w_m--type" key={category.id}>

                                    <TopCategoryName
                                        categoryName={category.categoryName}
                                        helpUrl={category.helpUrl}
                                        helpText={category.helpText}/>

                                    <form>
                                        <React.Fragment>
                                            {category.websitesCalculatorTopItemses.map(item => (
                                                <TopInput key={item.id}
                                                          name={item.name}
                                                          cost={item.cost}
                                                          day={item.day}
                                                          id={item.id}
                                                          checked={item.checked}
                                                          nameId={category.id}
                                                          category={category.categoryName}
                                                />
                                            ))}
                                        </React.Fragment>
                                    </form>
                                </div>
                            ))}

                        </div>

                        {/*Конец верхней части калькулятора*/}


                        {/*Средняя часть калькулятора*/}

                        <div className="w_m--main up">

                            {websitesCalculatorMidleCategories.map(category => (
                                <div className="w_m--m--item">
                                    <MiddleCategoryName
                                        key={category.id}
                                        titleClassName={category.titleClassName}
                                        categoryName={category.categoryName}
                                        helpText={category.helpText}
                                        helpUrl={category.helpUrl}
                                    />
                                    {category.websitesCalculatorMidleItems.map(item => (
                                        <MiddleInput
                                            key={item.id}
                                            id={item.id}
                                            name={item.name}
                                            cost={item.cost}
                                            day={item.day}
                                            useCount={item.useCount}
                                            countInputText={item.countInputText}
                                            nameId={category.id}
                                        />
                                    ))}
                                </div>
                            ))}


                        </div>

                        {/*Конец средней части калькулятора*/}


                    </div>
                </div>

            </React.Fragment>
        )
    }
    return (
        <div></div>
    )
}

export const calculator = gql`
    {
        websiteInfoes {
            title
        }
        websitesCalculatorTopCategories {
            id
            categoryName
            websitesCalculatorTopItemses {
                id
                name
                cost
                day
                checked
            }
            helpText
            helpUrl
        }
        websitesCalculatorMidleCategories {
            id
            titleClassName
            categoryName
            helpText
            helpUrl
            websitesCalculatorMidleItems {
                id
                name
                cost
                day
                useCount
                countInputText
            }
        }
    }
`

export default graphql(calculator)(Calculator)

