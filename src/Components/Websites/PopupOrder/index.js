import React from 'react'
import gql from "graphql-tag";
import {graphql, Mutation} from 'react-apollo'
import NavLink from "react-router-dom/NavLink";


const sendRequest = gql`
    mutation createWebsitesRequest($data: WebsitesRequestCreateInput!) {
        createWebsitesRequest(data: $data) {
            phone
            email
            cost
            day
            options
        }
    }
`;


const PopupOrder = ({data: {loading, error, websitesRequests}, activePopup, closePopup, successPopup, setSuccess, websitesSum, websitesDay, websiteType, websiteDesign, websiteAdaptive, websitesAdditional}) => {

    let inputPhone, inputEmail, optionsArr = [];

    const optionsToArr = (arr) => {
        let optionName;
        for (let i = 0; i < arr.length; i++) {
            optionName = arr[i].name;
            optionsArr.push(optionName);
        }
    };

    const setInput = () => {
        const contactInput = document.querySelectorAll('.popup--input > input');

        for (let i = 0; i < contactInput.length; i++) {
            contactInput[i].addEventListener('focus', (e) => {
                contactInput[i].parentNode.classList.add('click');
            });
            contactInput[i].addEventListener('blur', (e) => {
                contactInput[i].parentNode.classList.remove('click');
            });
            contactInput[i].addEventListener('change', (e) => {
                if (contactInput[i].value !== '') {
                    contactInput[i].parentNode.classList.add('active');
                } else {
                    contactInput[i].parentNode.classList.remove('active');
                }
            });
        }
    }

    return (
        <React.Fragment>
        <div className={`popup_confirm ${activePopup ? 'active' : ''} ${successPopup === true ? 'success' : ''}`}>

            <div className="popup_wrap">
                <div className="main_popup">
                    <div className="popup_top">
                        <div className="popup_icon">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 125 125">
                                <g>
                                    <g>
                                        <path fill="#2c37e3" d="M0 0h125v125H0z"/>
                                    </g>
                                    <g>
                                        <path fill="#fff"
                                              d="M85.827 42.49L55.643 72.743 41.172 58.274 36 63.469l17.089 17.1 2.554 2.433 2.553-2.433L90.999 47.62z"/>
                                    </g>
                                </g>
                            </svg>
                        </div>
                        <div className="popup_title">Ваш заказ</div>
                        <div
                            className="popup_cls"
                            onClick={closePopup}
                        >
                            <i></i>
                            <i></i>
                        </div>
                    </div>
                    <div className="popup_info">
                        {/* <p className="popup_site--type">Ваш заказ:</p>*/}
                        <div className="popup_site--sum">
                            <p className="popup_site--sum--cost">$<span>{websitesSum}</span></p>
                            <p className="popup_site--sum--day"><span>{websitesDay}</span> дней</p>
                        </div>
                    </div>
                    <div className="popup_bottom">
                        <p className="popup_bottom--title">Введите данные для оформление заказа:</p>
                        <Mutation mutation={sendRequest}>
                            {(createRequests, {data}) => (
                                <form
                                    className="website_form"
                                    onSubmit={e => {
                                        e.preventDefault();
                                        optionsToArr(websiteType);
                                        optionsToArr(websiteDesign);
                                        optionsToArr(websiteAdaptive);
                                        optionsToArr(websitesAdditional);
                                        createRequests({
                                            variables:
                                                {
                                                    data:
                                                        {
                                                            phone: inputPhone.value.toString(),
                                                            cost: websitesSum.toString(),
                                                            day: websitesDay.toString(),
                                                            options: optionsArr.toString()
                                                        }
                                                }
                                        });
                                        inputPhone.value = "";
                                        setSuccess();
                                    }}
                                >
                                    <div className="popup--input">
                                        <input
                                            type="text"
                                            name="tel"
                                            autoComplete="false"
                                            required
                                            onChange={setInput}
                                            ref={node => {
                                                inputPhone = node;
                                            }}
                                        />
                                        <span>Номер телефона/Email/Telegram</span>
                                    </div>
                                    <button><span>Оформить заказ</span>
                                        <i className="icon-btn_arrow"/>
                                    </button>
                                </form>
                            )}
                        </Mutation>
                    </div>
                </div>

                <div className="success_popup">
                    <div className="c_p--success">
                        <div className="c_p--success--icon">
                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 117 118">
                                <g>
                                    <g>
                                        <path fill="#202ce5"
                                              d="M75.324 117.998c-8.474 0-17.416-2.667-24.17-7.117-8.278-5.456-16.305-13.126-23.748-20.601-7.46-7.493-15.11-15.585-20.548-24-8.264-12.788-10.11-33.512.312-43.982l3.448-3.46a12.795 12.795 0 0 1 7.806-3.7l.157-.016a12.708 12.708 0 0 1 10.276 3.704l15.59 15.66c.749.752.749 1.972 0 2.724l-12.79 12.847c-2.813 2.827-3.145 7.373-.774 10.577 3.38 4.565 8.25 10.848 12.236 14.853 3.989 4.006 10.241 8.894 14.784 12.29 3.189 2.383 7.716 2.049 10.53-.776L81.22 74.155c.72-.723 1.993-.723 2.713 0l14.833 14.9a12.849 12.849 0 0 1 3.701 10.114 12.88 12.88 0 0 1-3.705 8.042l-3.676 3.692c-4.922 4.943-12.154 7.095-19.763 7.095zM19.821 18.916c-.29 0-.581.015-.874.043l-.156.015a8.946 8.946 0 0 0-5.46 2.59l-3.447 3.46c-8.974 9.014-7.1 27.87.192 39.158 5.245 8.115 12.733 16.03 20.043 23.372 7.298 7.33 15.153 14.839 23.14 20.103 11.558 7.618 30.034 9.642 39.115.519l3.677-3.693a9.015 9.015 0 0 0 2.592-5.626 8.988 8.988 0 0 0-2.589-7.077L82.577 78.243 71.145 89.726c-4.148 4.169-10.826 4.66-15.533 1.142-4.645-3.471-11.053-8.485-15.206-12.656-4.15-4.17-9.143-10.61-12.6-15.277-3.5-4.726-3.013-11.434 1.138-15.603L40.376 35.85 26.143 21.552a8.895 8.895 0 0 0-6.322-2.636zm71.235 32.215a1.923 1.923 0 0 1-1.919-1.927c0-5.533-2.145-10.735-6.04-14.648-3.898-3.915-9.076-6.068-14.584-6.068-.024 0-.05.002-.076 0a1.921 1.921 0 0 1-1.918-1.92 1.923 1.923 0 0 1 1.91-1.934h.084c6.535 0 12.673 2.554 17.296 7.197 4.62 4.64 7.165 10.81 7.166 17.372a1.923 1.923 0 0 1-1.92 1.928zm24.026 0a1.923 1.923 0 0 1-1.919-1.927c0-11.98-4.646-23.242-13.079-31.714C91.65 9.017 80.44 4.353 68.512 4.353c-.026 0-.047.012-.075 0a1.921 1.921 0 0 1-1.918-1.924 1.922 1.922 0 0 1 1.915-1.93h.078c12.953 0 25.125 5.064 34.286 14.267C111.955 23.964 117 36.196 117 49.204c0 1.064-.86 1.927-1.92 1.927z"/>
                                    </g>
                                </g>
                            </svg>
                            <div className="circle"/>
                        </div>
                        <div className="c_p--success--title">
                            Спасибо за заявку
                        </div>
                        <div className="c_p--success--description">
                            Мы уже вам звоним!
                        </div>
                    </div>
                </div>

            </div>
        </div>

        </React.Fragment>
    )
};

const websitesRequests = gql`
    {
        websitesRequests {
            id
        }
    }
`

export default graphql(websitesRequests)(PopupOrder);