import React from 'react'
import {NavLink} from "react-router-dom";
import {graphql} from 'react-apollo'
import gql from 'graphql-tag'


const TopSection = ({data: {loading, error, websitesMenu}}) => {
    if (error) return <h1></h1>
    if (!loading) {
        return (
            <div className="websites_top">

                <div className="w_t--wrap">
                    <div className="w_t--menu">

                        {websitesMenu.menuItems.map(function (name, index) {
                            return (
                                <NavLink
                                    to={websitesMenu.menuLinks[index]}
                                    className="w_t--m--item"
                                    activeClassName="w_t--m--item active">
                                    {name}
                                </NavLink>
                            );
                        })}


                    </div>

                    <NavLink
                        to={websitesMenu ? websitesMenu.moreLink : '/base'}>
                        <span>{websitesMenu ? websitesMenu.moreText : 'Подробнее'}</span>
                        <i className="icon-back"/>
                    </NavLink>
                </div>

                <img className="w_t--circle"
                     src={`https://media.graphcms.com/${websitesMenu.topImageCircle.handle}`}
                     alt={websitesMenu.topImageCircle.fileName}/>
                <img className="w_t--bg"
                     src={`https://media.graphcms.com/${websitesMenu.backgroundImage.handle}`}
                     alt={websitesMenu.backgroundImage.fileName}/>
            </div>
        )
    }
    return (
        <div></div>
    )
}

export const topSection = gql`
    query topSection($id: ID) {
        websitesMenu(where: {id: $id}) {
            id
            menuItems
            menuLinks
            moreText
            moreLink
            topImageCircle {
                handle
                fileName
            }
            backgroundImage {
                handle
                fileName
            }
        }
    }
`

export default graphql(topSection, {
    options: () => ({
        variables: {
            id: "ck3j5jjkci4an0b20m066tcjg"
        }
    })
})(TopSection)


