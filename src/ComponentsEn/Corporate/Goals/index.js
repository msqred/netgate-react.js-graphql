import React, {useState} from 'react';
import gql from "graphql-tag";
import {graphql} from 'react-apollo'


const Goals = ({data: {loading, error, corporateGoalses}}) => {
    const [isActive, setIsActive] = useState(null);

    function setState() {
        setIsActive(corporateGoalses[0].id);
    }


    if (loading) return '';
    if (error) return '';

    return (
        <React.Fragment>

            <div className="corporate_page--goals" onLoad={setState}>
                <div className="c_p--g--wrap">
                    {corporateGoalses.map(item => (
                        <div className={`c_p--g--item up ${isActive === item.id ? 'active' : ''}`} key={item.id} onClick={() => {setIsActive(item.id)}}>
                            <div className="c_p--g--title">
                                <span>{item.enTitle}</span>
                            </div>
                            <div className="c_p--g--info">
                                <div className="c_p--g--info--icon">
                                    <img
                                        src={`https://media.graphcms.com/${item.icon.handle}`}
                                        alt=""
                                    />
                                </div>
                                <div className="c_p--g--info--title">
                                    <span>{item.enTitle}</span>
                                </div>
                                <div className="c_p--g--info--text">
                                    <p>{item.enText}</p>
                                </div>
                            </div>
                        </div>
                    ))}


                </div>
            </div>

        </React.Fragment>
    )
}


const goals = gql`
    {
        corporateGoalses {
            id
            icon {
                handle
            }
            enTitle
            enText
        }
    }
`

export default graphql(goals)(Goals)
