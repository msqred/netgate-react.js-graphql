import React, {useState} from 'react';
import gql from "graphql-tag";
import {graphql} from 'react-apollo'
import LeftSide from "./LeftSide";
import RightSide from "./RightSide";


const HowWeDo = ({data: {loading, error, corporateHowWeDoes}}) => {


    if (loading) return '';
    if (error) return '';

    return (
        <div className="corporate_how">
            <div className="corporate_how--wrap">
                <LeftSide/>
                <RightSide/>
            </div>
            <div className="corporate_how--bg">
                <img
                    src={`https://media.graphcms.com/${corporateHowWeDoes[0].backgroundImage.handle}`}
                    alt=""
                />
            </div>
        </div>
    )
}


const howWeDo = gql`
    {
        corporateHowWeDoes {
            backgroundImage {
                handle
            }
        }
    }
`

export default graphql(howWeDo)(HowWeDo)
