import React, {useState} from 'react';
import gql from "graphql-tag";
import {graphql} from 'react-apollo'
import Goals from "./Goals";
import TopSection from './TopSection'
import Quote from "./Quote";
import {Helmet} from "react-helmet";
import HowWeDo from "./HowWeDo";


const Corporate = ({data: {loading, error, corporateInfoes}}) => {


    if (loading) return '';
    if (error) return 'Something Bad Happened';

    return (
        <React.Fragment>
            <Helmet>
                <title>{corporateInfoes[0].enHeaderTitle}</title>
                <meta name="description" content={corporateInfoes[0].headerDescription}/>
            </Helmet>
            <div className="shop_page corporate_page">
                <div className="shop_page--wrap">
                    <TopSection/>
                    <h1 className="s_p--title">{corporateInfoes[0].enTitle}</h1>
                </div>

                <Goals/>
                <Quote/>
                <img
                    className="shop_page--bg"
                    src={`https://media.graphcms.com/${corporateInfoes[0].backgroundImage.handle}`}
                    alt=""
                />
            </div>

            <HowWeDo/>


        </React.Fragment>
    )
}


const corporate = gql`
    {
        corporateInfoes {
            enTitle
            backgroundImage {
                handle
            }
            enHeaderTitle
            headerDescription
        }
    }
`

export default graphql(corporate)(Corporate)
