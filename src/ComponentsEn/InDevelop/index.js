import React, {useState} from 'react';
import gql from "graphql-tag";
import {graphql} from 'react-apollo'
import {Helmet} from "react-helmet";
import {ReactComponent as InDevelopIcon} from '../../Assets/svg/inDevelop.svg';


const InDevelop = ({data: {loading, error, inDevelops}}) => {


    if (loading) return '';
    if (error) return 'Something Bad Happened';

    return (
        <React.Fragment>
            <Helmet>
                <title>{inDevelops[0].enHeaderTitle}</title>
                <meta name="description" content={inDevelops[0].headerDescription}/>
            </Helmet>

            <div className="indevelop_page">
                <div className="indevelop_page--wrap">

                    <div className="indevelop_img up">
                        <InDevelopIcon/>
                    </div>
                    <a
                        href={`/en${inDevelops[0].buttonUrl}`}
                        className="indevelop_btn up">
                        <span>{inDevelops[0].enButtonText}</span>
                    </a>
                    <div className="indevelop_text">
                        <span className="indevelop_text--title up">{inDevelops[0].enTitle}</span>
                        <span className="indevelop_text--description up">{inDevelops[0].enText}</span>
                    </div>
                </div>
            </div>

        </React.Fragment>
    )
}


const inDevelop = gql`
    {
        inDevelops {
            enButtonText
            buttonUrl
            enTitle
            enText
            enHeaderTitle
            headerDescription
        }
    }
`

export default graphql(inDevelop)(InDevelop)
