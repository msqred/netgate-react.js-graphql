import React, {useState, useEffect} from 'react'
import {NavLink} from "react-router-dom";
import {Link} from "react-router-dom";
import {graphql} from 'react-apollo'
import gql from 'graphql-tag'

let cost = 0,
    sum = 0,
    dayType = 0,
    dayCount = 0,
    dayAll = 0;

const Calculator = ({data: {loading, error, promoCalculators}}) => {

    let countDay, button;

    const [isActive, setIsActive] = useState(null);
    const [stateCount, setStateCount] = useState(1);
    const [stateSum, setStateSum] = useState(0);
    const [stateDay, setStateDay] = useState(0);

    useEffect(() => {
        setOnLoad();
    }, [promoCalculators]);


    const setOnLoad = () => {
        if (button) {
            setIsActive(promoCalculators[0].enTypeItems[0]);
            cost = parseInt(promoCalculators[0].typeItemCost[0]);
            dayType = parseInt(promoCalculators[0].typeItemsDay[0]);
            dayCount = parseInt(countDay.getAttribute('data-day'));
            dayAll = dayType + dayCount;
            sum = cost * stateCount;
            setStateSum(sum);
            setStateDay(dayAll);
        }
    };


    const mySetActive = (e) => {
        setIsActive(e.target.getAttribute('id'));
        mySetCost(e);
    };

    const mySetCost = (e) => {
        cost = (parseInt(e.target.getAttribute('data-cost')));
        dayType = (parseInt(e.target.getAttribute('data-day')));
        dayCount = parseInt(countDay.getAttribute('data-day'));
        dayAll = dayType + dayCount;
        sum = cost * stateCount;
        setStateSum(sum);
        setStateDay(dayAll);
    };

    const decrement = () => {
        if (stateCount < 2) {
            setStateCount(1);
        } else {
            setStateCount(stateCount - 1);
        }
    };


    const increment = () => {
        setStateCount(stateCount + 1);
    };

    useEffect(() => {
            setAllSum();
        }
    );

    const setAllSum = () => {
        sum = cost * stateCount;
        setStateSum(sum);
        dayAll = 0;
        let dayC = (dayCount * stateCount);
        dayAll = dayC + dayType;
        setStateDay(dayAll);
    };


    if (error) return <h1></h1>
    if (!loading) {
        return (


            <div className="promo_calc calc">
                <div className="p_c--wrap">
                    <h1 className="p_c--wrap--title up">{promoCalculators[0].enTitle}</h1>

                    <div className="p_c--wrap-calculator up">
                        <div className="p_c--w--c--left">
                            <div className="p_c--c--type">
                                <div className="p_c--c--title">
                                    <span>{promoCalculators[0].enTypeName}</span>
                                    <a
                                        href={`/en${promoCalculators[0].typeHelpUrl}`}
                                        className="p_c--c--type__info"
                                    >
                                        <i className="icon-info"/>
                                        <span>{promoCalculators[0].enTypeHelpText}</span>
                                    </a>
                                </div>
                                <div className="p_c--c--buttons">
                                    {promoCalculators[0].enTypeItems.map(function (name, index) {
                                        return (
                                            <div
                                                className={`p_c--c--btn ${isActive === name ? 'active' : ''}`}
                                                data-cost={promoCalculators[0].typeItemCost[index]}
                                                data-day={promoCalculators[0].typeItemsDay[index] ? promoCalculators[0].typeItemsDay[index] : 1}
                                                id={name}
                                                onClick={mySetActive}
                                                ref={node => {
                                                    button = node;
                                                }}

                                            >
                                                {name}
                                            </div>
                                        );
                                    })}
                                </div>
                            </div>
                            <div className="p_c--c--time">
                                <div className="p_c--c--title">
                                    <span>{promoCalculators[0].enLengthName}</span>
                                    <a
                                        href={`/en${promoCalculators[0].lengthHelpUrl}`}
                                        className="p_c--c--type__info"
                                    >
                                        <i className="icon-info"/>
                                        <span>{promoCalculators[0].enLengthHelpText}</span>
                                    </a>
                                </div>
                                <div className="p_c--c--time--input">
                                    <div className="p_c--c--input">
                                        <div className="p_c--c--input--minute">
                                            <span data-time="minute">{stateCount}</span>
                                        </div>

                                        <div
                                            className="p_c--c--input-buttons"
                                            data-day={promoCalculators[0].lengthDay ? promoCalculators[0].lengthDay : 1}
                                            ref={node => {
                                                countDay = node;
                                            }}
                                        >
                                            <div
                                                className="p_c--c--btn--plus"
                                                onClick={increment}
                                            >
                                                <i className="icon-arrow"/>
                                            </div>
                                            <div
                                                className="p_c--c--btn--minus"
                                                onClick={decrement}
                                            >
                                                <i className="icon-arrow"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="p_c--c--text">
                                        <span>Minutes</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="p_c--w--c--right">
                            <div className="p_c--right--title">
                                <span>{promoCalculators[0].enResultName}</span>
                                <div className="p_c--right-info">
                                    <span>{promoCalculators[0].enResultText}</span>
                                    <a
                                        href={`/en${promoCalculators[0].resultHelpUrl}`}
                                        className="p_c--r--t--info-text"
                                    >
                                        <i className="icon-info"/>
                                        <span>{promoCalculators[0].enResultHelpText}</span>
                                    </a>
                                </div>
                            </div>
                            <div className="p_c--right--sum">
                        <span className="p_c--sum--price">
                            {`$${stateSum}`}
                        </span>
                                <span className="p_c--sum--day">
                            {`${stateDay} days`}
                        </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
    return (
        <div></div>
    )
}

export const calculator = gql`
    {
        promoCalculators {
            enTitle
            enTypeName
            enTypeHelpText
            typeHelpUrl
            enTypeItems
            typeItemCost
            typeItemsDay
            enLengthName
            lengthDay
            enLengthHelpText
            lengthHelpUrl
            enResultName
            enResultText
            enResultHelpText
            resultHelpUrl
        }
    }
`

export default graphql(calculator)(Calculator)


