import React from 'react';
import gql from "graphql-tag";
import {graphql} from 'react-apollo'


const FooterSection = ({data: {loading, error, shopPageFooters}}) => {


    if (loading) return 'Loading...';
    if (error) return 'Something Bad Happened';

    return (
        <div className="shop_footer">

            <div className="shop_footer--wrap up">
                <a
                    href={`/en${shopPageFooters[0].buttonUrl}`}
                    className="s_f--btn">
                    <span>{shopPageFooters[0].enButtonText}</span>
                    <i className="icon-calculator"/>
                </a>
            </div>

            <img
                src={`https://media.graphcms.com/${shopPageFooters[0].backgroundImage.handle}`}
                alt={shopPageFooters[0].fileName}
                className="shop_footer--bg"
            />
        </div>
    )
}


const footerSection = gql`
    {
        shopPageFooters {
            buttonUrl
            enButtonText
            backgroundImage {
                handle
                fileName
            }
        }
    }
`

export default graphql(footerSection)(FooterSection)
