import React from 'react';
import gql from "graphql-tag";
import {graphql} from 'react-apollo'


const MainSection = ({data: {loading, error, shopPageInfoes, shopPageMiddleTopItemses, shopPageMiddleBottomItemses}}) => {


    if (loading) return 'Loading...';
    if (error) return 'Something Bad Happened';

    return (
        <React.Fragment>
            <h1 className="s_p--title">{shopPageInfoes[0].enTitle}</h1>

            <div className="s_p--wrap">

                {shopPageMiddleTopItemses.map(category => (
                    <div className={`s_p--wrap--item ${category.blue ? 'blue' : ''} up`} key={category.id}>
                        <div className="s_p--w--i--title">{category.enTitle}</div>

                        <div className="s_p--w--i--list">
                            {category.enItems.map(item => (
                                <p className="up">{item}</p>
                            ))}
                        </div>
                    </div>
                ))}
            </div>

            <div className="function_wrap">
                <div className="function_title up">{shopPageInfoes[0].enFunctionalBlockTitle}</div>
                <div className="function_box">

                    {shopPageMiddleBottomItemses.map(item => (
                        <div className="function_item up">
                            <div className="function_item--icon">
                                <img
                                    src={`https://media.graphcms.com/${item.icon.handle}`}
                                    alt=""
                                />
                            </div>
                            <h3 className="function_icon--title">{item.enTitle}</h3>
                            <p>{item.enText}</p>
                        </div>
                    ))}

                </div>
            </div>
        </React.Fragment>
    )
}


const mainSection = gql`
    {
        shopPageInfoes {
            enTitle
            enFunctionalBlockTitle
        }
        shopPageMiddleTopItemses {
            id
            enTitle
            enItems
            blue
        }
        shopPageMiddleBottomItemses {
            id
            icon {
                handle
            }
            enTitle
            enText
        }
    }
`

export default graphql(mainSection)(MainSection)
