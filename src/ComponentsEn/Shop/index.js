import React from 'react';
import {Helmet} from "react-helmet";
import gql from "graphql-tag";
import {graphql} from 'react-apollo'
import TopSection from "./TopSection";
import MainSection from "./MainSection";
import FooterSection from './FooterSection'


const Shop = ({data: {loading, error, shopPageInfoes}}) => {


    if (loading) return '';
    if (error) return 'Something Bad Happened';

    return (
        <React.Fragment>
            <div className="shop_page">
                <div className="shop_page--wrap">
                    <Helmet>
                        <title>Netgate - Shop</title>
                        <meta name="description" content="Shop"/>
                    </Helmet>

                    <TopSection/>
                    <MainSection/>
                </div>

                <FooterSection/>

                <img className="shop_page--bg"
                     src={`https://media.graphcms.com/${shopPageInfoes[0].backgroundImage.handle}`}
                     alt={shopPageInfoes[0].fileName}
                />
            </div>


        </React.Fragment>
    )
}


const shop = gql`
    {
        shopPageInfoes {
            backgroundImage {
                handle
                fileName
            }
        }
    }
`

export default graphql(shop)(Shop)
