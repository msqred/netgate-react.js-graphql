import React, {useState} from 'react'
import gql from "graphql-tag";
import {graphql} from "react-apollo";

import {connect} from 'react-redux';


const mapStateToProps = (state) => {
    return {
        websiteType: state.websiteType,
        websiteDesign: state.websiteDesign,
        websiteAdaptive: state.websiteAdaptive,
        websitesAdditional: state.websitesAdditional
    };
};


const BottomResultLeft = ({data: {loading, error, websiteBottoms}, websiteType, websiteDesign, websiteAdaptive, websitesAdditional, additionalSum}) => {


    if (loading) return 'Loading...';
    if (error) return 'Something Bad Happened';

    let sum = 0;
    let name = [];
    let showName = '';

    const setInfo = (arr) => {
        sumAdaptive(arr);
        nameAdaptive(arr);
        if (name.length > 2) {
            showName = name[1] + ' + ' + name[2];
        } else if (name.length === 1) {
            showName = '';
        } else if (name.length === 2) {
            showName = name[1];
        }
    }

    const sumAdaptive = (arr) => {
        for (let i = 0; i < arr.length; i++) {
            sum = sum + parseInt(arr[i].cost);
            console.log(sum);
        }
    }

    const nameAdaptive = (arr) => {
        for (let i = 0; i < arr.length; i++) {
            name.push(arr[i].name);
        }
    }


    return (


        <div className="w_b--left up" onChange={setInfo(websiteAdaptive)}>
            <span className="w_b--title">{websiteBottoms[0].enResultTitle}</span>

            <div className="w_b--list">
                <div className="w_b--list--main">
                    <div className="w_b--list--line">
                        <p className="w_b--list--main--type">{websiteBottoms[0].enTypeName}
                            <span>{websiteType[0].name}</span>
                        </p>
                        <p className="w_b--list--main--cost">{websiteType[0].cost} USD</p>
                    </div>
                    <div className="w_b--list--line">
                        <p className="w_b--list--main--type">{websiteBottoms[0].enDesingName}
                            <span>{websiteDesign[0].name}</span>
                        </p>
                        <p className="w_b--list--main--cost">{websiteDesign[0].cost} USD</p>
                    </div>
                    <div className="w_b--list--line">
                        <p className="w_b--list--main--type">{websiteBottoms[0].enAdaptiveName}
                            <span>
                                {showName}
                            </span>
                        </p>
                        <p className="w_b--list--main--cost">
                            <span>
                                {sum ? sum : 0} USD
                            </span>
                        </p>
                    </div>
                </div>

                <div className="w_b--list--additional">
                    <div className="w_b--list--line">
                        <p className="w_b--list--main--type w_b--additional">
                            {websiteBottoms[0].enAdditionalName}
                            {websitesAdditional.map(item => {
                                return (
                                    <span>{item.name}</span>
                                )
                            })}
                        </p>
                        <p className="w_b--list--main--cost w_b--additional--cost">{additionalSum ? additionalSum : 0} USD</p>
                    </div>
                </div>
            </div>
        </div>
    )

}


const websitesInfo = gql`
    {
        websiteInfoes {
            title
            backgroundImage {
                handle
                fileName
            }
            headerTitle
            headerDescription
        }
        websiteBottoms {
            id
            enResultTitle
            enTypeName
            enDesingName
            enAdaptiveName
            enAdditionalName
            paymentName
            paymentDesciption
            paymentHelpUrl
            paymentHelpText
            nameInCost
            buttonText
            buttonUrl
        }
    }
`

export default connect(mapStateToProps)(graphql(websitesInfo)(BottomResultLeft))