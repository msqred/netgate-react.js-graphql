import React from 'react'


class MiddleCategoryName extends React.Component {


    constructor(props) {
        super(props);


        this.state = {
            titleClassName: this.props.titleClassName,
            categoryName: this.props.categoryName,
            helpText: this.props.helpText,
            helpUrl: this.props.helpUrl
        };

    }


    render() {
        return (
            <div className={`w_m--m--title ${this.state.titleClassName ? this.state.titleClassName : ''}`}>

                <span>{this.state.categoryName}</span>

                <a href={`/en${this.state.helpUrl}`} className="p_c--c--type__info">
                    <i className="icon-info"/>
                    <span>{this.state.helpText}</span>
                </a>
            </div>
        )
    }
}

export default MiddleCategoryName;
