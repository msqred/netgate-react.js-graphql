import React, { useState, useEffect } from 'react';
import {graphql} from 'react-apollo';
import gql from 'graphql-tag';
import {ReactComponent as WebsitesIcon} from '../../../Assets/img/websites-icon.svg';
import TopInput from './TopInput';
import TopCategoryName from "./TopCategoryName";
import MiddleCategoryName from "./MiddleCategoryName";
import MiddleInput from "./MiddleInput";
import {connect} from "react-redux";



const Calculator = ({data: {loading, error, websitesCalculatorTopCategories, websitesCalculatorMidleCategories, websiteInfoes}}) => {



    if (error) return <h1></h1>
    if (!loading) {
        return (

            <React.Fragment>


                <div className="websites_m">
                    <div className="w_m--icon up">
                        <WebsitesIcon/>
                    </div>


                    <div className="w_m--wrap">
                        <h1 className="title up">{websiteInfoes[0].enTitle}</h1>
                        {/*Верхняя часть калькулятора*/}
                        <div className="w_m--top up">

                            {websitesCalculatorTopCategories.map(category => (
                                <div className="w_m--t--item w_m--type" key={category.id}>

                                    <TopCategoryName
                                        categoryName={category.enCategoryName}
                                        helpUrl={category.helpUrl}
                                        helpText={category.enHelpText}/>

                                    <form>
                                        <React.Fragment>
                                            {category.websitesCalculatorTopItemses.map(item => (
                                                <TopInput key={item.id}
                                                          name={item.enName}
                                                          cost={item.cost}
                                                          day={item.day}
                                                          id={item.id}
                                                          checked={item.checked}
                                                          nameId={category.id}
                                                          category={category.enCategoryName}
                                                />
                                            ))}
                                        </React.Fragment>
                                    </form>
                                </div>
                            ))}

                        </div>

                        {/*Конец верхней части калькулятора*/}


                        {/*Средняя часть калькулятора*/}

                        <div className="w_m--main up">

                            {websitesCalculatorMidleCategories.map(category => (
                                <div className="w_m--m--item">
                                    <MiddleCategoryName
                                        key={category.id}
                                        titleClassName={category.titleClassName}
                                        categoryName={category.enCategoryName}
                                        helpText={category.enHelpText}
                                        helpUrl={category.helpUrl}
                                    />
                                    {category.websitesCalculatorMidleItems.map(item => (
                                        <MiddleInput
                                            key={item.id}
                                            id={item.id}
                                            name={item.enName}
                                            cost={item.cost}
                                            day={item.day}
                                            useCount={item.useCount}
                                            countInputText={item.enCountInputText}
                                            nameId={category.id}
                                        />
                                    ))}
                                </div>
                            ))}


                        </div>

                        {/*Конец средней части калькулятора*/}


                    </div>
                </div>

            </React.Fragment>
        )
    }
    return (
        <div></div>
    )
}

export const calculator = gql`
    {
        websiteInfoes {
            enTitle
        }
        websitesCalculatorTopCategories {
            id
            enCategoryName
            websitesCalculatorTopItemses {
                id
                enName
                cost
                day
                checked
            }
            enHelpText
            helpUrl
        }
        websitesCalculatorMidleCategories {
            id
            titleClassName
            enCategoryName
            enHelpText
            helpUrl
            websitesCalculatorMidleItems {
                id
                enName
                cost
                day
                useCount
                enCountInputText
            }
        }
    }
`

export default graphql(calculator)(Calculator)

