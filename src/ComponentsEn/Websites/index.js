import React, {useState} from 'react';
import PopupOrder from './PopupOrder'
import TopSection from "./TopSection";
import Calculator from './Calculator'
import BottomResult from "./BottomResult";
import {Helmet} from "react-helmet";
import gql from "graphql-tag";
import {graphql} from 'react-apollo'
import {connect} from 'react-redux';


const mapStateToProps = (state) => {
    return {
        websiteType: state.websiteType,
        websiteDesign: state.websiteDesign,
        websiteAdaptive: state.websiteAdaptive,
        websitesAdditional: state.websitesAdditional
    };
};


const Websites = ({data: {loading, error, websiteInfoes}, state, websitesAdditional, websiteType, websiteDesign, websiteAdaptive}) => {

    const [activePopup, setActivePopup] = useState(false);
    const [successPopup, setSuccessPopup] = useState(false);

    const openPopup = () => {
      setActivePopup(true);
    };

    const closePopup = () => {
        setActivePopup(false);
    };

    const setSuccess = () => {
        setSuccessPopup(true);
    };


    if (loading) return '';
    if (error) return '';

    let sumArr = []; // Массив выбранных дополнительных опций
    let sumDayArr = []; // Массив выбранных дополнительных опций дней
    let additionalSum = 0; // Сумма только выбранных дополнительных опций

    let basicSumArr = []; // Массив выбранных базовых опций
    let basicSumDay = []; // Массив выбранных базовых опций дней
    let websitesSum = 0; // Сумма всех выбранных опций
    let websitesDay = 0; // Сумма всех выбранных опций дней
    let basicSum = 0;


    const onAllSum = (arr) => {
        sumAdditional(arr);
        getItemSum(websiteType);
        getItemSum(websiteDesign);
        getItemSum(websiteAdaptive);
        let allSumArray = [...sumArr, ...basicSumArr];
        let allSumDay = [...sumDayArr, ...basicSumDay];
        for (let i = 0; i < allSumArray.length; i++) {
            websitesSum = websitesSum + allSumArray[i];
        }
        for (let i = 0; i < allSumDay.length; i++) {
            websitesDay = websitesDay + allSumDay[i];
        }
    };

    const getItemSum = (arr) => {
        let itemSumCost;
        let itemSumDay;
        for (let i = 0; i < arr.length; i++) {
            itemSumCost = parseInt(arr[i].cost) * parseInt(arr[i].count);
            itemSumDay = parseInt(arr[i].day) * parseInt(arr[i].count);
            basicSumArr.push(itemSumCost);
            basicSumDay.push(itemSumDay);
        }
        sumBasicArr(arr);
    };

    const sumBasicArr = (arr) => {
        basicSum = 0;
        for (let i = 0; i < arr.length; i++) {
            basicSum += parseInt(arr[i]);
        }
    };

    const sumAdditional = (arr) => {
        let itemSum;
        let itemDay;
        for (let i = 0; i < arr.length; i++) {
            itemSum = parseInt(arr[i].cost) * parseInt(arr[i].count);
            itemDay = parseInt(arr[i].day) * parseInt(arr[i].count);
            sumArr.push(itemSum);
            sumDayArr.push(itemDay);
        }
        sumAdditionalArray(sumArr);
    };


    const sumAdditionalArray = (arr) => {
        additionalSum = 0;
        for (let i = 0; i < arr.length; i++) {
            additionalSum += parseInt(arr[i]);
        }
    };

    return (
        <div className="websites_main" onChange={onAllSum(websitesAdditional)}>
            <Helmet>
                <title>{websiteInfoes[0].headerTitle}</title>
                <meta name="description" content={websiteInfoes[0].headerDescription}/>
            </Helmet>
            <PopupOrder
                activePopup={activePopup}
                closePopup={closePopup}
                websitesSum={websitesSum}
                websitesDay={websitesDay}
                websiteType={websiteType}
                websiteDesign={websiteDesign}
                websiteAdaptive={websiteAdaptive}
                websitesAdditional={websitesAdditional}
                successPopup={successPopup}
                setSuccess={setSuccess}
            />
            <TopSection/>
            <Calculator/>
            <BottomResult
                state={state}
                additionalSum={additionalSum}
                websitesSum={websitesSum}
                websitesDay={websitesDay}
                openPopup={openPopup}
            />
        </div>
    )
}


const websitesInfo = gql`
    {
        websiteInfoes {
            headerTitle
            headerDescription
        }
    }
`

export default connect(mapStateToProps)(graphql(websitesInfo)(Websites))
