import React from 'react'
import {NavLink} from "react-router-dom";
import {Link, animateScroll as scroll} from "react-scroll";
import {graphql} from 'react-apollo'
import gql from 'graphql-tag'
import {ReactComponent as BrandingIcon} from '../../../Assets/svg/brandingIcon.svg';
import Menu from "./Menu";


const TopSection = ({data: {loading, error, brandingInfoes}}) => {
    if (error) return <h1></h1>
    if (!loading) {
        return (


            <div className="websites_top">
                <div className="w_t--wrap">
                    <Menu/>
                </div>

                <div className="promo_info--wrap">
                    <div className="p_i--wrap--icon up">
                        <BrandingIcon/>
                    </div>
                    <div className="p_i--wrap--text">
                        <div className="p_i--wrap--title up">
                            <span>{brandingInfoes[0].uaTitle}</span>
                            <Link to="calc"
                                  className="p_i--wrap--btn-calc"
                                  spy={true}
                                  smooth={true}
                                  offset={-70}
                                  duration={500}
                            >
                                <i className="icon-calculator"/>
                            </Link>
                        </div>
                        <div className="p_i--wrap--info up">
                            <p>{brandingInfoes[0].uaText}</p>
                        </div>
                        <div className="p_i--wrap--description up">
                            <p>{brandingInfoes[0].uaDescription}</p>
                        </div>
                    </div>
                </div>

                <img
                    className="w_t--circle"
                    src={`https://media.graphcms.com/${brandingInfoes[0].circleImage.handle}`}
                    alt=""
                />
                <img
                    className="w_t--bg"
                    src={`https://media.graphcms.com/${brandingInfoes[0].backgroundImage.handle}`}
                    alt=""
                />
            </div>
        )
    }
    return (
        <div></div>
    )
}

export const topSection = gql`
    {
        brandingInfoes {
            uaTitle
            uaText
            uaDescription
            circleImage {
                handle
            }
            backgroundImage {
                handle
            }
        }
    }
`

export default graphql(topSection)(TopSection)


