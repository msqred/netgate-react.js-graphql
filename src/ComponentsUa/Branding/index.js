import React, {useState, useRef} from 'react';
import gql from "graphql-tag";
import {graphql} from 'react-apollo'
import {Helmet} from "react-helmet";
import TopSection from "./TopSection";
import Calculator from "./Calculator";
import BrandingInfo from './Info'


const Branding = ({data: {loading, error, brandingInfoes}}) => {





    if (loading) return '';
    if (error) return 'Something Bad Happened';

    return (

        <React.Fragment>
            <Helmet>
                <title>{brandingInfoes[0].uaHeaderTitle}</title>
                <meta name="description" content={brandingInfoes[0].headerDescription}/>
            </Helmet>


            <div className="websites_main promo_main branding_main">
                <TopSection/>
                <Calculator/>
                <BrandingInfo/>
            </div>


        </React.Fragment>
    )
}


const branding = gql`
    {
        brandingInfoes {
            uaHeaderTitle
            headerDescription
        }
    }
`

export default graphql(branding)(Branding)
