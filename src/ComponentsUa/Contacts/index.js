import React, {useState, useRef} from 'react';
import gql from "graphql-tag";
import {graphql} from 'react-apollo'
import {Helmet} from "react-helmet";
import TopSection from "./TopSection";
import BottomSection from "./BottomSection";
import ContactsPopup from "./Popup";


const Contacts = ({data: {loading, error, contactses}}) => {

    const [activePopup, setActivePopup] = useState('false');
    const [hiddenBlock, setHiddenBlock] = useState('false');

    const openPopup = () => {
        setActivePopup(true);
        setHiddenBlock(true);
    }

    const closePopup = () => {
        setActivePopup(false);
        setHiddenBlock(false);
    }


    if (loading) return '';
    if (error) return 'Something Bad Happened';

    return (

        <React.Fragment>
            <Helmet>
                <title>{contactses[0].uaHeaderTitle}</title>
                <meta name="description" content={contactses[0].headerDescription}/>
            </Helmet>

            <div className="contacts_page">

                <div className="contacts_wrap">

                    <TopSection
                        hiddenBlock={hiddenBlock}
                    />

                    <BottomSection
                        openPopup={openPopup}
                        hiddenBlock={hiddenBlock}
                    />
                </div>

                <ContactsPopup
                    activePopup={activePopup}
                    closePopup={closePopup}
                />

            </div>


        </React.Fragment>
    )
}


const contacts = gql`
    {
        contactses {
            uaHeaderTitle
            headerDescription
        }
    }
`

export default graphql(contacts)(Contacts)
