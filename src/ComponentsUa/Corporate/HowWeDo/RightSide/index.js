import React, {useState} from 'react';
import gql from "graphql-tag";
import {graphql} from 'react-apollo'


const RightSide = ({data: {loading, error, corporateHowWeDoItemses}}) => {


    if (loading) return '';
    if (error) return '';

    return (
        <div className="c_h--w--right">

            {corporateHowWeDoItemses.map(item => (
                <div className="c_h--w--right--item up" key={item.id}>
                    <div className="c_h--w--right--item--title">
                        <span>{item.uaTitle}</span>
                    </div>
                    <div className="c_h--w--right--item--text">
                        <p>{item.uaText}</p>
                    </div>
                </div>
            ))}

        </div>
    )
}


const rightSide = gql`
    {
        corporateHowWeDoItemses {
            id
            uaTitle
            uaText
        }
    }
`

export default graphql(rightSide)(RightSide)
