import React from 'react'
import {NavLink} from "react-router-dom";
import {Link} from "react-router-dom";
import {graphql} from 'react-apollo'
import gql from 'graphql-tag'


const TopSection = ({data: {loading, error, websitesMenu}}) => {
    if (error) return <h1></h1>
    if (!loading) {
        return (


            <div className="s_p--header">
                <div className="s_p--h--menu">
                    <Link className="s_p--header--arrow" to="/ua/websites"><i className="icon-back"/></Link>

                    {websitesMenu.uaMenuItems.map(function (name, index) {
                        return (
                            <NavLink
                                to={`/ua${websitesMenu.menuLinks[index]}`}
                                activeClassName="active">
                                {name}
                            </NavLink>
                        );
                    })}
                </div>

                <img className="s_p--header--bg"
                     src={`https://media.graphcms.com/${websitesMenu.topImageCircle.handle}`}
                     alt={websitesMenu.topImageCircle.fileName}
                />
            </div>
        )
    }
    return (
        <div></div>
    )
}

export const topSection = gql`
    query topSection($id: ID) {
        websitesMenu(where: {id: $id}) {
            id
            uaMenuItems
            menuLinks
            topImageCircle {
                handle
                fileName
            }
        }
    }
`

export default graphql(topSection, {
    options: () => ({
        variables: {
            id: "ck3j5jjkci4an0b20m066tcjg"
        }
    })
})(TopSection)


