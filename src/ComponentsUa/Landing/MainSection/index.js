import React, {Component} from 'react'

import Line from '../../../Assets/img/l_p-line.png';


class MainSection extends React.Component {


    render() {


        return (
            <React.Fragment>
                <h1 className="l_p--title">{this.props.mainTitle}</h1>

                <div className="l_p--items-wrap">

                    {this.props.data.map(item => (
                        <div className={`l_p--item ${item.isReverse ? 'reverse' : ''} up`}>
                            <div className="l_p--icon--wrap">
                                <div className="l_p--icon">
                                    <img
                                        className="line_icon"
                                        src={`https://media.graphcms.com/${item.lineSvg.handle}`}
                                        alt=""
                                    />
                                    <img
                                        className="square_icon"
                                        src={`https://media.graphcms.com/${item.squareSvg.handle}`}
                                        alt=""
                                    />
                                    <img
                                        className="main_icon"
                                        src={`https://media.graphcms.com/${item.mainIconSvg.handle}`}
                                        alt=""
                                    />
                                </div>
                            </div>
                            <div className="l_p--text">
                                <h2 className="l_p--text--title">
                                    {item.uaTitle}
                                </h2>
                                <div className="l_p--text--info">
                                    <p>{item.uaText}</p>
                                </div>
                            </div>
                        </div>
                    ))}


                    <img className="l_p-line"
                         src={Line}
                         alt=""/>
                </div>
            </React.Fragment>
        )
    }
}


export default MainSection;


