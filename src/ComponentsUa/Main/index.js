import React, {useState} from 'react';
import gql from 'graphql-tag'
import {graphql} from 'react-apollo'
import {Link} from 'react-router-dom'


const Main = ({data: {loading, error, mainPages}}) => {

    if (error) return <h1></h1>
    if (!loading) {

        return (
            <div className="main_pag">
                <div className="main_screen">
                    <div className="main_screen--left">
                        <div className="m_s--l--wrap">
                            <Link to={`/ua/${mainPages[0].leftSideLink}`} className="m_s--l--arrow"
                            >
                                <iframe src="http://netgate.com.ua/uaArrow/left-arrow/"
                                        scrolling="no"
                                />
                            </Link>

                            <div className="m_s--l--info">
                                <Link to={`/ua/${mainPages[0].leftSideLink}`}
                                      className="m_s--l--category up">{mainPages[0].uaLeftSideCategory}</Link>
                                <Link to={`/ua/${mainPages[0].leftSideLink}`}
                                      className="m_s--l--text up">{mainPages[0].uaLeftSideText}</Link>
                            </div>
                        </div>
                    </div>
                    <div className="main_screen--right">

                        <div className="m_s--l--wrap">
                            <Link
                                to={`/ua/${mainPages[0].rightSideLink}`}
                                className="m_s--l--arrow"

                            >
                                <iframe
                                    src="http://netgate.com.ua/uaArrow/right-arrow/"
                                    scrolling="no"
                                />
                            </Link>
                            <div className="m_s--l--info">
                                <Link to={`/ua/${mainPages[0].rightSideLink}`}
                                      className="m_s--l--category up">{mainPages[0].uaRightSideCategory}</Link>
                                <Link to={`/ua/${mainPages[0].rightSideLink}`}
                                      className="m_s--l--text up">{mainPages[0].uaRightSideText}</Link>
                            </div>
                        </div>
                    </div>
                    <div className="main_screen--bg">
                        <img className="parallax"
                             src={`https://media.graphcms.com/${mainPages[0].backgroundImage.handle}`} alt=""/>
                    </div>

                </div>

            </div>
        )
    }

    return <h2></h2>
};


export const main = gql`
    {
        mainPages {
            leftSideLink
            uaLeftSideText
            uaLeftSideCategory
            uaRightSideText
            rightSideLink
            uaRightSideCategory
            backgroundImage {
                handle
            }
        }
    }
`

export default graphql(main)(Main)
