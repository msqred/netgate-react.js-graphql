import React from 'react'
import ScrollReveal from 'scrollreveal';
import {ReactComponent as Logo} from '../../Assets/svg/Logo.svg';

class Preloader extends React.Component {


    constructor(props) {
        super(props);

        this.state = {
            class: 'preloader active'
        };

        this.timer = null;

    }

    componentWillMount() {
        this.timer = setTimeout(() => this.setState({
            class: 'preloader active load'
        }), 50);
    }

    componentDidUpdate() {
        clearInterval(this.timer);
        this.timer = setTimeout(() => this.setState({
            class: 'preloader load'
        }), 1700);
    }

    render() {
        return (
            <div className={this.state.class}>
                <div className="logo">
                    <Logo/>
                </div>
                <span></span>
                <span></span>
            </div>
        )
    }
}

export default Preloader;
