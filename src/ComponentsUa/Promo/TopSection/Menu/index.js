import React from 'react'
import {NavLink} from "react-router-dom";
import {Link} from "react-router-dom";
import {graphql} from 'react-apollo'
import gql from 'graphql-tag'


const Menu = ({data: {loading, error, promoMenus}}) => {
    if (error) return <h1></h1>
    if (!loading) {
        return (


            <div className="w_t--menu">
                {promoMenus.map(item => (

                    (() => {

                        return (
                            <React.Fragment>
                                {item.uaMenuName.map(function (name, index) {
                                    return (
                                        <NavLink
                                            to={`/ua${item.menuLinks[index]}`}
                                            className="w_t--m--item"
                                            activeClassName="w_t--m--item active"
                                        >
                                            {name}
                                        </NavLink>
                                    );
                                })}
                            </React.Fragment>
                        )
                    })()
                ))}
            </div>
        )
    }
    return (
        <div></div>
    )
}

export const menu = gql`
    {
        promoMenus {
            id
            uaMenuName
            menuLinks
        }
    }
`

export default graphql(menu)(Menu)


