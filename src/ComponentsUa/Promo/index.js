import React, {useState, useRef} from 'react';
import gql from "graphql-tag";
import {graphql} from 'react-apollo'
import {Helmet} from "react-helmet";
import TopSection from "./TopSection";
import Calculator from "./Calculator";


const Promo = ({data: {loading, error, promoInfoes}}) => {






    if (loading) return '';
    if (error) return '';

    return (

        <React.Fragment>
            <Helmet>
                <title>{promoInfoes[0].uaHeaderTitle}</title>
                <meta name="description" content={promoInfoes[0].headerDescription}/>
            </Helmet>


            <div className="websites_main promo_main">
                <TopSection/>
                <Calculator/>
            </div>


        </React.Fragment>
    )
}


const promo = gql`
    {
        promoInfoes {
            uaHeaderTitle
            headerDescription
        }
    }
`

export default graphql(promo)(Promo)
