import React, { useState, useEffect } from 'react';
import {graphql} from 'react-apollo';
import gql from 'graphql-tag';
import {ReactComponent as WebsitesIcon} from '../../../Assets/img/websites-icon.svg';
import TopInput from './TopInput';
import TopCategoryName from "./TopCategoryName";
import MiddleCategoryName from "./MiddleCategoryName";
import MiddleInput from "./MiddleInput";
import {connect} from "react-redux";



const Calculator = ({data: {loading, error, websitesCalculatorTopCategories, websitesCalculatorMidleCategories, websiteInfoes}}) => {



    if (error) return <h1></h1>
    if (!loading) {
        return (

            <React.Fragment>


                <div className="websites_m">
                    <div className="w_m--icon up">
                        <WebsitesIcon/>
                    </div>


                    <div className="w_m--wrap">
                        <h1 className="title up">{websiteInfoes[0].uaTitle}</h1>
                        {/*Верхняя часть калькулятора*/}
                        <div className="w_m--top up">

                            {websitesCalculatorTopCategories.map(category => (
                                <div className="w_m--t--item w_m--type" key={category.id}>

                                    <TopCategoryName
                                        categoryName={category.uaCategoryName}
                                        helpUrl={category.helpUrl}
                                        helpText={category.uaHelpText}/>

                                    <form>
                                        <React.Fragment>
                                            {category.websitesCalculatorTopItemses.map(item => (
                                                <TopInput key={item.id}
                                                          name={item.uaName}
                                                          cost={item.cost}
                                                          day={item.day}
                                                          id={item.id}
                                                          checked={item.checked}
                                                          nameId={category.id}
                                                          category={category.uaCategoryName}
                                                />
                                            ))}
                                        </React.Fragment>
                                    </form>
                                </div>
                            ))}

                        </div>

                        {/*Конец верхней части калькулятора*/}


                        {/*Средняя часть калькулятора*/}

                        <div className="w_m--main up">

                            {websitesCalculatorMidleCategories.map(category => (
                                <div className="w_m--m--item">
                                    <MiddleCategoryName
                                        key={category.id}
                                        titleClassName={category.titleClassName}
                                        categoryName={category.uaCategoryName}
                                        helpText={category.uaHelpText}
                                        helpUrl={category.helpUrl}
                                    />
                                    {category.websitesCalculatorMidleItems.map(item => (
                                        <MiddleInput
                                            key={item.id}
                                            id={item.id}
                                            name={item.uaName}
                                            cost={item.cost}
                                            day={item.day}
                                            useCount={item.useCount}
                                            countInputText={item.uaCountInputText}
                                            nameId={category.id}
                                        />
                                    ))}
                                </div>
                            ))}


                        </div>

                        {/*Конец средней части калькулятора*/}


                    </div>
                </div>

            </React.Fragment>
        )
    }
    return (
        <div></div>
    )
}

export const calculator = gql`
    {
        websiteInfoes {
            uaTitle
        }
        websitesCalculatorTopCategories {
            id
            uaCategoryName
            websitesCalculatorTopItemses {
                id
                uaName
                cost
                day
                checked
            }
            uaHelpText
            helpUrl
        }
        websitesCalculatorMidleCategories {
            id
            titleClassName
            uaCategoryName
            uaHelpText
            helpUrl
            websitesCalculatorMidleItems {
                id
                uaName
                cost
                day
                useCount
                uaCountInputText
            }
        }
    }
`

export default graphql(calculator)(Calculator)

