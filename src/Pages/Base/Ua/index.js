import React, {useEffect} from 'react';
import gql from 'graphql-tag'
import {graphql} from 'react-apollo'
import {Helmet} from "react-helmet";
import {Link} from "react-router-dom";
import Header from "../../../ComponentsUa/Header";
import Preloader from "../../../ComponentsUa/Preloader";
import Bg from '../../../Assets/img/base_page--bg.png'
import {ReactComponent as IconFooter} from '../../../Assets/svg/baseIcon.svg'
import {ReactComponent as IconLine} from '../../../Assets/svg/baseLine.svg'
import {ReactComponent as IconSquare} from '../../../Assets/svg/baseSquare.svg'
import NavLink from "react-router-dom/NavLink";
import ScrollReveal from "scrollreveal";


const BasePageUa = ({data: {loading, error, bases, baseCategories}}) => {

    useEffect(() => {
            const timer = setTimeout(() => {
                scrollReveal()
            }, 1500 );
            return () => clearTimeout(timer);
        }
    );

    function scrollReveal() {

        ScrollReveal().reveal('.up', {
            distance: '100px',
            interval: 400,
            duration: 1700,
            easing: 'cubic-bezier(0.175, 0.885, 0.32, 1)',
            delay: 500,
            opacity: 0
        });

        ScrollReveal().reveal('.scale', {
            easing: 'cubic-bezier(0.175, 0.885, 0.32, 1)',
            duration: 3000,
            scale: .1
        });
    }


    if (error) return <h1></h1>
    if (!loading) {
        return (
            <React.Fragment>
                <Helmet>
                    <body className="classic_page"/>
                    <title>{bases[0].uaHeaderTitle}</title>
                    <meta name="description" content={bases[0].headerDescription}/>
                </Helmet>
                <Header/>

                <div className="base_page">
                    <div className="base_top">
                        <div className="base_top--wrap">
                            <h1 className="base_top--title up">
                                <span>{bases[0].uaTitle}</span>
                                <IconSquare/>
                            </h1>
                            <img
                                className="base_top--icon up"
                                src={`https://media.graphcms.com/${bases[0].icon.handle}`}
                                alt=""
                            />
                            <IconLine/>
                        </div>
                    </div>
                    <div className="base_main">
                        <div className="base_main--wrap">

                            {baseCategories.map(category => (

                                (() => {

                                    return (
                                        <React.Fragment>
                                            <div className="base_main--block">
                                                <div className="b_m--b--title up">{category.uaNameOfCategory}</div>
                                                <div className="b_m--b--list up">
                                                    <React.Fragment>
                                                        {category.basePageses.map(item => (
                                                            <Link
                                                                to={`base/${item.linkAdress}`}
                                                                key={item.id}
                                                            >
                                                                {item.uaTitle}
                                                            </Link>
                                                        ))}
                                                    </React.Fragment>
                                                </div>
                                            </div>

                                        </React.Fragment>
                                    )
                                })()
                            ))}

                        </div>
                    </div>
                    <IconFooter/>
                    <img
                        className="base_page--bg"
                        src={Bg}
                        alt=""/>
                </div>

                <Preloader/>
            </React.Fragment>
        )
    }
    return (
        <div></div>
    )
}

export const info = gql`
    {
        bases {
            uaTitle
            icon {
                handle
            }
            uaHeaderTitle
            headerDescription
        }
        baseCategories {
            uaNameOfCategory
            basePageses {
                id
                uaTitle
                linkAdress
            }
        }
    }
`

export default graphql(info)(BasePageUa)
