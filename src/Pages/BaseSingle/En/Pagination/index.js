import React, {useEffect, useState} from 'react';
import PrevBase from "./Prev";
import NextBase from "./Next";


const BasePagination = (props) => {


    return (
        <React.Fragment>

            <div className="base_inner--buttons">

                <PrevBase
                    currentId={props.currentId.toString()}
                />
                <NextBase
                    currentId={props.currentId.toString()}
                />

            </div>
        </React.Fragment>
    )
}


export default BasePagination;
