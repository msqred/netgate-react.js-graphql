import React, {useEffect, useState} from 'react';
import gql from 'graphql-tag'
import {graphql} from 'react-apollo'
import {Helmet} from "react-helmet";
import {Link} from "react-router-dom";
import Header from "../../../ComponentsEn/Header";
import Preloader from "../../../ComponentsEn/Preloader";
import Bg from '../../../Assets/img/base_inner--page--bg.png'
import ScrollReveal from "scrollreveal";
import BasePagination from "./Pagination";


const BaseSingleEn = ({data: {loading, error, basePageses}}) => {
    useEffect(() => {
            const timer = setTimeout(() => {
                scrollReveal()
            }, 1500 );
            return () => clearTimeout(timer);
        }
    );

    function scrollReveal() {

        ScrollReveal().reveal('.up', {
            distance: '100px',
            interval: 400,
            duration: 1700,
            easing: 'cubic-bezier(0.175, 0.885, 0.32, 1)',
            delay: 500,
            opacity: 0
        });

        ScrollReveal().reveal('.scale', {
            easing: 'cubic-bezier(0.175, 0.885, 0.32, 1)',
            duration: 3000,
            scale: .1
        });
    }



    if (error) return <h1></h1>
    if (!loading) {
        return (
            <React.Fragment>
                <Helmet>
                    <body className="classic_page"/>
                    <title>{basePageses[0].enHeaderTitle}</title>
                    <meta name="description" content={basePageses[0].headerDescription}/>
                </Helmet>
                <Header/>
                <div className="base_inner--page">
                    <div className="base_inner--wrap">
                        <div className="base_inner--top">
                            <div className="base_inner--category up">
                                <a className="b_i--c--prev" href="/en/base">
                                    <i className="icon-back"/>
                                </a>
                                <div className="base_inner--name">{basePageses[0].baseCategory.enNameOfCategory}</div>
                            </div>
                            <div className="base_inner--title up">{basePageses[0].enTitle}</div>
                        </div>
                        <div className="base_inner--info up">
                            <div className="b_i--img">
                                <img
                                    src={`https://media.graphcms.com/${basePageses[0].coverImage.handle}`}
                                    alt=""
                                />
                            </div>
                            <div className="b_i--description">
                                {basePageses[0].enDescription}
                            </div>
                            <div className="b_i--text"
                                 dangerouslySetInnerHTML={{__html: `${basePageses[0].enMainText.html}`}}
                            >

                            </div>
                        </div>
                        <BasePagination
                            currentId={basePageses[0].id}
                        />
                    </div>
                    <img
                        className="base_inner--page--bg"
                        src={Bg}
                        alt=""
                    />
                </div>

                <Preloader/>
            </React.Fragment>
        )
    }
    return (
        <div></div>
    )
}

export const info = gql`
    query basePageses($link: String) {
        basePageses(where: {linkAdress: $link}) {
            id
            enTitle
            coverImage {
                handle
            }
            enDescription
            enMainText {
                html
            }
            enHeaderTitle
            headerDescription
            linkAdress
            baseCategory {
                enNameOfCategory
            }
        }
    }
`


export default graphql(info,  {
    options: ({match}) => ({
        variables: {
            link: match.params.link
        }
    })
})(BaseSingleEn)
