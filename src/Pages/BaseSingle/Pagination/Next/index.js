import React, {useState, useEffect} from 'react';
import gql from 'graphql-tag'
import {graphql} from 'react-apollo'
import {Link} from "react-router-dom";


const NextBase = ({data: {loading, error, basePageses}, currentId}) => {
    function getSafe() {
        try {
            if (error) return <h1></h1>
            if (!loading) {
                return (

                    <Link
                        className="b_i--btn--next"
                        to={typeof basePageses[0].linkAdress !== "undefined" ? basePageses[0].linkAdress : '/base'}
                    >
                        <i className="icon-back"/>
                        <span>{typeof basePageses[0].title !== "undefined" ? basePageses[0].title : 'К базе знаний'}</span>
                    </Link>

                )
            }
            return (
                <div></div>
            )

        } catch (e) {

            return (

                <Link
                    className="b_i--btn--next"
                    to='/base'
                >
                    <i className="icon-back"/>
                    <span>К базе знаний</span>
                </Link>

            )

        }
    }

    return getSafe();


}


export const nextPage = gql`
    query basePageses($id: String!) {
        basePageses(first: 1, after: $id) {
            title
            linkAdress
        }
    }
`

export default graphql(nextPage, {
    options: (ownProps) => ({
        variables: {
            id: ownProps.currentId // set your path like this
        }
    })
})(NextBase);
