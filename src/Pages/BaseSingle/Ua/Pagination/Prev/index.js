import React, {useState, useEffect} from 'react';
import gql from 'graphql-tag'
import {graphql} from 'react-apollo'
import {Link} from "react-router-dom";


const PrevBase = ({data: {loading, error, basePageses}, currentId}) => {



    function getSafe() {
        try {
            if (error) return <h1></h1>
            if (!loading) {
                return (

                    <Link
                        className="b_i--btn--prev"
                        to={typeof basePageses[0].linkAdress !== "undefined" ? `/ua${basePageses[0].linkAdress}` : '/ua/base'}
                    >
                        <i className="icon-back"/>
                        <span>{typeof basePageses[0].uaTitle !== "undefined" ? basePageses[0].uaTitle : 'Повернутися до бази'}</span>
                    </Link>

                )
            }
            return (
                <div></div>
            )

        } catch (e) {

            return (

                <Link
                    className="b_i--btn--prev"
                    to='/ua/base'
                >
                    <i className="icon-back"/>
                    <span>Повернутися до бази</span>
                </Link>

            )

        }
    }

    return getSafe();
}


const prevPage = gql`
    query basePageses($id: String!) {
        basePageses(first: 1, before: $id) {
            uaTitle
            linkAdress
        }
    }
`

export default graphql(prevPage, {
    options: (ownProps) => ({
        variables: {
            id: ownProps.currentId // set your path like this
        }
    })
})(PrevBase);

