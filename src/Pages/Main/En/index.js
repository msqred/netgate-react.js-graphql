import React, {useEffect} from 'react';
import ScrollReveal from 'scrollreveal';
import Header from "../../../ComponentsEn/Header";
import Main from "../../../ComponentsEn/Main";
import Preloader from "../../../ComponentsEn/Preloader";
import {Helmet} from "react-helmet";


const MainPageEn = () => {

    useEffect(() => {
            const timer = setTimeout(() => {
                scrollReveal()
            }, 1500 );
            return () => clearTimeout(timer);
        }
    );

    function scrollReveal() {

        ScrollReveal().reveal('.up', {
            distance: '100px',
            interval: 400,
            duration: 1700,
            easing: 'cubic-bezier(0.175, 0.885, 0.32, 1)',
            delay: 500,
            opacity: 0
        });

        ScrollReveal().reveal('.scale', {
            easing: 'cubic-bezier(0.175, 0.885, 0.32, 1)',
            duration: 3000,
            scale: .1
        });
    }

    return (
        <React.Fragment>
            <Helmet>
                <meta charSet="utf-8"/>
                <title>Netgate</title>
                <body className="m_page"/>
            </Helmet>
            <Header/>
            <Main/>
            <Preloader/>
        </React.Fragment>
    )
}

export default MainPageEn;
