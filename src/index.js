import React from 'react'
import ReactDOM from 'react-dom'

import {ApolloClient} from 'apollo-client'
import {HttpLink} from 'apollo-link-http'
import {InMemoryCache} from 'apollo-cache-inmemory'
import {ApolloProvider} from 'react-apollo'
import {Provider} from 'react-redux'
import {createStore} from "redux";
import update from 'react-addons-update';

import App from './App'
import './Assets/styles/index.scss'


const GRAPHCMS_API = 'https://api-euwest.graphcms.com/v1/ck3et14xt11ut01cqfj1hbn3w/master';

const client = new ApolloClient({
    link: new HttpLink({uri: GRAPHCMS_API}),
    cache: new InMemoryCache()
});

const initialState = {
    websiteType: [
        {
            name: '',
            cost: 0,
            day: 0,
            count: 1
        }
    ],
    websiteDesign: [
        {
            name: '',
            cost: 0,
            day: 0,
            count: 1
        }
    ],
    websiteAdaptive: [
        {
            name: '',
            cost: 0,
            day: 0,
            count: 1
        }
    ],
    websitesAdditional: [
        {
            name: '',
            cost: 0,
            day: 0,
            count: 0
        }
    ]
};

function reducer(state = initialState, action) {
    if (action.type === "ADD_WEBSITE_TYPE" && action.payload.category === "Тип сайта") {
        return {
            ...state,
            websiteType: [
                action.payload
            ]
        };
    }
    if (action.type === "ADD_WEBSITE_TYPE" && action.payload.category === "Site type") {
        return {
            ...state,
            websiteType: [
                action.payload
            ]
        };
    }
    if (action.type === "ADD_WEBSITE_TYPE" && action.payload.category === "Тип сайту") {
        return {
            ...state,
            websiteType: [
                action.payload
            ]
        };
    }
    if (action.type === "ADD_WEBSITE_DESIGN" && action.payload.category === "Дизайн") {
        return {
            ...state,
            websiteDesign: [
                action.payload
            ]
        };
    }
    if (action.type === "ADD_WEBSITE_DESIGN" && action.payload.category === "Design") {
        return {
            ...state,
            websiteDesign: [
                action.payload
            ]
        };
    }

    if (action.type === "ADD_WEBSITE_ADAPTIVE" && action.payload.category === "Адаптивность") {
        return {
            ...state,
            websiteAdaptive: [
                ...state.websiteAdaptive,
                action.payload
            ]
        };
    }
    if (action.type === "ADD_WEBSITE_ADAPTIVE" && action.payload.category === "Adaptability") {
        return {
            ...state,
            websiteAdaptive: [
                ...state.websiteAdaptive,
                action.payload
            ]
        };
    }
    if (action.type === "ADD_WEBSITE_ADAPTIVE" && action.payload.category === "Адаптивність") {
        return {
            ...state,
            websiteAdaptive: [
                ...state.websiteAdaptive,
                action.payload
            ]
        };
    }
    if (action.type === "DELETE_WEBSITE_ADAPTIVE") {
        return {
            ...state,
            websiteAdaptive: [
                ...state.websiteAdaptive.filter(name => name.name !== action.payload.name)
            ]
        };
    }
    if (action.type === "ADD_WEBSITE_ADDITIONAL") {
        return {
            ...state,
            websitesAdditional: [
                ...state.websitesAdditional,
                action.payload
            ]
        };
    }
    if (action.type === "DELETE_WEBSITE_ADDITIONAL") {
        return {
            ...state,
            websitesAdditional: [
                ...state.websitesAdditional.filter(name => name.name !== action.payload.name)
            ]
        };
    }
    return state;
}

const store = createStore(reducer);

store.subscribe(() => {
    console.log('subscribe', store.getState());
});


ReactDOM.render(
    <ApolloProvider client={client}>
        <Provider store={store}>
            <App/>
        </Provider>
    </ApolloProvider>,
    document.getElementById('root')
);
